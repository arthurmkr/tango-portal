CREATE TABLE IF NOT EXISTS user_profile (
  id       BIGSERIAL PRIMARY KEY NOT NULL,
  username VARCHAR,
  password VARCHAR
);

CREATE TABLE IF NOT EXISTS user_role (
  user_id BIGINT NOT NULL,
  role    VARCHAR(10),

  CONSTRAINT user_role_user_id_fk FOREIGN KEY (user_id) REFERENCES user_profile (id)
);

CREATE TABLE IF NOT EXISTS city (
  id   BIGSERIAL PRIMARY KEY NOT NULL,
  name VARCHAR               NOT NULL,
  pid  VARCHAR               NOT NULL,
  lat  FLOAT                 NOT NULL,
  lng  FLOAT                 NOT NULL
);

CREATE TABLE IF NOT EXISTS address (
  id      BIGSERIAL PRIMARY KEY NOT NULL,
  value   VARCHAR               NOT NULL,
  pid     VARCHAR               NOT NULL,
  lat     FLOAT                 NOT NULL,
  lng     FLOAT                 NOT NULL,
  city_id BIGINT                NOT NULL,

  CONSTRAINT address_city_id_fk FOREIGN KEY (city_id) REFERENCES city (id)
);

CREATE TABLE IF NOT EXISTS school (
  id               BIGSERIAL PRIMARY KEY NOT NULL,
  name             VARCHAR,
  work_phone       VARCHAR,
  mobile_phone     VARCHAR,
  address_id       BIGINT,
  site_url         VARCHAR,
  fb_url           VARCHAR,
  vk_url           VARCHAR,
  default_duration INT,
  user_id          BIGINT                NOT NULL,

  CONSTRAINT school_address_id_fk FOREIGN KEY (address_id) REFERENCES address (id),
  CONSTRAINT school_user_id_fk FOREIGN KEY (user_id) REFERENCES user_profile (id)
);

CREATE TABLE IF NOT EXISTS teacher (
  id          BIGSERIAL PRIMARY KEY NOT NULL,
  first_name  VARCHAR               NOT NULL,
  second_name VARCHAR               NOT NULL,
  school_id   BIGINT                NOT NULL,
  user_id     BIGINT                NOT NULL,
  resume      VARCHAR,

  CONSTRAINT teacher_school_id_fk FOREIGN KEY (school_id) REFERENCES school (id),
  CONSTRAINT teacher_user_id_fk FOREIGN KEY (user_id) REFERENCES user_profile (id)
);

CREATE TABLE IF NOT EXISTS room (
  id         BIGSERIAL PRIMARY KEY NOT NULL,
  name       VARCHAR,
  address_id BIGINT,
  school_id  BIGINT,
  user_id    BIGINT                NOT NULL,

  CONSTRAINT room_school_id_fk FOREIGN KEY (school_id) REFERENCES school (id),
  CONSTRAINT room_address_id_fk FOREIGN KEY (address_id) REFERENCES address (id),
  CONSTRAINT room_user_id_fk FOREIGN KEY (user_id) REFERENCES user_profile (id)
);

CREATE TABLE IF NOT EXISTS festival (
  id          BIGSERIAL PRIMARY KEY NOT NULL,
  user_id     BIGINT                NOT NULL,
  school_id   BIGINT,
  name        VARCHAR               NOT NULL,
  start_date  DATE                  NOT NULL,
  end_date    DATE                  NOT NULL,
  address_id  BIGINT                NOT NULL,
  description VARCHAR,

  CONSTRAINT festival_address_id_fk FOREIGN KEY (address_id) REFERENCES address (id),
  CONSTRAINT festival_user_id_fk FOREIGN KEY (user_id) REFERENCES user_profile (id),
  CONSTRAINT festival_school_id_fk FOREIGN KEY (school_id) REFERENCES school (id)
);

CREATE TABLE IF NOT EXISTS activity (
  id                BIGSERIAL PRIMARY KEY NOT NULL,
  name              VARCHAR               NOT NULL,
  school_id         BIGINT,
  festival_id       BIGINT,
  user_id           BIGINT                NOT NULL,
  start_date        DATE                  NOT NULL,
  end_date          DATE,
  start_time        TIME                  NOT NULL,
  duration          INT                   NOT NULL,
  cost              INT                   NOT NULL,
  address_id        BIGINT                NOT NULL,
  type              VARCHAR               NOT NULL,
  day_of_week       INT,
  regular           BOOL                  NOT NULL,
  dj                VARCHAR,
  lesson_level      INT DEFAULT 0,
  room_id           BIGINT,
  first_teacher_id  BIGINT,
  second_teacher_id BIGINT,

  CONSTRAINT activity_user_id_fk FOREIGN KEY (user_id) REFERENCES user_profile (id),
  CONSTRAINT activity_school_id_fk FOREIGN KEY (school_id) REFERENCES school (id),
  CONSTRAINT activity_festival_id_fk FOREIGN KEY (festival_id) REFERENCES festival (id),
  CONSTRAINT activity_address_id_fk FOREIGN KEY (address_id) REFERENCES address (id),
  CONSTRAINT activity_room_id_fk FOREIGN KEY (room_id) REFERENCES room (id),
  CONSTRAINT activity_first_teacher_id_fk FOREIGN KEY (first_teacher_id) REFERENCES teacher (id),
  CONSTRAINT activity_second_teacher_id_fk FOREIGN KEY (second_teacher_id) REFERENCES teacher (id)
);
