ALTER TABLE user_profile
  ADD COLUMN dj BOOLEAN NOT NULL DEFAULT FALSE,
  ADD COLUMN teacher BOOLEAN NOT NULL DEFAULT FALSE,
  ADD COLUMN school_lead BOOLEAN NOT NULL DEFAULT FALSE,
  ADD COLUMN organizer BOOLEAN NOT NULL DEFAULT FALSE,
  ADD invite_token VARCHAR,
  ADD status VARCHAR NOT NULL DEFAULT 'INACTIVE',
  ADD invited_by BIGINT NOT NULL DEFAULT 1;

ALTER TABLE user_profile
  ADD CONSTRAINT user_invited_by_id_fk FOREIGN KEY (invited_by) REFERENCES user_profile (id);

UPDATE user_profile
SET status = 'ACTIVE';

CREATE TABLE IF NOT EXISTS register_request (
  id      BIGSERIAL PRIMARY KEY NOT NULL,
  created TIMESTAMP             NOT NULL,
  token   VARCHAR               NOT NULL,
  user_id BIGINT                NOT NULL,
  status  VARCHAR               NOT NULL,
  sent    TIMESTAMP,
  CONSTRAINT register_request_user_id_fk FOREIGN KEY (user_id) REFERENCES user_profile (id)
);

GRANT INSERT, DELETE, UPDATE, SELECT ON register_request TO tangouser;