-- update 02-07-2017
ALTER TABLE teacher
  ADD COLUMN avatar VARCHAR;

-- update 04-07-2017
ALTER TABLE user_profile
  ADD COLUMN first_name VARCHAR,
  ADD COLUMN second_name VARCHAR,
  ADD COLUMN fb_profile VARCHAR,
  ADD COLUMN vk_profile VARCHAR,
  ADD COLUMN city_id BIGINT;


ALTER TABLE user_profile
  ADD CONSTRAINT user_city_id_fk FOREIGN KEY (city_id) REFERENCES city (id);

-- update 05-07-2017
ALTER TABLE user_profile
  ADD COLUMN avatar VARCHAR;