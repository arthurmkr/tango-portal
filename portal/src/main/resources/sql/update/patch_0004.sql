CREATE TABLE IF NOT EXISTS js_error (
  id          BIGSERIAL PRIMARY KEY NOT NULL,
  created     TIMESTAMP             NOT NULL DEFAULT now(),
  message     VARCHAR               NOT NULL,
  url         VARCHAR,
  line_number INT,
  col_number  INT,
  ip          VARCHAR               NOT NULL,
  user_id     VARCHAR               NOT NULL,
  user_agent  VARCHAR               NOT NULL,
  referrer    VARCHAR
)