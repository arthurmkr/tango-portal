<#ftl encoding="UTF-8">
<#setting number_format="computer">

<#import "spring.ftl" as spring/>

<#assign availableLanguages = {"ru" : "Русский", "en" : "English", "es" : "Español"}/>

<#macro display_page initFunction="">
    <#assign language = .locale_object?string[0..1]/>
<!DOCTYPE html>
<html lang="${language}">
<head>
    <#if !Session.debug>
        <base href="${Session.serverUrl + '/' + language + '/'}">
    <#else >
        <base href="${Session.serverUrl + '/'}">
    </#if>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="yandex-verification" content="c71915207e772e4a"/>
    <meta name="google-site-verification" content="rNyQEpHHADU9cYtsHjubHUSc--JTivjxjMKlC_nfSN0"/>
    <#if _csrf??>
        <meta name="_csrf" content="${(_csrf.token)!''}"/>
        <meta name="_csrf_header" content="${(_csrf.headerName)!''}"/>
    </#if>
    <title><@head_title/></title>
    <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=default-3.6,HTMLCanvasElement.prototype.toBlob"></script>
    <!-- CSS Files -->
    <link href="/css/bootstrap-clockpicker.min.css" rel="stylesheet">
    <link href="/css/google-font-roboto.css" rel="stylesheet" type="text/css"/>
    <link href="/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/css/material-kit.min.css" rel="stylesheet"/>
    <link href="/css/cropper.min.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet"/>


    <#if !Session.debug>
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" async>
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function () {
                    try {
                        w.yaCounter45311346 = new Ya.Metrika({
                            id: 45311346,
                            clickmap: true,
                            trackLinks: true,
                            accurateTrackBounce: true
                        });
                    } catch (e) {
                    }
                });
                var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function () {
                            n.parentNode.insertBefore(s, n);
                        };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mc.yandex.ru/metrika/watch.js";
                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else {
                    f();
                }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript>
            <div><img src="https://mc.yandex.ru/watch/45311346" style="position:absolute; left:-9999px;" alt=""/></div>
        </noscript>
        <!-- /Yandex.Metrika counter -->

        <!-- Google counter -->
        <script type="text/javascript" async>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-102575339-1', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- /Google counter -->
    </#if>
</head>
<body>

<div class="container">

    <!-- Static navbar -->
    <nav class="navbar navbar-primary navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="./">Tango World</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="./"><@spring.message "nav.label.playbill" /></a></li>
                    <li><a href="./schools"><@spring.message "nav.label.schools"/></a></li>
                    <li><a href="./events"><@spring.message "nav.label.events"/></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false"><@spring.message "nav.label.people"/> <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="./djs"><@spring.message "nav.label.djs"/></a></li>
                            <li><a href="./photographers"><@spring.message "nav.label.photographers"/></a></li>
                        </ul>
                    </li>
                    <li><a href="./map"><@spring.message "nav.label.map"/></a></li>
                    <#if Session.user??>
                        <li><a href="./cabinet"><@spring.message "nav.label.cabinet"/></a></li>
                    </#if>

                    <#if Session.user??>
                        <li>
                            <a href="/logout" class="btn btn-rose btn-raised btn-round">
                                <@spring.message "button.label.logout"/>
                            </a>
                        </li>
                    <#else>
                        <li>
                            <a class="btn btn-rose btn-raised btn-round" data-toggle="modal"
                               data-target="#loginModal">
                                <@spring.message "button.label.login"/>
                            </a>
                        </li>
                    </#if>
                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>
</div> <!-- /container -->

    <@page_content/>

<footer class="footer footer-black">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav>
                    <ul>
                        <#list  availableLanguages as key, value>
                            <li><a class="${(key == language)?then('language-selected','')}" href="/${key}/">${value}</a></li>
                        </#list>
                    </ul>
                </nav>
            </div>
        </div>
        <a class="footer-brand hidden-xs" href="./">Tango World</a>

        <ul class="pull-left">
            <li>
                <a href="./about">
                    <@spring.message "nav.label.about"/>
                </a>
            </li>
            <li>
                <a href="./schools">
                    <@spring.message "nav.label.schools"/>
                </a>
            </li>
            <li>
                <a href="./djs">
                    <@spring.message "nav.label.djs"/>
                </a>
            </li>
            <li>
                <a href="./photographers">
                    <@spring.message "nav.label.photographers"/>
                </a>
            </li>
            <li>
                <a href="./events">
                    <@spring.message "nav.label.events"/>
                </a>
            </li>
            <li>
                <a href="./map">
                    <@spring.message "nav.label.map"/>
                </a>
            </li>
        </ul>

        <div class="copyright pull-right">
            Copyright © 2017 TangoWorld All Rights Reserved.
        </div>
        <ul class="social-buttons pull-right">
            <li>
                <a href="https://www.facebook.com/groups/tangoworld" target="_blank"
                   class="btn btn-just-icon btn-simple">
                    <i class="fa fa-facebook-square"></i>
                </a>
            </li>
        </ul>
    </div>
</footer>

    <#include "components/loginForm.ftl"/>
    <#include "components/confirmationDialog.ftl"/>


    <@components/>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDO93xxES72FyLkmEjRhSLd7wszT7hBbKc&libraries=places"></script>

<script src="/wro4j/all-${language}.js" type="text/javascript"></script>

<script type="text/javascript" defer>
    $(function () {
        materialKit.initFormExtendedDatetimepickers();

    })

        <#if initFunction?has_content>
        ${initFunction}();
        </#if>

</script>

</body>
</html>
</#macro>

<#macro components>

</#macro>

<#macro head_title>
    <@spring.message "page.title.default"/>
</#macro>

<#macro page_content>

</#macro>

<#function array args...>
    <#return args/>
</#function>