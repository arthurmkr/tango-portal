<#ftl encoding="UTF-8">

<#include "template.ftl"/>

<#macro components>
    <#include "components/form.ftl"/>

</#macro>

<#macro page_content>
<div class="wrapper" id="schoolList" v-cloak="">

    <div class="container page-filter-panel">
        <div class="main main-raised">
            <div class="row">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <h2 class="title"><@spring.message "page.title.schools"/></h2>
                        <form class="form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback is-empty label-floating">
                                        <label for="city" class="control-label"><@spring.message "common.city"/></label>
                                        <tp-city name="city" v-model="filter.city"></tp-city>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="map" style="height: 500px;">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container" v-if="isEmpty && firstLoaded">
        <div class="row">
            <div class="col-md-12">
                <div class="card text-center">

                    <div class="card-content content-warning">
                        <h3 class="card-title"><@spring.message "error.school.notFoundInCity"/>
                            {{filter.city.name}}.</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="grid">
            <div v-for="school in pageData.schools" class="grid-item col-md-6">
                <div class="grid-item-content">

                    <div class="card text-center">
                        <a :href="'/school/' + school.id" v-if="school.photo">
                            <img :src="school.photo" style="min-height: 180px">
                        </a>
                        <div class="card-content">
                            <h3 class="card-title">
                                <a :href="'/school/' + school.id">{{school.name}}</a>
                            </h3>

                            <div class="footer">
                                <div class="tp-stats" v-if="school.address">
                                    <i class="material-icons">room</i> {{school.address.value}}
                                </div>

                                <div class="tp-stats" v-if="school.mobilePhone">
                                    <i class="material-icons">phone</i> {{school.mobilePhone}}
                                </div>
                                <div class="tp-stats" v-if="school.workPhone">
                                    <i class="material-icons">phone</i> {{school.workPhone}}
                                </div>

                                <a v-if="school.siteUrl" :href="school.siteUrl" target="_blank"
                                   class="btn btn-just-icon btn-warning btn-round"><i
                                        class="fa fa-external-link"></i></a>
                                <a v-if="school.fbUrl" :href="school.fbUrl" target="_blank"
                                   class="btn btn-just-icon btn-facebook btn-round"><i
                                        class="fa fa-facebook-square"></i></a>
                                <a v-if="school.vkUrl" :href="school.vkUrl" target="_blank"
                                   class="btn btn-just-icon btn-vk btn-round"><i
                                        class="fa fa-vk"></i></a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>

<@display_page "initSchoolListPage"/>