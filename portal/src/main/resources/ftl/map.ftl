<#ftl encoding="UTF-8">

<#include "template.ftl"/>

<#macro page_content>
<div class="wrapper" id="tangoMap" v-cloak>
    <div class="container">
        <div class="main main-raised">
            <div class="row">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <h2 class="title"><@spring.message "page.title.map"/></h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="map" style="height: 500px;">
                        <div id="schoolCityPopover" v-show="initialized">
                            <h2 class="popover-title">{{selectedMarker.name}}</h2>
                            <div class="popover-content text-center">
                                <a v-if="selectedMarker.schoolCount" :href="'./schools?city=' + selectedMarker.name" class="btn btn-sm btn-primary block"><@spring.message "nav.label.schools"/> ({{selectedMarker.schoolCount}})</a>
                                <a v-if="selectedMarker.activityCount" :href="'./?city=' + selectedMarker.name" class="btn btn-sm btn-info block"><@spring.message "nav.label.playbill"/> ({{selectedMarker.activityCount}})</a>
                                <a v-if="selectedMarker.djCount" :href="'./djs?city=' + selectedMarker.name" class="btn btn-sm btn-success block"><@spring.message "nav.label.djs"/> ({{selectedMarker.djCount}})</a>
                                <a v-if="selectedMarker.eventCount" :href="'./events'" class="btn btn-sm btn-warning block"><@spring.message "nav.label.events"/> ({{selectedMarker.eventCount}})</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</#macro>

<@display_page "initMapPage"/>