<#ftl encoding="UTF-8">

<#include "template.ftl"/>

<#macro page_content>
<div class="wrapper" id="index">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-offset-3">
                <div class="card">
                    <#if response.already>
                        <div class="card-content content-success">
                            <h4 class="card-title">
                                <@spring.message "page.registrationConfirm.emailAlreadyConfirmed"/>
                            </h4>
                            <p class="card-description">
                                <@spring.message "page.registrationConfirm.emailAlreadyConfirmed.description"/>
                            </p>
                        </div>
                    <#else>
                        <#if !response.userInvalid>
                            <div class="card-content content-success">
                                <h4 class="card-title">
                                    <@spring.message "page.registrationConfirm.emailConfirmed"/>
                                </h4>
                                <p class="card-description">
                                    <@spring.messageArgs "page.registrationConfirm.emailConfirmed.description1"
                                    array("<strong>${response.user.firstName} ${response.user.secondName}</strong>")/>
                                </p>
                                <p class="card-description">
                                    <@spring.message "page.registrationConfirm.emailConfirmed.description3"/>
                                </p>
                            </div>
                        <#else >
                            <div class="card-content content-rose">
                                <h4 class="card-title">
                                    <@spring.message "page.registrationConfirm.emailConfirmed"/>
                                </h4>
                                <p class="card-description">
                                    <@spring.message "page.registrationConfirm.emailConfirmed.description4"/>
                                </p>
                                <p class="card-description">
                                    <@spring.message "page.registrationConfirm.emailConfirmed.description5"/>
                                </p>
                            </div>
                        </#if>
                    </#if>
                </div>
            </div>
        </div>
    </div>
</div>

</#macro>

<@display_page />