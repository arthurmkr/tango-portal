<#import "../util.ftl" as util/>
<#import "/spring.ftl" as spring/>

<script type="text/x-template" id="teacher-list">
    <div>

        <div class="tp-block-header">
            <h3><@spring.message "common.teachers"/></h3>
            <button class="btn btn-success btn-fab btn-round" data-toggle="modal" data-target="#teacherModal">
                <i class="material-icons">add</i>
            </button>
        </div>

        <table class="table rows-hovered teacher-table">
            <colgroup>
                <col style="width: 100px">
                <col>
                <col class="td-actions">
            </colgroup>
            <thead>
            <tr>
                <th><@spring.message "common.avatar"/></th>
                <th><@spring.message "common.teacher"/></th>
                <th style="width: 76px"></th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="teacher in items">
                <td>
                    <img class="teacher-avatar"
                         :src="teacher.avatar"
                         v-blank-image>
                </td>
                <td>
                    {{teacher.firstName}} {{teacher.secondName}}
                </td>
                <td class="td-actions">
                    <button type="button" rel="tooltip" class="btn btn-info" @click="editItem(teacher)">
                        <i class="material-icons">edit</i>
                    </button>
                    <button type="button" rel="tooltip" class="btn btn-danger" @click="removeItem(teacher)">
                        <i class="material-icons">close</i>
                    </button>
                </td>
            </tr>

            </tbody>
        </table>
    </div>
</script>

<script type="text/x-template" id="teacher-modal">
    <@util.modalForm id="teacher">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-top: 0; margin-bottom: -30px">
                <image-cropper id="teacherAvatar" v-model="obj.avatar" :event="EventType.TEACHER_AVATAR_UPLOADED"></image-cropper>
            </div>
        </div>
        <div class="col-md-6">
            <div class="required form-group has-feedback is-empty label-floating">
                <label for="firstName" class="control-label"><@spring.message "common.firstName"/></label>

                <input id="firstName" name="firstName" type="text" class="form-control" v-model="obj.firstName"
                       required autocomplete="off"/>
            </div>

            <div class="required form-group has-feedback is-empty label-floating">
                <label for="secondName" class="control-label"><@spring.message "common.secondName"/></label>

                <input id="secondName" name="secondName" type="text" class="form-control" v-model="obj.secondName"
                       required autocomplete="off"/>
            </div>
        </div>
    </div>




    </@util.modalForm>
</script>

