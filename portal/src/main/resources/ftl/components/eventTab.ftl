<#import "/spring.ftl" as spring/>

<script type="text/x-template" id="event-tab">
    <div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <label for="events" class="control-label"><@spring.message "common.event"/></label>
                    <tp-select name="events" v-model="selectedEventId" :items="events" placeholder="<@spring.message 'placeholders.chooseEvent'/>"
                               :live-search="true" live-search-placeholder="<@spring.message 'placeholders.typeEventName'/>">
                        <template scope="props">
                            <option :value="props.item.id" :data-subtext="props.item.year">
                                <span :class="props.item.started ? 'started-event-list-item' : ''">{{props.item.name}}</span>
                            </option>
                        </template>
                    </tp-select>
                </div>
                <div class="col-md-6">
                    <div id="festivalTopButtonPanel">
                        <button type="button" class="btn btn-success" data-toggle="modal"
                                data-target="#eventModal"><i
                                class="fa fa-plus"></i> <@spring.message "button.label.add"/></button>
                        <button type="button" class="btn btn-danger" v-show="selectedEvent && !selectedEvent.started"
                                @click="removeEvent"
                        ><i class="fa fa-trash"></i> <@spring.message "button.label.delete"/>
                        </button>
                    </div>
                </div>
            </div>

        </div>
        <div class="container-fluid" v-if="selectedEvent">
            <div class="row">
                <div class="col-md-12">
                    <img :src="selectedEvent.photo" class="img-raised" style="width: 100%" v-if="selectedEvent.photo">
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 tp-block">
                    <div class="tp-block-header">
                        <h3><@spring.message "block.title.commonSchoolInfo"/></h3>
                        <button class="btn btn-info btn-fab btn-round" @click="editEvent"
                                v-show="selectedEvent && !selectedEvent.started">
                            <i class="material-icons">edit</i>
                        </button>
                    </div>

                    <p class="profile-item"><span><@spring.message "selectedEvent.name"/></span> <strong>{{selectedEvent.name}}</strong>
                    </p>
                    <p class="profile-item" v-if="selectedEvent.city != null">
                        <span><@spring.message "common.city"/></span>
                        <strong>{{selectedEvent.city.name}}</strong></p>
                    <p class="profile-item"><span><@spring.message "school.siteUrl"/></span> <a
                            :href="selectedEvent.siteUrl">{{selectedEvent.siteUrl}}</a>
                    </p>
                    <p class="profile-item"><span><@spring.message "common.facebook"/></span> <a
                            :href="selectedEvent.fbUrl">{{selectedEvent.fbUrl}}</a>
                    </p>
                    <p class="profile-item"><span><@spring.message "common.vkontakte"/></span> <a
                            :href="selectedEvent.vkUrl">{{selectedEvent.vkUrl}}</a>
                    </p>
                    <p class="profile-item"><span><@spring.message "event.startDate"/></span> <strong>{{selectedEvent.startDate
                        | localeDate}}</strong>
                    </p>
                    <p class="profile-item"><span><@spring.message "event.endDate"/></span> <strong>{{selectedEvent.endDate
                        | localeDate}}</strong>
                    </p>
                </div>
            </div>
            <div class="row">
                <milonga-list-table title="<@spring.message "event.milongas"/>"
                                    :items="selectedEvent.milongas"
                                    :edit-enabled="true"
                                    :remove-enabled="true"
                                    :onAdd="addMilonga"
                ></milonga-list-table>
            </div>
        </div>
    </div>
</script>