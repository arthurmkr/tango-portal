<#import "../util.ftl" as util/>
<#import "/spring.ftl" as spring/>


<script type="text/x-template" id="profile-modal">
    <@util.modalForm id="profile">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-top: 0; margin-bottom: -30px;">
                <image-cropper id="profileAvatar" v-model="obj.avatar" :event="EventType.PROFILE_AVATAR_UPLOADED"></image-cropper>
            </div>

            <@util.roles/>
        </div>
        <div class="col-md-6">
            <div class="required form-group has-feedback is-empty label-floating">
                <label for="firstName" class="control-label"><@spring.message "common.firstName"/></label>

                <input id="firstName" name="firstName" type="text" class="form-control" v-model="obj.firstName"
                       required autocomplete="off"/>
            </div>

            <div class="required form-group has-feedback is-empty label-floating">
                <label for="secondName" class="control-label"><@spring.message "common.secondName"/></label>

                <input id="secondName" name="secondName" type="text" class="form-control" v-model="obj.secondName"
                       required autocomplete="off"/>
            </div>

            <div class="required form-group has-feedback is-empty label-floating">
                <label for="city" class="control-label"><@spring.message "common.city"/></label>

                <tp-city name="city" v-model="obj.city" :required="true"></tp-city>
            </div>

            <div class="form-group has-feedback is-empty label-floating">
                <label for="vkProfile" class="control-label"><@spring.message "common.vkProfile"/></label>

                <input id="vkProfile" name="vkProfile" type="url" class="form-control" v-model="obj.vkProfile"
                       autocomplete="off"/>
            </div>

            <div class="form-group has-feedback is-empty label-floating">
                <label for="fbProfile" class="control-label"><@spring.message "common.fbProfile"/></label>

                <input id="fbProfile" name="fbProfile" type="url" class="form-control" v-model="obj.fbProfile"
                       autocomplete="off"/>
            </div>

            <div class="form-group has-feedback is-empty label-floating" v-show="obj.roles.dj">
                <label for="djNick" class="control-label"><@spring.message "common.djNick"/></label>

                <input id="djNick" name="djNick" type="text" class="form-control" v-model="obj.djNick"
                       autocomplete="off"/>
            </div>


        </div>
    </div>

    </@util.modalForm>
</script>

