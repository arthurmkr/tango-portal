<#import "/spring.ftl" as spring/>

<script type="text/x-template" id="alerts">
    <div>
        <div v-for="alert in alerts" class="alert" :class="'alert-' + alert.type">
            {{alert.msg}}
        </div>
    </div>
</script>

<script type="text/x-template" id="image-cropper">
    <div class="text-center image-container" :id="id">
        <div id="blankDiv" class="img-raised image-cropper-blank" v-show="!value" :style="blankStyle">
        </div>
        <div v-show="value" class="img-raised">
            <img id="image" :src="value">
        </div>
        <div>
            <span class="btn btn-raised btn-simple btn-default btn-file" data-toggle="modal"
                  :data-target="'#' + id + 'Modal'">
                <span v-show="!value"><@spring.message "button.label.choosePhoto"/></span>
                <span v-show="value"><@spring.message "button.label.change"/></span>
            </span>
            <span class="btn btn-raised btn-simple btn-warning btn-file" v-show="value" @click="removeImage">
                <span><@spring.message "button.label.delete"/></span>
            </span>
        </div>
    </div>
</script>


<script type="text/x-template" id="tp-address">
    <input :id="name" type="text" class="form-control" :name="name" placeholder="" :required="required"
           autocomplete="off"/>
</script>

<script type="text/x-template" id="tp-city">
    <input :id="name" type="text" class="form-control" :name="name" placeholder="" :required="required"
           autocomplete="off"/>
</script>

<script type="text/x-template" id="tp-date">
    <input :id="name" :name="name" type="text" class="form-control datepicker" :required="required" maxlength="10"
           autocomplete="off"/>
</script>

<script type="text/x-template" id="tp-time">
    <div class="input-group clockpicker">
        <input :id="name" :name="name" type="text" class="form-control" :required="required" maxlength="5"
               onkeypress="return false;" autocomplete="off"/>
    </div>
</script>

<script type="text/x-template" id="tp-select">
    <select :id="name" :name="name" class="selectpicker" data-style="form-control" v-model="internalValue"
            :required="required"
            :multiple="multiple"
            :data-max-options="maxOptions"
            :data-none-selected-text="placeholder"
            :title="placeholder"
            data-size="10"
            :data-live-search="liveSearch"
            :data-live-search-placeholder="liveSearchPlaceholder">
        <template v-for="item in items">
            <slot :item="item"/>
        </template>
    </select>
</script>

<script type="text/x-template" id="image-select-modal">
    <div class="modal fade" :id="modalId" tabindex="-1" role="dialog" aria-hidden="true"
         :aria-labelledby="modalId + 'Label'">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" v-show="choose">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title title" :id="modalId + 'Label'"><@spring.message "modal.title.uploadImage"/></h4>
                </div>
                <div class="modal-body">
                    <div class="text-center" data-provides="fileinput" style="width: 100%">
                        <h4><@spring.message "upload.image.description1"/></h4>

                        <span class="btn btn-raised  btn-primary btn-file">
                                <span><@spring.message "button.label.chooseFile"/></span>
                                <input id="imageSelector" type="file" @change="onFileChange">
                            </span>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <h4>
                        <small><@spring.message "upload.image.description2"/>
                        </small>
                    </h4>
                </div>
            </div>
            <div class="modal-content" v-show="!choose">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title title" :id="modalId + 'Label'"><@spring.message "modal.title.photoSetting"/></h4>
                </div>
                <div class="modal-body text-center">
                    <h4><@spring.message "upload.image.description3"/></h4>

                    <div style="width: 100%;">
                        <img id="selectedPhoto" style="width: 100%;">
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal"><@spring.message "button.label.cancel"/></button>
                    <button type="button" class="btn btn-primary"
                            @click="saveImage"><@spring.message 'button.label.save'/></button>
                </div>
            </div>
        </div>
    </div>
</script>
