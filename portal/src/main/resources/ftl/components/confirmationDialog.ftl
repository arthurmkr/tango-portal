<#import "/spring.ftl" as spring/>

<div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">

            <div class="modal-header text-center">
                <h4 class="title">{{message}}</h4>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-success btn-simple"
                        data-dismiss="modal"><@spring.message "button.label.no"/>
                </button>
                <button type="button" class="btn btn-danger btn-simple" @click="processYes"><@spring.message "button.label.yes"/></button>
            </div>
        </div>

    </div>
</div>
