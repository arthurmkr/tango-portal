<#import "../util.ftl" as util/>
<#import "/spring.ftl" as spring/>

<script type="text/x-template" id="milonga-list">
    <div class="container-fluid">
        <div class="row">
            <milonga-list-table title="<@spring.message "milonga.scheduledMilongas"/>"
                                :items="planned"
                                :edit-enabled="true"
                                :remove-enabled="true"
                                :repeat-enabled="true"
                                :onAdd="addMilonga"
            ></milonga-list-table>
        </div>

        <div class="row">
            <milonga-list-table title="<@spring.message "milonga.completedMilongas"/>"
                                :items="completed"
                                :repeat-enabled="true">

            </milonga-list-table>
        </div>
    </div>
</script>

<script type="text/x-template" id="milonga-list-table">
    <div class="col-md-12 tp-block">
        <div class="tp-block-header">
            <h3>{{title}}</h3>
            <button class="btn btn-success btn-fab  btn-round" @click="onAdd">
                <i class="material-icons">add</i>
            </button>
        </div>

        <table class="table rows-hovered milonga-list">
            <colgroup>
                <col style="min-width: 140px">
                <col style="width: 40%">
                <col style="width: 60%">
                <col style="min-width: 200px">
                <col class="td-actions-3">
            </colgroup>
            <thead>
            <tr>
                <th><@spring.message "common.dateTime"/></th>
                <th><@spring.message "common.name"/></th>
                <th><@spring.message "common.address"/></th>
                <th><@spring.message "common.dj"/></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="milonga in items">
                <td class="datetime-td"><span>{{milonga.startDate}}</span> <span>{{milonga.startTime}} - {{milonga.endTime}}</span>
                </td>
                <td>{{milonga.name}}</td>
                <td>{{milonga.address.value}}</td>
                <td>{{milonga.djFullName}}</td>
                <td class="td-actions">
                    <button v-if="repeatEnabled" type="button" rel="tooltip" class="btn btn-success"
                            @click="repeatMilonga(milonga)"
                            title="<@spring.message 'button.label.repeat'/>">
                        <i class="material-icons">refresh</i>
                    </button>
                    <button v-if="editEnabled" type="button" rel="tooltip" class="btn btn-info"
                            @click="editMilonga(milonga)"
                            title="<@spring.message 'button.label.edit'/>">
                        <i class="material-icons">edit</i>
                    </button>
                    <button v-if="removeEnabled" type="button" rel="tooltip" class="btn btn-danger"
                            @click="removeMilonga(milonga)"
                            title="<@spring.message 'button.label.delete'/>">
                        <i class="material-icons">close</i>
                    </button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</script>


<script type="text/x-template" id="milonga-modal">
    <@util.modalForm id="milonga">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-top: 0; margin-bottom: -30px">
                <image-cropper id="milongaPhoto" v-model="obj.photo" :event="EventType.MILONGA_PHOTO_UPLOADED"
                               blank-image="img/insert_photo_grey.png"></image-cropper>
            </div>
        </div>
        <div class="col-md-6">
            <div class="required form-group has-feedback is-empty label-floating">
                <label for="name" class="control-label"><@spring.message "common.name"/></label>

                <input id="name" name="name" type="text" class="form-control" v-model="obj.name" required
                       autocomplete="off"/>
            </div>
            <div class="required form-group has-feedback tp-select-block tp-image-select">
                <label for="dj" class="control-label"><@spring.message "common.dj"/></label>
                <tp-select name="dj" v-model="obj.djId" :items="djs"
                           placeholder="<@spring.message 'placeholders.chooseDj'/>"
                           :live-search="true" live-search-placeholder="<@spring.message 'placeholders.nameOfDj'/>">
                    <template scope="props">
                        <option :value="props.item.id">
                            <img :src="props.item.avatar ? props.item.avatar : 'img/person_grey.png'">
                            {{props.item.fullName}}
                        </option>

                    </template>
                </tp-select>
            </div>

            <div class="form-group has-feedback is-empty label-floating">
                <label for="fbUrl" class="control-label"><@spring.message "common.facebook"/></label>

                <input id="fbUrl" name="fbUrl" type="url" class="form-control" v-model="obj.fbUrl" autocomplete="off"/>
            </div>
            <div class="form-group has-feedback is-empty label-floating">
                <label for="vkUrl" class="control-label"><@spring.message "common.vkontakte"/></label>

                <input id="vkUrl" name="vkUrl" type="url" class="form-control" v-model="obj.vkUrl" autocomplete="off"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <div class="required form-group has-feedback is-empty label-floating">
                <label for="address" class="control-label"><@spring.message "common.address"/></label>

                <tp-address name="address" v-model="obj.address" :required="true"></tp-address>
            </div>
        </div>
        <div class="col-md-2">
            <div class="required form-group has-feedback is-empty label-floating">
                <label for="cost" class="control-label"><@spring.message "activity.cost"/></label>

                <input id="cost" type="text" class="form-control" maxlength="4" v-model="obj.cost"
                       v-mask="'0000'" autocomplete="off" required/>
            </div>
        </div>
        <div class="col-md-2">
            <div class="required form-group has-feedback tp-select-block">
                <label for="currency" class="control-label"><@spring.message "common.currency"/></label>
                <tp-select name="currency" v-model="obj.currency" :items="['RUB', 'USD', 'EUR', 'GEL']"
                           placeholder="<@spring.message 'placeholders.chooseCurrency'/>">
                    <template scope="props">
                        <option :value="props.item">
                            {{props.item}}
                        </option>
                    </template>
                </tp-select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="required form-group has-feedback is-empty label-floating">
                <label for="startDate" class="control-label"><@spring.message "milonga.startDate"/></label>

                <tp-date name="startDate" v-model="obj.startDate" :required="true"></tp-date>
            </div>
        </div>
        <div class="col-md-4">
            <div class="required form-group has-feedback is-empty label-floating">
                <label for="startTime" class="control-label"><@spring.message "activity.startTime"/></label>

                <tp-time name="startTime" v-model="obj.startTime" :required="true"></tp-time>
            </div>
        </div>
        <div class="col-md-4">
            <div class="required form-group has-feedback is-empty label-floating">
                <label for="endTime" class="control-label"><@spring.message "common.endTime"/></label>

                <tp-time name="endTime" v-model="obj.endTime" :required="true"></tp-time>
            </div>
        </div>
    </div>
    </@util.modalForm>
</script>