<#import "../util.ftl" as util/>
<#import "/spring.ftl" as spring/>

<script type="text/x-template" id="practice-modal">
    <@util.modalForm id="practice">

    <div class="required form-group has-feedback is-empty label-floating">
        <label for="name" class="control-label"><@spring.message "common.name"/></label>

        <input id="name" type="text" class="form-control" v-model="obj.name" required autocomplete="off"/>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group has-feedback is-empty label-floating">
                <label for="cost" class="control-label"><@spring.message "activity.cost"/></label>

                <input id="cost" type="text" class="form-control" maxlength="4" v-model="obj.cost"
                       v-mask="'0000'" autocomplete="off"/>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="required form-group has-feedback tp-select-block">
                <label for="room" class="control-label"><@spring.message "activity.room"/></label>
                <tp-select name="room" v-model="obj.roomId" :items="rooms">
                    <template scope="props">
                        <option :value="props.item.id">
                            {{props.item.name}}
                        </option>
                    </template>
                </tp-select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="required form-group has-feedback is-empty label-floating">
                <label for="startTime" class="control-label"><@spring.message "activity.startTime"/></label>

                <tp-time name="startTime" v-model="obj.startTime" :required="true"></tp-time>
            </div>

            <div class="required form-group has-feedback is-empty label-floating">
                <label for="duration" class="control-label"><@spring.message "activity.duration"/></label>

                <input id="duration" name="duration" type="text" class="form-control" maxlength="3" v-model="obj.duration" required
                       v-mask="'000'" autocomplete="off"/>
            </div>

            <div class="required form-group has-feedback is-empty label-floating">
                <label for="startDate" class="control-label"><@spring.message "activity.repeatFrom"/></label>

                <tp-date name="startDate" v-model="obj.startDate" :to-value="obj.endDate" :required="true"></tp-date>
            </div>

            <div class="form-group has-feedback is-empty label-floating">
                <label for="endDate" class="control-label"><@spring.message "activity.repeatTo"/></label>

                <tp-date name="endDate" v-model="obj.endDate" :end-of-range="true" :from-value="obj.startDate"></tp-date>
            </div>
        </div>
        <div class="col-md-6">
            <@util.daysOfWeek/>
        </div>
    </div>

    </@util.modalForm>
</script>