<#import "../util.ftl" as util/>
<#import "/spring.ftl" as spring/>

<#--<script type="text/x-template" id="school-modal">-->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-login">
        <div class="modal-content">
            <div class="card card-signup card-plain">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="material-icons">clear</i></button>

                    <div class="header header-primary text-center">
                        <h4 class="card-title" id="loginModalLabel"><@spring.message "modal.title.login"/></h4>
                    </div>
                </div>
                <div class="modal-body">
                    <form class="form" id="loginForm" role="form">
                        <div class="card-content">
                            <div class="alert alert-warning" v-if="error">
                                <div class="container">
                                    <div class="alert-icon">
                                        <i class="material-icons">warning</i>
                                    </div>

                                    <span>{{'ERROR_' + error | translate}}</span>
                                </div>
                            </div>

                            <div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">email</i>
								</span>
                                <div class="form-group is-empty">
                                    <input class="form-control"
                                           type="text" v-model="username" placeholder="<@spring.message 'common.email'/>">
                                    <span class="material-input"></span>
                                </div>
                            </div>

                            <div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">lock_outline</i>
								</span>
                                <div class="form-group is-empty">
                                    <input class="form-control"
                                           type="password" v-model="password" placeholder="<@spring.message 'common.password'/>">
                                    <span class="material-input"></span>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer text-center">
                            <button class="btn btn-primary btn-simple btn-wd btn-lg" type="submit"><@spring.message "button.label.login"/>
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<#--</script>-->