<#import "/spring.ftl" as spring/>

<div class="container-fluid profile-cabinet-page">

    <div class="row">
        <div class="col-sm-3">
            <img :src="profile.avatar" v-blank-image class="img-raised" style="width: 100%">
        </div>
        <div class="col-md-9" v-if="profile != null">
            <h2>{{profile.firstName}} {{profile.secondName}} <a @click="editProfile"><@spring.message "link.label.edit"/></a></h2>

            <p class="profile-item"><span><@spring.message "common.city"/></span> <span v-if="profile.city != null">{{profile.city.name}}</span></p>
            <p class="profile-item"><span><@spring.message "common.facebook"/></span> <a :href="profile.fbProfile">{{profile.fbProfile}}</a></p>
            <p class="profile-item"><span><@spring.message "common.vkontakte"/></span> <a :href="profile.vkProfile">{{profile.vkProfile}}</a></p>

            <button type="button" class="btn btn-success btn-lg" data-toggle="modal"
                    data-target="#schoolModal" v-if="!hasSchool && (profile.roles.schoolLead || profile.roles.schoolAdmin)">
                <i class="fa fa-plus"></i> <@spring.message "button.label.addSchool"/>
            </button>
            <button id="facebookOauthUrl" v-if="profile.roles.admin" target="_blank" @click="getOauthFacebookUrl" class="btn btn-success btn-lg">
                <i class="fa fa-facebook"></i>
                <@spring.message "button.label.updateTokenFb"/>
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-warning" data-toggle="modal"
                    data-target="#changePasswordModal">
                <i class="material-icons">vpn_key</i> <@spring.message "button.label.changePassword"/>
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <referral-list></referral-list>
        </div>
    </div>
</div>
