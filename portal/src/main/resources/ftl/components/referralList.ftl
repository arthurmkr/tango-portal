<#import "../util.ftl" as util/>
<#import "/spring.ftl" as spring/>


<script type="text/x-template" id="referral-list">
    <div>
        <div class="tp-block-header">
            <h3><@spring.message "block.title.invitedUsers"/></h3>
        </div>

        <div class="row">
            <div class="col-md-2">
                <button type="button" rel="tooltip" class="btn btn-success" @click="getReferralLink"
                        v-if="!referralLink">
                <@spring.message "button.label.getReferralLink"/>
                </button>

                <button type="button" rel="tooltip" class="btn btn-success" @click="copyReferralLink"
                        v-if="referralLink">
                <@spring.message "button.label.copy"/>
                </button>
            </div>

            <div class="col-md-10 referral-link-block" v-if="referralLink">
                <input type="text" class="referral-link" readonly id="referralLink" v-model="referralLink">
            </div>
        </div>
        <table class="table table-striped rows-hovered" v-if="items.length">
            <colgroup>
                <col style="width: 33%">
                <col style="width: 33%">
                <col style="width: 33%">
                <col class="td-actions">
            </colgroup>
            <thead>
            <tr>
                <th><@spring.message "common.user"/></th>
                <th><@spring.message "common.vkProfile"/></th>
                <th><@spring.message "common.fbProfile"/></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="referral in items">
                <td>
                    {{referral.firstName}} {{referral.secondName}}
                </td>
                <td>
                    <a :href="referral.vkProfile">{{referral.vkProfile}}</a>
                </td>
                <td>
                    <a :href="referral.fbProfile">{{referral.fbProfile}}</a>
                </td>
                <td class="td-actions">
                    <button type="button" rel="tooltip" class="btn btn-success" @click="approveItem(referral)">
                        <i class="material-icons">done</i>
                    </button>
                    <button type="button" rel="tooltip" class="btn btn-danger" @click="removeItem(referral)">
                        <i class="material-icons">close</i>
                    </button>

                </td>
            </tr>
            </tbody>
        </table>
    </div>
</script>
