<#import "../util.ftl" as util/>
<#import "/spring.ftl" as spring/>

<script type="text/x-template" id="room-list">
    <div>
        <div class="tp-block-header">
            <h3><@spring.message "common.rooms"/></h3>
            <button class="btn btn-success btn-fab  btn-round"
                    data-toggle="modal" data-target="#roomModal">
                <i class="material-icons">add</i>
            </button>
        </div>

        <table class="table rows-hovered room-list">
            <colgroup>
                <col style="width: 150px">
                <col>
                <col class="td-actions">
            </colgroup>
            <thead>
            <tr>
                <th><@spring.message "common.name"/></th>
                <th><@spring.message "common.address"/></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="room in items">
                <td>{{room.name}}</td>
                <td>{{room.address.value}}</td>
                <td class="td-actions">
                    <button type="button" rel="tooltip" class="btn btn-info" @click="editItem(room)">
                        <i class="material-icons">edit</i>
                    </button>
                    <button type="button" rel="tooltip" class="btn btn-danger" @click="removeItem(room)">
                        <i class="material-icons">close</i>
                    </button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</script>


<script type="text/x-template" id="room-modal">
    <@util.modalForm id="room">

    <div class="required form-group has-feedback is-empty label-floating">
        <label for="name" class="control-label"><@spring.message "common.name"/></label>

        <input id="name" name="name" type="text" class="form-control" v-model="obj.name" required
               autocomplete="off"/>
    </div>

    <div class="required form-group has-feedback is-empty label-floating">
        <label for="address" class="control-label"><@spring.message "room.address"/></label>

        <tp-address name="address" v-model="obj.address" :required="true"></tp-address>
    </div>

    </@util.modalForm>
</script>