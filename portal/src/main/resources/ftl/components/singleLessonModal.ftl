<#import "../util.ftl" as util/>
<#import "/spring.ftl" as spring/>

<script type="text/x-template" id="single-lesson-modal">
    <@util.modalForm id="singleLesson">

    <div class="required form-group has-feedback is-empty label-floating">
        <label for="name" class="control-label"><@spring.message "common.name"/></label>

        <input id="name" type="text" class="form-control" v-model="obj.name" required autocomplete="off"/>
    </div>
    <div class="required form-group has-feedback tp-select-block">
        <label for="teachers" class="control-label"><@spring.message "common.teachers"/></label>

        <tp-select name="teachers" v-model="obj.teachers" :items="teachers" :multiple="true" max-options="2"
                   placeholder="<@spring.message 'placeholders.teachers'/>">
            <template scope="props">
                <option :value="props.item.id">
                    {{props.item.firstName}} {{props.item.secondName}}
                </option>

            </template>
        </tp-select>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group has-feedback is-empty label-floating">
                <label for="cost" class="control-label"><@spring.message "activity.cost"/></label>

                <input id="cost" type="text" class="form-control" maxlength="4" v-model="obj.cost"
                       v-mask="'0000'" autocomplete="off"/>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="required form-group has-feedback tp-select-block">
                <label for="room" class="control-label"><@spring.message "activity.room"/></label>
                <tp-select name="room" v-model="obj.roomId" :items="rooms">
                    <template scope="props">
                        <option :value="props.item.id">
                            {{props.item.name}}
                        </option>
                    </template>
                </tp-select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="required form-group has-feedback is-empty label-floating">
                <label for="startTime" class="control-label"><@spring.message "activity.startTime"/></label>

                <tp-time name="startTime" v-model="obj.startTime" :required="true"></tp-time>
            </div>

            <div class="required form-group has-feedback is-empty label-floating">
                <label for="duration" class="control-label"><@spring.message "activity.duration"/></label>

                <input id="duration" name="duration" type="text" class="form-control" maxlength="3" v-model="obj.duration" required
                       v-mask="'000'"/>
            </div>

            <div class="required form-group has-feedback is-empty label-floating">
                <label for="startDate" class="control-label"><@spring.message "activity.lessonDate"/></label>

                <tp-date name="startDate" v-model="obj.startDate" :required="true"></tp-date>
            </div>
        </div>
    </div>

    </@util.modalForm>
</script>