<#import "../util.ftl" as util/>
<#import "/spring.ftl" as spring/>

<script type="text/x-template" id="change-password-modal">
    <div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-hidden="true"
         aria-labelledby="changePasswordModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="title modal-title" id="changePasswordModalLabel"><@spring.message "modal.title.changePassword"/></h4>
                </div>
                <form id="changePasswordForm" role="form">
                    <div class="modal-body">
                        <div class="alert alert-warning" v-if="error">
                                <div class="alert-icon">
                                    <i class="material-icons">warning</i>
                                </div>

                                <span>{{'ERROR_' + error | translate}}</span>
                        </div>

                        <input type="hidden" :value="unique">
                        <div class="required form-group has-feedback is-empty label-floating">
                            <label for="oldPassword" class="control-label"><@spring.message "changePassword.oldPassword"/></label>

                            <input id="oldPassword" name="oldPassword" type="password" class="form-control"
                                   v-model="obj.oldPassword" required autocomplete="off"/>
                        </div>

                        <div class="required form-group has-feedback is-empty label-floating">
                            <label for="newPassword" class="control-label"><@spring.message "changePassword.newPassword"/></label>

                            <input id="newPassword" name="newPassword" type="password" class="form-control"
                                   v-model="obj.newPassword" required autocomplete="off"/>
                        </div>

                        <div class="required form-group has-feedback is-empty label-floating">
                            <label for="confirmPassword" class="control-label"><@spring.message "changePassword.confirmPassword"/></label>

                            <input id="confirmPassword" name="confirmPassword" type="password" class="form-control"
                                   v-model="obj.confirmPassword" required autocomplete="off"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal"><@spring.message "button.label.cancel"/></button>
                        <button type="submit" class="btn btn-primary"><@spring.message 'button.label.change'/></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</script>
