<#import "../util.ftl" as util/>
<#import "/spring.ftl" as spring/>

<script type="text/x-template" id="school-modal">
    <@util.modalForm id="school">

    <div class="form-group" style="margin-top: 0; margin-bottom: -30px">
        <image-cropper id="schoolPhoto" v-model="obj.photo" :event="EventType.SCHOOL_PHOTO_UPLOADED"></image-cropper>
    </div>

    <div class="required form-group has-feedback is-empty label-floating">
        <label for="name" class="control-label"><@spring.message "common.name"/></label>

        <input id="name" name="name" type="text" class="form-control" v-model="obj.name" required autocomplete="off"/>
    </div>

    <div class="required form-group has-feedback is-empty label-floating">
        <label for="address" class="control-label"><@spring.message "room.address"/></label>

        <tp-address name="address" v-model="obj.address" :required="true"></tp-address>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group has-feedback is-empty label-floating">
                <label for="workPhone" class="control-label"><@spring.message "school.workPhone"/></label>

                <input id="workPhone" name="workPhone" type="text" class="form-control" v-model="obj.workPhone"
                       maxlength="25" pattern="^[\d\+\-\(\)\s]{5,}" autocomplete="off"/>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="required form-group has-feedback is-empty label-floating">
                <label for="mobilePhone" class="control-label"><@spring.message "school.mobilePhone"/></label>

                <input id="mobilePhone" type="text" name="mobilePhone" class="form-control" v-model="obj.mobilePhone"
                       required
                       maxlength="25"
                       pattern="^[\d\+\-\(\)\s]{5,}" autocomplete="off"/>
            </div>
        </div>
    </div>

    <div class="form-group has-feedback is-empty label-floating">
        <label for="siteUrl" class="control-label"><@spring.message "school.siteUrl"/></label>

        <input id="siteUrl" name="siteUrl" type="url" class="form-control" v-model="obj.siteUrl" autocomplete="off"/>
    </div>
    <div class="form-group has-feedback is-empty label-floating">
        <label for="fbUrl" class="control-label"><@spring.message "common.facebook"/></label>

        <input id="fbUrl" name="fbUrl" type="url" class="form-control" v-model="obj.fbUrl" autocomplete="off"/>
    </div>
    <div class="form-group has-feedback is-empty label-floating">
        <label for="vkUrl" class="control-label"><@spring.message "common.vkontakte"/></label>

        <input id="vkUrl" name="vkUrl" type="url" class="form-control" v-model="obj.vkUrl" autocomplete="off"/>
    </div>
    </@util.modalForm>
</script>
