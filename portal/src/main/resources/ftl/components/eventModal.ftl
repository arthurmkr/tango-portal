<#import "../util.ftl" as util/>
<#import "/spring.ftl" as spring/>

<script type="text/x-template" id="event-modal">
    <@util.modalForm id="event">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-top: 0; margin-bottom: -30px">
                <image-cropper id="eventPhoto" v-model="obj.photo"
                               :event="EventType.EVENT_PHOTO_UPLOADED"></image-cropper>
            </div>
        </div>
        <div class="col-md-6">
            <div class="required form-group has-feedback is-empty label-floating">
                <label for="name" class="control-label"><@spring.message "common.name"/></label>

                <input id="name" name="name" type="text" class="form-control" v-model="obj.name" required
                       autocomplete="off"/>
            </div>

            <div class="required form-group has-feedback is-empty label-floating">
                <label for="city" class="control-label"><@spring.message "common.city"/></label>

                <tp-city name="city" v-model="obj.city" :required="true"></tp-city>
            </div>

            <div class="required form-group has-feedback is-empty label-floating">
                <label for="startDate" class="control-label"><@spring.message "event.startDate"/></label>

                <tp-date name="startDate" v-model="obj.startDate" :to-value="obj.endDate" :required="true"></tp-date>
            </div>

            <div class="required form-group has-feedback is-empty label-floating">
                <label for="endDate" class="control-label"><@spring.message "event.endDate"/></label>

                <tp-date name="endDate" v-model="obj.endDate" :end-of-range="true" :required="true"
                         :from-value="obj.startDate"></tp-date>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group has-feedback is-empty label-floating">
                <label for="siteUrl" class="control-label"><@spring.message "school.siteUrl"/></label>

                <input id="siteUrl" name="siteUrl" type="url" class="form-control" v-model="obj.siteUrl"
                       autocomplete="off"/>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group has-feedback is-empty label-floating">
                <label for="fbUrl" class="control-label"><@spring.message "common.facebook"/></label>

                <input id="fbUrl" name="fbUrl" type="url" class="form-control" v-model="obj.fbUrl" autocomplete="off"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group has-feedback is-empty label-floating">
                <label for="vkUrl" class="control-label"><@spring.message "common.vkontakte"/></label>

                <input id="vkUrl" name="vkUrl" type="url" class="form-control" v-model="obj.vkUrl" autocomplete="off"/>
            </div>
        </div>
    </div>

    </@util.modalForm>
</script>
