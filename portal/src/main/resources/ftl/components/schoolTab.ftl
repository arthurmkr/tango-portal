<#import "/spring.ftl" as spring/>

<div class="container-fluid">
    <img :src="school.info.photo" class="img-raised" style="width: 100%" v-if="school.info.photo">

    <div class="row">
        <div class="col-md-12 tp-block">
            <div class="tp-block-header">
                <h3><@spring.message "block.title.commonSchoolInfo"/></h3>
                <button class="btn btn-info btn-fab btn-round" @click="editSchool">
                    <i class="material-icons">edit</i>
                </button>
            </div>

            <p class="profile-item"><span><@spring.message "school.name"/></span> {{school.info.name}}</p>
            <p class="profile-item"><span><@spring.message "school.workPhone"/></span> {{school.info.workPhone}}</p>
            <p class="profile-item"><span><@spring.message "school.mobilePhone"/></span> {{school.info.mobilePhone}}</p>
            <p class="profile-item" v-if="school.info.address != null"><span><@spring.message "common.address"/></span> {{school.info.address.value}}</p>
            <p class="profile-item"><span><@spring.message "school.siteUrl"/></span> <a :href="school.info.siteUrl">{{school.info.siteUrl}}</a></p>
            <p class="profile-item"><span><@spring.message "common.facebook"/></span> <a :href="school.info.fbUrl">{{school.info.fbUrl}}</a></p>
            <p class="profile-item"><span><@spring.message "common.vkontakte"/></span> <a :href="school.info.vkUrl">{{school.info.vkUrl}}</a></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7 tp-block">
            <room-list :table-config="configs.schoolRoom"></room-list>
        </div>
        <div class="col-md-5 tp-block">
            <teacher-list></teacher-list>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="tp-block-header">
                <h3><@spring.message "common.timetable"/></h3>
            </div>

            <div class="container-fluid">
                <div class="col-md-12">
                    <form class="form">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group has-feedback is-empty label-floating">
                                    <label for="from" class="control-label"><@spring.message "common.fromDate"/></label>
                                    <tp-date name="from" v-model="filter.from" :disabled-days="[0, 2, 3, 4, 5, 6]"></tp-date>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <table class="table table-striped timetable tp-block" :class="roomColumnClass"
                   v-for="day in school.timetable.days">
                <colgroup>
                    <col v-for="room in school.timetable.rooms">
                </colgroup>
                <thead>
                <tr class="bg-primary ">
                    <th>
                        {{day.dayOfWeek | translate}}
                    </th>
                    <th class="room" v-for="(room, index) in school.timetable.rooms">
                        <div class="tp-block-header">
                            {{room}}
                            <button class="btn btn-success btn-fab btn-round"
                                    data-toggle="modal"
                                    data-target="#chooseSchoolActivityModal"
                                    @click="selectDayOfWeek = day.dayOfWeek"
                            v-if="index == school.timetable.rooms.length -1">
                                <i class="material-icons">add</i>
                            </button>
                        </div>
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="row in day.rows" class="timetable-row">
                    <td>{{row.startTime}} - {{row.endTime}}</td>
                    <td v-for="item in row.items">
                        <div class="card card-profile card-plain" v-if="item != null">
                            <div class="card-content">
                                <h4 class="card-title">{{item.name}}</h4>

                                <p class="card-description">
                                    <span v-if="item.firstTeacherId != null">{{school.timetable.teachers[item.firstTeacherId]}}
                                    <span v-if="item.secondTeacherId != null"> / {{school.timetable.teachers[item.secondTeacherId]}}</span>
                                    </span>
                                </p>

                                <div class="event-actions">
                                    <button type="button"  class="btn btn-info" @click="editTimetableItem(item)">
                                        <i class="material-icons">edit</i>
                                    </button>
                                    <button type="button" class="btn btn-danger" @click="removeActivity(item)">
                                        <i class="material-icons">close</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>
