<#ftl encoding="UTF-8">

<#include "template.ftl"/>

<#macro components>
    <#include "components/form.ftl"/>

</#macro>

<#macro page_content>
<div class="wrapper" id="djList" v-cloak>

    <div class="container page-filter-panel">
        <div class="main main-raised">
            <div class="row">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <h2 class="title"><@spring.message "page.title.djs"/></h2>
                        <form class="form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback is-empty label-floating">
                                        <label for="city" class="control-label"><@spring.message "common.city"/></label>
                                        <tp-city name="city" v-model="filter.city"></tp-city>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="map" style="height: 500px;">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container" v-if="isEmpty && firstLoaded">
        <div class="row">
            <div class="col-md-12">
                <div class="card text-center">
                    <div class="card-content content-warning">
                        <h3 class="card-title"><@spring.message "error.dj.notFoundInCity"/> {{filter.city.name}}.</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="grid">
            <div v-for="item in pageData.items" class="grid-item col-xs-12 col-md-3">
                <div class="grid-item-content">
                    <div class="card text-center">
                        <img :src="item.avatar" v-if="item.avatar" class="user-avatar">
                        <img src="/img/person_grey.png" v-if="!item.avatar" class="user-avatar">
                        <div class="card-content">
                            <h3 class="card-title">
                                {{item.firstName}} {{item.secondName}}
                            </h3>
                            <h4 class="card-title" v-if="item.djNick">
                                DJ {{item.djNick}}
                            </h4>
                            <h6 class="card-title" v-if="item.city">
                                {{item.city}}
                            </h6>
                            <div class="footer">
                                <a v-if="item.fbProfile" :href="item.fbProfile" target="_blank"
                                   class="btn btn-just-icon btn-facebook btn-round"><i
                                        class="fa fa-facebook-square"></i></a>
                                <a v-if="item.vkProfile" :href="item.vkProfile" target="_blank"
                                   class="btn btn-just-icon btn-vk btn-round"><i
                                        class="fa fa-vk"></i></a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>

<@display_page "initDjListPage"/>