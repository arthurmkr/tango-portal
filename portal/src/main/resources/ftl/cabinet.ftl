<#ftl encoding="UTF-8">

<#include "template.ftl"/>
<#include "util.ftl"/>


<#macro components>
    <#include "components/form.ftl"/>
    <#include "components/teacherList.ftl"/>
    <#include "components/profileModal.ftl"/>
    <#include "components/schoolModal.ftl"/>
    <#include "components/eventModal.ftl"/>
    <#include "components/regularLessonModal.ftl"/>
    <#include "components/singleLessonModal.ftl"/>
    <#include "components/practiceModal.ftl"/>
    <#include "components/changePasswordModal.ftl"/>
    <#include "components/roomList.ftl"/>
    <#include "components/referralList.ftl"/>
    <#include "components/milongas.ftl"/>
    <#include "components/eventTab.ftl"/>
    <#include "components/adminTab.ftl"/>

</#macro>

<#macro page_content>
<div class="wrapper" id="cabinet" v-cloak>
    <div class="container">
        <div class="main main-raised">

            <ul class="nav nav-pills nav-pills-icons" role="tablist">
                <li class="active">
                    <a href="#profileTab" role="tab" data-toggle="tab" aria-expanded="false">
                        <i class="material-icons">account_box</i>
                        <@spring.message "tab.label.profile"/>
                    </a>
                </li>
                <li v-if="hasSchool && (profile.roles.schoolLead || profile.roles.schoolAdmin)">
                    <a href="#schoolTab" role="tab" data-toggle="tab" aria-expanded="true" >
                        <i class="material-icons">school</i>
                        <@spring.message "tab.label.school"/>
                    </a>
                </li>
                <li  v-show="profile.roles.organizer">
                    <a id="milongasTabLink" href="#milongasTab" role="tab" data-toggle="tab" aria-expanded="true">
                        <i class="material-icons">group</i>
                        <@spring.message "tab.label.milongas"/>
                    </a>
                </li>
                <li  v-show="profile.roles.organizer">
                    <a id="eventsTabLink" href="#eventsTab" role="tab" data-toggle="tab" aria-expanded="true">
                        <i class="material-icons">assistant_photo</i>
                        <@spring.message "tab.label.events"/>
                    </a>
                </li>
                <li  v-show="profile.roles.admin">
                    <a id="adminTabLink" href="#adminTab" role="tab" data-toggle="tab" aria-expanded="true">
                        <i class="material-icons">settings</i>
                        <@spring.message "tab.label.admin"/>
                    </a>
                </li>
            </ul>
            <div class="tab-content tab-space">
                <div role="tabpanel" class="tab-pane active" id="profileTab">
                    <#include "components/profileTab.ftl"/>
                </div>
                <div role="tabpanel" class="tab-pane" id="schoolTab" v-if="hasSchool">
                    <#include "components/schoolTab.ftl"/>
                </div>
                <div role="tabpanel" class="tab-pane" id="milongasTab">
                    <#include "components/milongasTab.ftl"/>
                </div>
                <div role="tabpanel" class="tab-pane" id="eventsTab">
                    <event-tab></event-tab>
                </div>
                <div role="tabpanel" class="tab-pane" id="adminTab">
                    <#include "components/adminTab.ftl"/>
                </div>
            </div>
        </div>
    </div>

    <room-modal></room-modal>
    <school-modal></school-modal>
    <event-modal></event-modal>
    <teacher-modal></teacher-modal>
    <profile-modal></profile-modal>
    <regular-lesson-modal></regular-lesson-modal>
    <single-lesson-modal></single-lesson-modal>
    <practice-modal></practice-modal>
    <milonga-modal></milonga-modal>
    <change-password-modal></change-password-modal>
    <image-select-modal id="profileAvatar" :ratio="1" size="AVATAR" :event="EventType.PROFILE_AVATAR_UPLOADED"></image-select-modal>
    <image-select-modal id="teacherAvatar" :ratio="1" size="AVATAR" :event="EventType.TEACHER_AVATAR_UPLOADED"></image-select-modal>
    <image-select-modal id="schoolPhoto" :ratio="3" size="SCHOOL_PHOTO" :event="EventType.SCHOOL_PHOTO_UPLOADED"></image-select-modal>
    <image-select-modal id="eventPhoto" :ratio="16/9" size="EVENT_PHOTO" :event="EventType.EVENT_PHOTO_UPLOADED"></image-select-modal>
    <image-select-modal id="milongaPhoto" :ratio="16/9" size="MILONGA_PHOTO" :event="EventType.MILONGA_PHOTO_UPLOADED"></image-select-modal>



    <div class="modal" id="chooseSchoolActivityModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content text-center">
                <div class="modal-header">
                    <h4 class="modal-title"><@spring.message "modal.title.schoolActivity"/></h4>
                </div>
                <div class="modal-body wide-buttons">
                    <button class="btn btn-lg btn-success" role="button" @click="addRegularLesson">
                        <@spring.message "activity.regularLesson"/>
                    </button>
                    <button class="btn btn-lg btn-primary" role="button" @click="addSingleSchoolLesson">
                        <@spring.message "activity.singleLesson"/>
                    </button>
                    <button class="btn btn-lg btn-info" role="button" @click="addPractice">
                        <@spring.message "activity.practice"/>
                    </button>
                </div>
            </div>
        </div>
    </div>

</div>
</#macro>

<@display_page "initCabinetPage"/>