<#ftl encoding="UTF-8">

<#include "template.ftl"/>
<#import "util.ftl" as util/>

<#macro page_content>
<div class="wrapper" id="register" v-cloak>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2" v-if="!result">
                <div class="main main-raised">
                    <div class="container-fluid">

                        <h2 class="title"><@spring.message "page.title.registration"/></h2>

                        <form id="registerForm" role="form">
                            <div class="alert alert-warning" v-if="error">
                                <div class="container">
                                    <div class="alert-icon">
                                        <i class="material-icons">warning</i>
                                    </div>

                                    <span>{{'ERROR_' + error | translate}}</span>
                                </div>
                            </div>

                            <input type="hidden" id="inviteToken" value="${inviteToken}">
                            <div class="col-md-6">
                                <div class="required form-group has-feedback is-empty label-floating">
                                    <label for="email" class="control-label"><@spring.message "common.email"/></label>

                                    <input id="email" name="email" type="email" class="form-control"
                                           v-model="obj.email" required autocomplete="off"/>
                                </div>


                                <div class="required form-group has-feedback is-empty label-floating">
                                    <label for="firstName"
                                           class="control-label"><@spring.message "common.firstName"/></label>

                                    <input id="firstName" name="firstName" type="text" class="form-control"
                                           v-model="obj.firstName" required autocomplete="off"/>
                                </div>

                                <div class="required form-group has-feedback is-empty label-floating">
                                    <label for="secondName"
                                           class="control-label"><@spring.message "common.secondName"/></label>

                                    <input id="secondName" name="secondName" type="text" class="form-control"
                                           v-model="obj.secondName"
                                           required autocomplete="off"/>
                                </div>

                                <div class="form-group has-feedback is-empty label-floating">
                                    <label for="vkProfile"
                                           class="control-label"><@spring.message "common.vkProfile"/></label>

                                    <input id="vkProfile" name="vkProfile" type="url" class="form-control"
                                           v-model="obj.vkProfile" autocomplete="off"/>
                                </div>

                                <div class="form-group has-feedback is-empty label-floating">
                                    <label for="fbProfile"
                                           class="control-label"><@spring.message "common.fbProfile"/></label>

                                    <input id="fbProfile" name="fbProfile" type="url" class="form-control"
                                           v-model="obj.fbProfile" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <@util.roles/>
                            </div>
                            <div class="col-md-12">
                                <div class="text-center">
                                    <button type="submit"
                                            class="btn btn-primary"><@spring.message "button.label.send"/></button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-offset-3" v-if="result">
                <div class="card">
                    <div class="card-content content-success">
                        <h4 class="card-title">
                            <@spring.message "page.registration.requestAccepted"/>
                        </h4>
                        <p class="card-description">
                            <@spring.message "page.registration.weSendEmail"/> <strong>{{obj.email}}</strong>.
                        </p>
                        <p class="card-description">
                            <@spring.message "page.registration.hintForEmailConfirmation"/>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</#macro>

<@display_page "initRegisterPage"/>