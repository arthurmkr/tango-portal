<#ftl encoding="UTF-8">

<#include "template.ftl"/>
<#import "spring.ftl" as spring/>

<#macro page_content>
<div class="wrapper" id="login">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4 col-md-offset-4">
                <div class="main main-raised">
                    <div class="container-fluid">
                        <form role="form" id="loginPageForm" action="/login" method="POST" class="login-form">
                            <h2 class="title"><@spring.message "page.title.login"/></h2>

                            <div class="col-md-12">
                                <div class="alert alert-warning" v-if="error">
                                    <div class="container">
                                        <div class="alert-icon">
                                            <i class="material-icons">warning</i>
                                        </div>

                                        <span>{{'ERROR_' + error | translate}}</span>
                                    </div>
                                </div>

                                <div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">email</i>
								</span>
                                    <div class="form-group is-empty">
                                        <input class="form-control"
                                               type="text" v-model="username" placeholder="<@spring.message 'common.email'/>">
                                        <span class="material-input"></span>
                                    </div>
                                </div>

                                <div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">lock_outline</i>
								</span>
                                    <div class="form-group is-empty">
                                        <input class="form-control"
                                               type="password" v-model="password" placeholder="<@spring.message 'common.password'/>">
                                        <span class="material-input"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="text-center">
                                    <button class="btn btn-primary btn-simple btn-wd btn-lg" type="submit">
                                        <@spring.message "button.label.login"/>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<#if Session.url_prior_login??>
    <script>
        alert(${Session.url_prior_login});
    </script>
</#if>

</#macro>

<@display_page "initLoginPage"/>