<#ftl encoding="UTF-8">

<#include "template.ftl"/>

<#macro components>
    <#include "components/form.ftl"/>
</#macro>

<#macro page_content>
<div class="wrapper" id="index" v-cloak>
    <div class="container page-filter-panel">

        <div class="main main-raised">
            <div class="row">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <h2 class="title"><@spring.message "page.title.playbill"/></h2>
                        <form class="form">
                            <div class="row">


                                <div class="col-md-6">
                                    <div class="form-group has-feedback is-empty label-floating">
                                        <label for="city" class="control-label"><@spring.message "common.city"/></label>
                                        <tp-city name="city" v-model="filter.city"></tp-city>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback is-empty label-floating">
                                        <label for="from"
                                               class="control-label"><@spring.message "common.fromDate"/></label>
                                        <tp-date name="from" v-model="filter.from"></tp-date>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group has-feedback is-empty label-floating">
                                        <label for="to" class="control-label"><@spring.message "common.toDate"/></label>
                                        <tp-date name="from" v-model="filter.to"></tp-date>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div v-for="(day, dayIndex) in playbill.days" v-if="!isEmpty && firstLoaded"
                 :class="dayIndex % 2 == 0 ? 'section-gray' : 'section-light-gray'">
                <div class="container-fluid">
                    <div>
                        <h3 class="title">{{day.date | friendlyDate}} </h3>
                    </div>
                    <div class="grid">
                        <div v-for="event in day.events" class="grid-item col-md-4 col-sm-6 col-xs-12">
                            <div class="grid-item-content">
                                <div class="card md-card">
                                    <img v-if="event.photo" :src="event.photo" class="playbill-activity-photo">
                                    <div class="card-content" :class="getCardClass(event.type)">
                                        <h4 class="card-title">{{getEventName(event)}}</h4>
                                        <h6><a :href="'/school/' + event.schoolId">{{event.schoolName}}</a></h6>

                                        <div class="footer">
                                            <template v-if="event.type == 'LESSON'">
                                                <div class="tp-stats" v-if="event.firstTeacherId">
                                                    <i class="material-icons">group</i>
                                                    <span>{{playbill.teachers[event.firstTeacherId].firstName}} {{playbill.teachers[event.firstTeacherId].secondName}}</span>
                                                    <span v-if="event.secondTeacherId"> и {{playbill.teachers[event.secondTeacherId].firstName}} {{playbill.teachers[event.secondTeacherId].secondName}}</span>
                                                </div>
                                            </template>

                                            <div class="tp-stats" v-if="event.dj">
                                                <i class="material-icons">headset</i>
                                                DJ <span
                                                    v-if="event.dj.nick">{{event.dj.nick}} ({{event.dj.fullName}})</span>
                                                <span v-if="!event.dj.nick">{{event.dj.fullName}}</span>
                                            </div>

                                            <div class="tp-stats">
                                                <i class="material-icons">today</i> {{day.date | friendlyDate}}
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="tp-stats">
                                                        <i class="material-icons">schedule</i> {{event.startTime}}<span > -
                                                        {{event.endTime ? event.endTime : 'N/A'}}</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="tp-stats" v-if="event.cost > 0">
                                                        <i class="material-icons">attach_money</i> {{event.cost}}
                                                        {{event.currency}}
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="tp-stats" v-if="event.address && event.address.pid != 0">
                                                <i class="material-icons">room</i> {{event.address.value}}
                                            </div>

                                            <a :href="event.fbUrl" v-if="event.fbUrl" target="_blank">
                                                <span class="fa-stack fa-lg">
                                                    <i class="fa fa-square-o fa-stack-2x"></i>
                                                    <i class="fa fa-facebook fa-stack-1x"></i>
                                                </span>
                                            </a>

                                            <a :href="event.vkUrl" v-if="event.vkUrl" target="_blank">
                                                <span class="fa-stack fa-lg">
                                                    <i class="fa fa-square-o fa-stack-2x"></i>
                                                    <i class="fa fa-vk fa-stack-1x"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container" v-if="isEmpty && firstLoaded">
        <div class="row">
            <div class="col-md-12">
                <div class="card text-center">

                    <div class="card-content content-warning">
                        <h3 class="card-title"><@spring.message "error.activity.notFoundInCity"/>
                            {{filter.city.name}}.</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</#macro>

<@display_page "initIndexPage"/>