<#ftl encoding="UTF-8">

<#include "template.ftl"/>

<#macro components>
    <#include "components/form.ftl"/>

</#macro>

<#macro page_content>
<div class="wrapper" id="eventList" v-cloak="">

    <div class="container page-filter-panel">
        <div class="main main-raised">
            <div class="row">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <h2 class="title"><@spring.message "page.title.events"/></h2>
                        <form class="form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback is-empty label-floating">
                                        <label for="city" class="control-label"><@spring.message "common.city"/></label>
                                        <tp-city name="city" v-model="filter.city"></tp-city>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="map" style="height: 500px;">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container" v-if="isEmpty && firstLoaded">
        <div class="row">
            <div class="col-md-12">
                <div class="card text-center">

                    <div class="card-content content-warning">
                        <h3 class="card-title"><@spring.message "error.event.notFound"/></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="grid">
            <div v-for="event in pageData.events" class="grid-item col-md-6">
                <div class="grid-item-content">

                    <div class="card text-center">
                            <img v-if="event.photo" :src="event.photo" style="min-height: 303px">
                        <div class="card-content">
                            <h3 class="card-title">
                                {{event.name}}
                            </h3>
                            <h4 class="card-title">
                                {{event.city.name}}
                            </h4>
                            <h5 class="card-title">{{event.startDate | localeDate}} - {{event.endDate | localeDate}} </h5>

                            <div class="footer">
                                <a v-if="event.siteUrl" :href="event.siteUrl" target="_blank"
                                   class="btn btn-just-icon btn-warning btn-round"><i
                                        class="fa fa-external-link"></i></a>
                                <a v-if="event.fbUrl" :href="event.fbUrl" target="_blank"
                                   class="btn btn-just-icon btn-facebook btn-round"><i
                                        class="fa fa-facebook-square"></i></a>
                                <a v-if="event.vkUrl" :href="event.vkUrl" target="_blank"
                                   class="btn btn-just-icon btn-vk btn-round"><i
                                        class="fa fa-vk"></i></a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>

<@display_page "initEventListPage"/>