<#ftl encoding="UTF-8">

<#include "template.ftl"/>

<#macro components>
    <#include "components/form.ftl"/>

</#macro>

<#macro page_content>
<div class="wrapper" id="schoolDetail">
    <div class="container">
        <#if school.photo?has_content>
            <div class="main main-raised">
                <img src="${school.photo}" style="width: 100%;">
            </div>
        </#if>

        <div class="main main-raised">
            <div class="section-white">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2 class="title">${school.name}
                        </h2>
                        <h4 class="tp-stats">
                            <i class="material-icons">place</i> ${school.address.value}
                        </h4>
                        <div>
                            <#if school.siteUrl?has_content>
                                <a href="${school.siteUrl}" class="btn btn-just-icon btn-warning btn-round"><i
                                        class="fa fa-external-link"></i></a>
                            </#if>
                            <#if school.fbUrl?has_content>
                                <a href="${school.fbUrl}" class="btn btn-just-icon btn-facebook btn-round"><i
                                        class="fa fa-facebook-square"></i></a>
                            </#if>
                            <#if school.vkUrl?has_content>
                                <a href="${school.vkUrl}" class="btn btn-just-icon btn-vk btn-round"><i
                                        class="fa fa-vk"></i></a>
                            </#if>
                        </div>
                        <h4 class="tp-stats">
                            <#if school.workPhone?has_content><i
                                    class="material-icons">phone</i> ${school.workPhone}</#if>
                            <#if school.mobilePhone?has_content><i
                                    class="material-icons">phone</i> ${school.mobilePhone}</#if>
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <iframe
                                height="450"
                                frameborder="0" style="border:0; width: 100%"
                                src="https://www.google.com/maps/embed/v1/place?key=AIzaSyC1s58ySzzxwO-pag1Mr2FiIbcfMk2bW0U&q=place_id:${school.address.pid}"
                                allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </div>
        </div>


        <div class="main main-raised">
            <div class="section-gray">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2 class="title"><@spring.message "common.teachers"/></h2>
                    </div>
                </div>
                <div class="row">
                    <#if teachers?has_content>
                        <div class="row-content">
                            <#list teachers as teacher>
                                <div class="col-md-4">
                                    <div class="card card-profile">
                                        <div class="card-image">
                                            <a>
                                                <#if teacher.avatar?has_content>
                                                    <img class="img" src="${teacher.avatar}">
                                                <#else>
                                                    <img class="img" src="/img/person_grey.png"
                                                         style="background-color: white">
                                                </#if>
                                            </a>
                                        </div>

                                        <div class="card-content">
                                            <h4 class="card-title">${teacher.firstName} ${teacher.secondName}</h4>
                                        </div>
                                    </div>
                                </div>
                            </#list>
                        </div>
                    </#if>
                </div>
            </div>
        </div>

        <div class="main main-raised">
            <div class="section-white">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2 class="title"><@spring.message "common.timetable"/></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <#list timetable.days as day>
                            <#if day.rows?has_content>
                                <table class="table table-striped timetable tp-block timetable-${timetable.rooms?size}">
                                    <colgroup>
                                        <#list timetable.rooms as room>
                                            <col>
                                        </#list>
                                    </colgroup>
                                    <thead>
                                    <tr class="bg-primary ">
                                        <th>
                                            <@spring.message 'days.' + day.dayOfWeek?lower_case/>
                                        </th>
                                        <#list timetable.rooms as room>
                                            <th class="room">
                                                <div class="tp-block-header">
                                                ${room}
                                                </div>
                                            </th>
                                        </#list>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <#list day.rows as row>
                                        <tr class="timetable-row">
                                            <td>${row.startTime?string('HH:mm')} - ${row.endTime?string('HH:mm')}</td>
                                            <#list row.items as item>
                                                <td>
                                                    <#if item?has_content>
                                                        <div class="card card-profile card-plain">
                                                            <div class="card-content">
                                                                <h4 class="card-title">${item.name}</h4>

                                                                <p class="card-description">
                                                                    <#if item.firstTeacherId??>
                                                                        <span>${teacherNames[item.firstTeacherId?string]}
                                                                            <#if item.secondTeacherId?has_content>
                                                                                <span> /${teacherNames[item.secondTeacherId?string]}</span>
                                                                            </#if>
                                                                    </span>
                                                                    </#if>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </#if>
                                                </td>
                                            </#list>
                                        </tr>
                                        </#list>
                                    </tbody>
                                </table>
                            </#if>
                        </#list>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</#macro>

<@display_page  "initSchoolDetailPage"/>