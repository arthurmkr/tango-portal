<#ftl encoding="UTF-8">

<#include "../template.ftl"/>
<#import "/spring.ftl" as spring/>

<#macro page_content>
<div class="wrapper" id="index">
    <div class="container">

        <div class="main main-raised">
            <div class="row">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <h1 class="title text-center"><@spring.message "error.404"/></h1>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

</#macro>

<@display_page />