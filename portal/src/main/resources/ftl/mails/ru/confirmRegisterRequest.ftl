<#ftl encoding="UTF-8">

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
</head>
<body>

<p>
    Информационное сообщение портала Tango World
    <br/>
    ---------------------------------------------------------------------
</p>

<p>
    Здравствуйте,
</p>
<p>
    Вы получили это сообщение, так как Ваш адрес был использован при регистрации нового пользователя на портале
    <a href="https://tango-world.info">Tango World</a>.
</p>
<p>
    Для подтверждения электронного адреса перейдите по следующей ссылке:<br/>
    <a href="https://tango-world.info/confirm/${registerToken}">https://tango-world.info/confirm/${registerToken}</a>
</p>

<p>
    ---------------------------------------------------------------------
    <br/>
    Сообщение сгенерировано автоматически.
</p>
<p>
    --<br/>
    С уважением,<br/>
    портал <a href="https://tango-world.info">Tango World</a>.
</p>
</body>
</html>