<#ftl encoding="UTF-8">
<#setting locale="ru">

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
</head>
<body>

<p>
    Информационное сообщение портала Tango World
    <br/>
    ---------------------------------------------------------------------
</p>

<p>
    Здравствуйте,
</p>
<p>
    Вы получили это сообщение, так как по Вашей реферальной ссылке на портале <a href="https://tango-world.info">Tango
    World</a> зарегистрировались пользователи:
</p>
<ul>
<#list users as user>
    <li>${user.firstName} ${user.secondName}</li>
</#list>
</ul>

<p>
    Для подтверждения их регистрации Вам необходимо перейти в личный <a href="https://tango-world.info">кабинет</a>.
</p>
<p>
    ---------------------------------------------------------------------
    <br/>
    Сообщение сгенерировано автоматически.
</p>
<p>
    --<br/>
    С уважением,<br/>
    портал <a href="https://tango-world.info">Tango World</a>.
</p>
</body>
</html>