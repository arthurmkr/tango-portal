<#ftl encoding="UTF-8">

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
</head>
<body>

<p>
    Информационное сообщение портала Tango World
    <br/>
    ---------------------------------------------------------------------
</p>

<p>
    Поздравляем!
</p>
<p>
    Ваша регистрация на портале <a href="https://tango-world.info">Tango World</a> одобрена.
</p>
<p>
    Ваши логин и пароль для входа в Личный кабинет<br/>
    Логин: <strong>${login}</strong> <br/>
    Пароль: <strong>${password}</strong>
</p>
<p>
    ---------------------------------------------------------------------
    <br/>
    С уважением,
</p>
<p>
    --<br/>
    С уважением,<br/>
    портал <a href="https://tango-world.info">Tango World</a>.
</p>
</body>
</html>