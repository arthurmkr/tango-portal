<#ftl encoding="UTF-8">

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
</head>
<body>

<p>
    Information message from the portal Tango World
    <br/>
    ---------------------------------------------------------------------
</p>

<p>
    Hello,
</p>
<p>
    You received this message, because your address was used when registering a new user on the portal
    <a href="https://tango-world.info">Tango World</a>.
</p>
<p>
    To verify your email address, please click the link below:<br/>
    <a href="https://tango-world.info/confirm/${registerToken}">https://tango-world.info/confirm/${registerToken}</a>
</p>

<p>
    ---------------------------------------------------------------------
    <br/>
    The mail is generated automatically.
</p>
<p>
    --<br/>
    Best regards, <br/>
    portal <a href="https://tango-world.info">Tango World</a>.
</p>
</body>
</html>