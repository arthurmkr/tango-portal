<#ftl encoding="UTF-8">
<#setting locale="ru">

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
</head>
<body>

<p>
    Information message from the portal Tango World
    <br/>
    ---------------------------------------------------------------------
</p>

<p>
    Hello,
</p>
<p>
    You have received this message, because users have registered on the
    <a href="https://tango-world.info">Tango World</a> portal by your referral link:
</p>
<ul>
<#list users as user>
    <li>${user.firstName} ${user.secondName}</li>
</#list>
</ul>

<p>
    To confirm their registration, you need to go to <a href="https://tango-world.info">Account</a>.
</p>
<p>
    ---------------------------------------------------------------------
    <br/>
    The mail is generated automatically.
</p>
<p>
    --<br/>
    Best regards, <br/>
    portal <a href="https://tango-world.info">Tango World</a>.
</p>
</body>
</html>