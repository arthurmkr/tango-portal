<#ftl encoding="UTF-8">

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
</head>
<body>

<p>
    Information message from the portal Tango World
    <br/>
    ---------------------------------------------------------------------
</p>

<p>
    Congratulations!
</p>
<p>
    Your registration on the <a href="https://tango-world.info">Tango World</a> portal is approved.
</p>
<p>
    Your username and password to enter your Account<br/>
    Username: <strong>${login}</strong> <br/>
    Password: <strong>${password}</strong>
</p>
<p>
    ---------------------------------------------------------------------
    <br/>
    The mail is generated automatically.
</p>
<p>
    --<br/>
    Best regards, <br/>
    portal <a href="https://tango-world.info">Tango World</a>.
</p>
</body>
</html>