<#ftl encoding="UTF-8">

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
</head>
<body>

<p>
    Mensaje informativo del portal Tango World
    <br/>
    ---------------------------------------------------------------------
</p>

<p>
    Hola,
</p>
<p>
    Usted ha obtenido este mensaje porque su correo había sido usado al registrar un nuevo usario en el portal
    <a href="https://tango-world.info">Tango World</a>.
</p>
<p>
    Para confirmar su correo use el enlace:<br/>
    <a href="https://tango-world.info/confirm/${registerToken}">https://tango-world.info/confirm/${registerToken}</a>
</p>

<p>
    ---------------------------------------------------------------------
    <br/>
    Este mensaje ha sido generado automáticamente.
</p>
<p>
    --<br/>
    Atentamente, <br/>
    portal <a href="https://tango-world.info">Tango World</a>.
</p>
</body>
</html>