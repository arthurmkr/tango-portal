<#ftl encoding="UTF-8">

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
</head>
<body>

<p>
    Mensaje informativo del portal Tango World
    <br/>
    ---------------------------------------------------------------------
</p>

<p>
    ¡Felicidades!
</p>
<p>
    Su cuenta en el portal <a href="https://tango-world.info">Tango World</a> ha sido confirmada.
</p>
<p>
    Su nombre de usario y contraseña para acceder al perfil<br/>
    Nombre de usario: <strong>${login}</strong> <br/>
    Contraseña: <strong>${password}</strong>
</p>
<p>
    ---------------------------------------------------------------------
    <br/>
    Este mensaje ha sido generado automáticamente.
</p>
<p>
    --<br/>
    Atentamente, <br/>
    portal <a href="https://tango-world.info">Tango World</a>.
</p>
</body>
</html>