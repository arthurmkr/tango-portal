<#ftl encoding="UTF-8">
<#setting locale="ru">

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
</head>
<body>

<p>
    Mensaje informativo del portal Tango World
    <br/>
    ---------------------------------------------------------------------
</p>

<p>
    Hola,
</p>
<p>
    Ha obtenido este mensaje porque algunos usarios nuevos habían usado su enlace de referencia  <a href="https://tango-world.info">Tango
    World</a> al registrarse en el portal:
</p>
<ul>
<#list users as user>
    <li>${user.firstName} ${user.secondName}</li>
</#list>
</ul>

<p>
    Para confirmar sus cuentas tiene que acceder  <a href="https://tango-world.info">a su perfil</a>.
</p>
<p>
    ---------------------------------------------------------------------
    <br/>
    Este mensaje ha sido generado automáticamente.
</p>
<p>
    --<br/>
    Atentamente, <br/>
    portal <a href="https://tango-world.info">Tango World</a>.
</p>
</body>
</html>