<#ftl encoding="UTF-8">

<#include "template.ftl"/>

<#macro page_content>
<div class="wrapper" id="index">
    <div class="container">

        <div class="main main-raised">
            <div class="row">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <h2>Инструкция</h2>
                        <h3>Регулярные уроки</h3>
                        <p>Регулярные уроки - это те уроки, которые повторяются каждую неделю. При создании такого урока
                            необходимо указать дату с которой данный урок начнет повторяться (это день может не
                            совпадать с днем недели, в который урок непосредственно проводится). Также можно указать
                            дату окончания действия, если дата окончания не указана урок будет повторяться
                            бесконечно.
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

</#macro>

<@display_page />