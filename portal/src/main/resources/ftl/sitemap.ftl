<#ftl encoding="UTF-8">
<#-- @ftlvariable name="cities" type="java.util.List<org.satuban.tangoportal.portal.model.dto.SiteMapDto>" -->

<#import "spring.ftl" as spring/>
<?xml version="1.0" encoding="UTF-8" standalone="no"?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<#list cities as siteMapCity>
    <url>
        <loc>${siteMapCity.url}</loc>
    </url>
</#list>
</urlset>