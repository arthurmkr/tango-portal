<#ftl encoding="UTF-8">

<#include "template.ftl"/>
<#import "spring.ftl" as spring/>

<#macro page_content>
<div class="wrapper" id="index">
    <div class="container">

        <div class="main main-raised">
            <div class="row">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <h2 class="title"><@spring.message "page.title.about"/></h2>
                        <p>
                            <@spring.message "page.about.content1"/>
                        </p>
                        <h3 class="title"><@spring.message "page.about.subtitle1"/></h3>
                        <p>
                            <@spring.message "page.about.content2"/>
                        </p>
                        <p>
                            <@spring.message "page.about.content3"/>
                        </p>
                        <p>
                            <@spring.message "page.about.content4"/>
                        </p>

                        <h3 class="title"><@spring.message "page.about.subtitle2"/></h3>
                        <p>
                            <@spring.message "page.about.content5"/>
                        </p>
                        <p>
                            <@spring.message "page.about.content6"/>
                        </p>
                        <p>
                            <@spring.message "page.about.content7"/>
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

</#macro>

<@display_page />