<#ftl encoding="UTF-8">
<#import "spring.ftl" as spring/>

<#macro modalForm id>
<div class="modal fade" id="${id}Modal" tabindex="-1" role="dialog" aria-hidden="true"
     aria-labelledby="${id}ModalLabel">
    <div class="modal-dialog" :class="config.dialogClass" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="title modal-title" id="${id}ModalLabel">{{title}}</h4>
            </div>
            <form id="${id}Form" role="form">
                <div class="modal-body">
                    <#nested />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal"><@spring.message "button.label.cancel"/></button>
                    <button type="submit" class="btn btn-primary"><@spring.message 'button.label.save'/></button>
                </div>
            </form>
        </div>
    </div>
</div>
</#macro>

<#macro roles>
<div>
    <label class="control-label"><@spring.message "common.roles"/></label>

    <div class="checkbox">
        <label>
            <input type="checkbox" v-model="obj.roles.dj">
            <@spring.message "common.dj"/>
        </label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" v-model="obj.roles.teacher">
            <@spring.message "common.teacher"/>
        </label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" v-model="obj.roles.schoolLead">
            <@spring.message "common.schoolLead"/>
        </label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" v-model="obj.roles.organizer">
            <@spring.message "common.organizer"/> <i class="material-icons" data-toggle="tooltip" data-placement="top"
                                                     title="<@spring.message 'tooltips.organizerRole'/>"
                                                     data-container="body">help</i>
        </label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" v-model="obj.roles.photographer">
            <@spring.message "common.photographer"/>
        </label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" v-model="obj.roles.schoolAdmin">
            <@spring.message "common.schoolAdmin"/>
        </label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" v-model="obj.roles.couturier">
            <@spring.message "common.couturier"/>
        </label>
    </div>
</div>
</#macro>

<#macro daysOfWeek>
<div class="required form-group tp-select-block">
    <label for="regular" class="control-label"><@spring.message "activity.dayOfWeek"/></label>
    <div>
        <div class="radio">
            <label>
                <input type="radio" value="MONDAY" name="dayOfWeek"
                       v-model="obj.dayOfWeek"> <@spring.message "days.monday"/>
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" value="TUESDAY" name="dayOfWeek"
                       v-model="obj.dayOfWeek"> <@spring.message "days.tuesday"/>
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" value="WEDNESDAY" name="dayOfWeek"
                       v-model="obj.dayOfWeek"> <@spring.message "days.wednesday"/>
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" value="THURSDAY" name="dayOfWeek"
                       v-model="obj.dayOfWeek"> <@spring.message "days.thursday"/>
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" value="FRIDAY" name="dayOfWeek"
                       v-model="obj.dayOfWeek"> <@spring.message "days.friday"/>
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" value="SATURDAY" name="dayOfWeek"
                       v-model="obj.dayOfWeek"> <@spring.message "days.saturday"/>
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" value="SUNDAY" name="dayOfWeek"
                       v-model="obj.dayOfWeek"> <@spring.message "days.sunday"/>
            </label>
        </div>
    </div>
</div>
</#macro>