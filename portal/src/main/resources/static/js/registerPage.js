function initRegisterPage() {
    new Vue({
        el: '#register',
        data: {
            obj: $.extend(true, {}, ProfileModel),
            result: false,
            error: null
        },
        mounted: function () {
            let form = $('#registerForm');
            form.validate({
                submitHandler: () => {
                    PublicResources.register.save({
                        token: $('#inviteToken').val()
                    }, this.obj)
                        .then(response => {
                            this.result = true;
                        }, response => {
                            this.error = response.body.message;
                        });

                    return false;
                },
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    firstName: {
                        required: true
                    },
                    secondName: {
                        required: true
                    },
                    fbProfile: {
                        url: true
                    },
                    vkProfile: {
                        url: true
                    }
                }
            });
        }
    })
}