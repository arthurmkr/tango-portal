function initIndexPage() {
    new Vue({
        el: '#index',
        data: {
            firstLoaded: false,
            playbill: {
                teachers: [],
                days: []
            },
            filter: {
                city: $.extend(true, {}, CityModel),
                from: null,
                to: null
            }
        },
        computed: {
            isEmpty: function () {
                return this.playbill.days.length == 0;
            }
        },
        methods: {
            getCardClass: function (type) {
                switch (type) {
                    case 'PRACTICE':
                        return 'content-info';
                    case 'MILONGA':
                        return 'content-rose';
                }

                return '';
            },
            getEventName: function (event) {
                if (event.name === '' || event.name === null) {
                    return I18N['ACTIVITY_DEFAULT_LABEL_' + event.type];
                } else {
                    return event.name;
                }
            },
            refreshPlaybill: function () {
                PublicResources.activity.get({
                    city: this.filter.city.name,
                    from: this.filter.from,
                    to: this.filter.to
                }).then(response => {
                    $.extend(this.playbill, response.body);
                    this.firstLoaded = true;
                    Vue.nextTick(() => {
                        $('.grid')
                            .packery({
                                itemSelector: '.grid-item',
                                percentPosition: true
                            })
                            .packery('reloadItems')
                            .packery();
                    })
                })
            },
            initFilter: function (city) {
                this.filter.city.name = city;
                this.filter.from = moment().format('DD/MM/YYYY');
                this.filter.to = moment().add({days: 6}).format('DD/MM/YYYY');

                this.refreshPlaybill();
            }
        },
        mounted: function () {
            let urlFilter = {
                city: null
            };

            urlToJson(urlFilter);
            getHomeCity(this.initFilter, urlFilter);

            this.$watch('filter', () => {
                appendFilterToUrl({
                    city: this.filter.city.name
                });

                this.refreshPlaybill();
            }, {deep: true});
        }
    });

}