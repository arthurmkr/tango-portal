function initCabinetPage() {

    let cabinet = new Vue({
        el: '#cabinet',
        data: {
            selectDayOfWeek: null,
            profile: AppState.profile,
            school: AppState.school,
            teachers: AppState.teachers,
            schoolRooms: AppState.schoolRooms,
            filter: {
                from: null,
                to: null
            },
            configs: {
                schoolRoom: {
                    items: AppState.schoolRooms,
                    event: {
                        edit: EventType.EDIT_SCHOOL_ROOM,
                        saved: EventType.SCHOOL_ROOM_SAVED
                    },
                    alert: {
                        saved: I18N.ROOM_IS_SAVED,
                        removed: I18N.ROOM_IS_REMOVED
                    },
                    resource: PrivateResources.schoolRoom
                }
            },
            removeActivityConfirmationConfig: {
                message: null
            }
        },
        computed: {
            hasSchool: function () {
                return this.school.info.id !== null;
            },
            roomColumnClass: function () {
                return 'timetable-' + this.school.timetable.rooms.length;
            },
            timetableColumnCount: function () {
                return this.school.timetable.rooms.length + 1;
            }
        },
        methods: {
            editSchool: function () {
                bus.$emit(EventType.EDIT_SCHOOL, this.school.info);
            },

            editProfile: function () {
                bus.$emit(EventType.EDIT_PROFILE, this.profile)
            },
            addRegularLesson: function () {
                $('#chooseSchoolActivityModal').modal('hide');

                let activity = $.extend(true, {}, ActivityModel);
                activity.dayOfWeek = this.selectDayOfWeek;
                activity.type = 'LESSON';
                bus.$emit(EventType.EDIT_REGULAR_LESSON, activity);
            },
            addSingleSchoolLesson: function () {
                $('#chooseSchoolActivityModal').modal('hide');

                let activity = $.extend(true, {}, ActivityModel);
                activity.type = 'LESSON';
                bus.$emit(EventType.EDIT_SINGLE_LESSON, activity);
            },
            addPractice: function () {
                $('#chooseSchoolActivityModal').modal('hide');

                let activity = $.extend(true, {}, ActivityModel);
                activity.dayOfWeek = this.selectDayOfWeek;
                activity.type = 'PRACTICE';
                bus.$emit(EventType.EDIT_PRACTICE, activity);
            },
            refreshTimetable: function () {
                PrivateResources.school.timetable({startFrom: this.filter.from}).then(response => {
                    $.extend(this.school.timetable, response.body);
                });
            },
            refreshDjs: function () {
                PublicResources.dictionary.djs()
                    .then(response => {
                        AppState.djs.splice(0, AppState.djs.length);
                        $.each(response.body, (index, item) => {
                            AppState.djs.push(item);
                        })
                    });
            },

            editTimetableItem: function (item) {
                PrivateResources.activity.get({id: item.id}).then(response => {
                    let event;
                    let activity = response.body;
                    switch (activity.type) {
                        case 'LESSON':
                            event = activity.regular ? EventType.EDIT_REGULAR_LESSON : EventType.EDIT_SINGLE_LESSON;
                            break;
                        case 'PRACTICE':
                            event = EventType.EDIT_PRACTICE;
                            break;
                    }

                    if (event) {
                        bus.$emit(event, activity);
                    }
                })
            },
            removeActivity: function (item) {
                this.selectedItem = item;

                switch (item.type) {
                    case 'LESSON':
                        this.removeActivityConfirmationConfig.message = I18N.CONFIRMATION_DELETE_LESSON;
                        break;
                    case 'PRACTICE':
                        this.removeActivityConfirmationConfig.message = I18N.CONFIRMATION_DELETE_PRACTICE;
                        break;
                    default:
                        return;
                }

                bus.$emit(EventType.SHOW_CONFIRMATION_DIALOG, this.removeActivityConfirmationConfig)
            },
            getOauthFacebookUrl: function() {
                PrivateResources.facebook.oauthUrl().then(response => {
                    return response.bodyText;
                }).then(url => {
                    window.open(url);
                });
            }
        },
        mounted: function () {
            let _this = this;
            bus.$on(EventType.SCHOOL_SAVED, school => {
                $.extend(this.school.info, school)
            });



            bus.$on(EventType.PROFILE_SAVED, profile => {
                $.extend(this.profile, profile)
            });

            bus.$on(EventType.REGULAR_LESSON_SAVED, () => {
                this.refreshTimetable();
            });

            bus.$on(EventType.SINGLE_LESSON_SAVED, () => {
                this.refreshTimetable();
            });

            bus.$on(EventType.PRACTICE_SAVED, () => {
                this.refreshTimetable();
            });

            PrivateResources.school.get()
                .then(response => {
                    $.extend(this.school.info, response.body);
                });

            this.$watch('filter', () => {
                this.refreshTimetable();
            }, {deep: true});

            this.filter.from = moment().startOf('isoweek').format('DD/MM/YYYY');

            PrivateResources.profile.get()
                .then(response => {
                    $.extend(this.profile, response.body);
                });

            this.removeActivityConfirmationConfig.onYes = function () {
                PrivateResources.activity.remove({id: _this.selectedItem.id}).then(() => {
                    _this.refreshTimetable();
                });
            };

            $('#milongasTabLink').on('show.bs.tab', function (e) {
                bus.$emit(EventType.REFRESH_PLANNED_MILONGAS);
                _this.refreshDjs();
            });

            $('#eventsTabLink').on('show.bs.tab', function (e) {
                bus.$emit(EventType.REFRESH_EVENT_TAB);
                _this.refreshDjs();
            })
        }
    });
}


