function initSchoolListPage() {
    new Vue({
        el: '#schoolList',
        data: {
            firstLoaded: false,
            pageData: {
                schools: []
            },
            filter: {
                city: $.extend(true, {}, CityModel),
            }
        },
        computed: {
            isEmpty: function () {
                return this.pageData.schools.length === 0;
            }
        },
        methods: {
            refreshSchoolList: function () {
                PublicResources.school.get({
                    city: this.filter.city.name
                }).then(response => {
                    this.pageData.schools = response.body;
                    this.firstLoaded = true;
                });
            },
            initFilter: function (city) {
                this.filter.city.name = city;

                this.refreshSchoolList();
            }
        },
        mounted: function () {
            PublicResources.dictionary.tangoMapByType({type: 'SCHOOL'})
                .then(response => {
                    let mapData = response.body;

                    let map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 2,
                        center: {
                            lat: 18,
                            lng: 10
                        }
                    });

                    let cityMarkers = mapData.markers;
                    for (let i = 0; i < cityMarkers.length; i++) {
                        let cityMarker = cityMarkers[i];

                        let marker = new google.maps.Marker({
                            position: {
                                lat: cityMarker.lat,
                                lng: cityMarker.lng
                            },
                            map: map,
                            label: '' + cityMarker.count
                        });

                        marker.addListener('click', () => {
                            this.filter.city.name = cityMarker.name;
                        });
                    }

                    this.initialized = true;
                });

            this.$watch('pageData', () => {
                Vue.nextTick(() => {
                    this.grid = $('.grid').packery({
                        itemSelector: '.grid-item',
                        percentPosition: true
                    });

                    this.grid.packery('reloadItems')
                        .packery();
                })

            }, {deep: true});

            let urlFilter = {
                city: null
            };

            urlToJson(urlFilter);
            getHomeCity(this.initFilter, urlFilter);

            this.$watch('filter', () => {
                appendFilterToUrl({
                    city: this.filter.city.name
                });

                this.refreshSchoolList();
            }, {deep: true});
        }
    })
}