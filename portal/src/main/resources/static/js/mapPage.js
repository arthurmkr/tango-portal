function initMapPage() {
    new Vue({
        el: '#tangoMap',
        data: {
            initialized: false,
            selectedMarker: {
                name: null
            }
        },
        mounted: function () {
            PublicResources.dictionary.tangoMap()
                .then(response => {
                    let mapData = response.body;


                    let infowindow = new google.maps.InfoWindow({
                        content: $('#schoolCityPopover')[0]
                    });

                    let map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 2,
                        center: {
                            lat: 18,
                            lng: 10
                        }
                    });

                    let cityMarkers = mapData.markers;
                    for (let i = 0; i < cityMarkers.length; i++) {
                        let cityMarker = cityMarkers[i];

                        let marker = new google.maps.Marker({
                            position: {
                                lat: cityMarker.lat,
                                lng: cityMarker.lng
                            },
                            map: map,
                            label: '' + cityMarker.totalCount
                        });
                        marker.addListener('click', () => {
                            this.selectedMarker = cityMarker;
                            Vue.nextTick(function() {
                                infowindow.open(map, marker);
                            })
                        });
                    }

                    this.initialized = true;
                });
        }
    })
}