function initLoginPage() {
    new Vue({
        el: '#login',
        data: {
            username: null,
            password: null,
            error: null
        },
        mounted: function () {
            let _this = this;

            $('#loginPageForm').validate({
                submitHandler: function () {
                    Vue.http.post("/api/public/auth/login", {
                        username: _this.username,
                        password: _this.password
                    }).then(response => {
                        let body = response.body;
                        if (body.errorCode) {
                            _this.error = body.errorCode;
                        } else {
                            var url = new URL(window.location.href);
                            var prevUrl = url.searchParams.get("prev_url");
                            if(prevUrl != null) {
                                window.location.href = decodeURIComponent(prevUrl);
                            } else {
                                location.reload();
                            }
                        }
                    });
                    return false;
                },
                rules: {
                    username: {
                        required: true
                    },
                    password: {
                        required: true
                    }
                }
            });
        }
    });
}