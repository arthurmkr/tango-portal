const Alerts = Object.freeze({
    success: function (message) {
        return {
            type: 'success',
            msg: message
        }
    }
});

const Constants = Object.freeze({
    DATE_FORMAT: 'DD/MM/YYYY'
});

const CookieNames = Object.freeze({
    HOME_CITY: 'homeCity'
});

const EventType = Object.freeze({
    NEW_ALERT: 'new-alert',
    TEACHER_SAVED: 'teacher-saved',
    PROFILE_SAVED: 'profile-saved',
    SCHOOL_SAVED: 'school-saved',
    REGULAR_LESSON_SAVED: 'regular-lesson-saved',
    SINGLE_LESSON_SAVED: 'single-lesson-saved',
    PRACTICE_SAVED: 'practice-saved',
    EDIT_TEACHER: 'edit-teacher',
    EDIT_PROFILE: 'edit-profile',
    EDIT_SCHOOL: 'edit-school',
    EDIT_EVENT: 'edit-event',
    EDIT_REGULAR_LESSON: 'edit-regular-lesson',
    EDIT_SINGLE_LESSON: 'edit-single-lesson',
    EDIT_PRACTICE: 'edit-practice',
    EDIT_SCHOOL_ROOM: 'edit-school-room',
    EDIT_MILONGA: 'edit-milonga',
    REPEAT_MILONGA: 'repeat-milonga',
    MILONGA_SAVED: 'milonga-saved',
    SCHOOL_ROOM_SAVED: 'school_room_saved',
    EVENT_SAVED: 'event-saved',
    SHOW_CONFIRMATION_DIALOG: 'show_confirmation_dialog',
    FORM_OPENING: 'form-opening',
    PROFILE_AVATAR_UPLOADED: 'profile-avatar-uploaded',
    TEACHER_AVATAR_UPLOADED: 'teacher-avatar-uploaded',
    SCHOOL_PHOTO_UPLOADED: 'school-photo-uploaded',
    EVENT_PHOTO_UPLOADED: 'event-photo-uploaded',
    MILONGA_PHOTO_UPLOADED: 'milonga-photo-uploaded',
    REFRESH_PLANNED_MILONGAS: 'refresh-planned-milongas',
    NEW_PLANNED_MILONGA_ADDED: 'new-planned-milonga-added',
    REFRESH_EVENT_TAB: 'refresh-event-tab'
});

let token = $('meta[name="_csrf"]');

Vue.http.headers.common['X-CSRF-TOKEN'] = token.attr('content');

$.ajaxSetup({
    headers: {
        'X-CSRF-Token': token.attr('content')
    }
});

// $( document ).ajaxError(function( event, request, settings ) {
//    console.log(request.getMessage())
// });

window.onerror = function (message, url, lineNumber, colNumber) {
    Vue.http.post('/api/saveError', {
        message: message,
        url: url,
        lineNumber: lineNumber,
        colNumber: colNumber
    });
};

const PrivateResources = Object.freeze({
    teacher: Vue.resource('/api/private/teacher{/id}'),
    schoolRoom: Vue.resource('/api/private/schoolRoom{/id}'),
    school: Vue.resource('/api/private/school', {}, {
        timetable: {
            method: 'GET',
            url: '/api/private/school/timetable'
        }
    }),
    activity: Vue.resource('/api/private/activity{/id}'),
    profile: Vue.resource('/api/private/profile', {}, {
        changePassword: {
            method: 'POST',
            url: '/api/private/profile/changePassword'
        }
    }),
    referral: Vue.resource('/api/private/referral{/id}', {}, {
        approve: {
            method: 'PUT',
            url: '/api/private/referral{/id}/approve'
        },
        getReferralLink: {
            method: 'GET',
            url: '/api/private/referral/getLink'
        }
    }),
    photo: Vue.resource('/api/private/photo'),
    milonga: Vue.resource('/api/private/milonga'),
    event: Vue.resource('/api/private/event{/id}', {}, {
        selectListData: {
            method: 'GET',
            url: '/api/private/event/selectListData'
        }
    }),
    facebook: Vue.resource('external/facebook',{}, {
        oauthUrl: {
            method: 'GET',
            url: 'external/facebook/oauth/url'
        }
    })
});

const PublicResources = Object.freeze({
    activity: Vue.resource('/api/public/activity'),
    school: Vue.resource('/api/public/school'),
    dj: Vue.resource('/api/public/user/dj'),
    photographer: Vue.resource('/api/public/user/photographer'),
    dictionary: Vue.resource('', {}, {
        tangoMap: {
            method: 'GET',
            url: '/api/tangoMap'
        },
        tangoMapByType: {
            method: 'GET',
            url: '/api/tangoMapByType'
        },
        djs: {
            method: 'GET',
            url: '/api/djs'
        }
    }),
    register: Vue.resource('/api/public/register{/token}'),
    event: Vue.resource('/api/public/event')
});

const bus = new Vue();

new Vue({
    el: '#confirmationModal',
    data: {
        message: null,
        onYes: null
    },
    methods: {
        processYes: function () {
            if (this.onYes) {
                this.onYes();
            }

            this.dialog.modal('hide');
        }
    },
    computed: {
        dialog: () => $('#confirmationModal')
    },
    mounted: function () {
        bus.$on(EventType.SHOW_CONFIRMATION_DIALOG, config => {
            this.message = config.message;
            this.onYes = config.onYes;

            this.dialog.modal('show');
        })
    }
});


Vue.filter('date', function (value) {
    return moment(value).format(Constants.DATE_FORMAT);
});

Vue.filter('friendlyDate', function (value) {
    return moment(value, Constants.DATE_FORMAT).format('dddd, DD MMMM');
});

Vue.filter('localeDate', function (value) {
    return moment(value, Constants.DATE_FORMAT).format('LL');
});


Vue.filter('time', function (value) {
    return moment(value).format('HH:mm');
});

Vue.filter('translate', function (value) {
    return I18N[value];
});


let ModalMixin = {
    computed: {
        modalId: function () {
            return this.config.id + 'Modal';
        },
        formId: function () {
            return this.config.id + 'Form';
        },
        title: function () {
            return this.obj.id == null || this.obj.id === 0 ? this.config.titles.create : this.config.titles.edit;
        }
    },
    methods: {
        save: function () {
            this.config.resource.save({}, this.obj).then(response => {
                $('#' + this.modalId).modal('hide');

                bus.$emit(this.config.event.saved, response.body);

                $.extend(this.obj, this.config.empty);
            });
        },
        modalInit: function (init, onModalOpen) {
            bus.$on(this.config.event.edit, (newObj) => {
                $.extend(this.obj, $.extend(true, {}, newObj));

                $('#' + this.modalId).modal('show');
            });

            let _this = this;

            let _form = this.form = $('#' + _this.formId);

            let floatingLabelBlocks = {};
            _form.find('.label-floating').each(function (index, item) {
                let block = $(item);
                let inp = block.find('input');
                floatingLabelBlocks[inp.get(0).id] = block;
            });

            _form.validate($.extend(true, _this.config.validatorOptions, {
                submitHandler: function () {
                    _this.save();

                    return false;
                }
            }));

            if (init != undefined) {
                init(_form, _this);
            }

            $('#' + this.modalId)
                .on('show.bs.modal', function (e) {
                    _form.valid();
                    if (onModalOpen != undefined) {
                        onModalOpen(_form, _this);
                    }

                    Vue.nextTick(function () {
                        $.each(floatingLabelBlocks, function (name, item) {
                            if (_form.find('#' + name).val()) {
                                item.removeClass('is-empty');
                            }
                        });
                    });


                    _this.$emit(EventType.FORM_OPENING);
                })
                .on('hide.bs.modal', function (e) {
                    if (e.target.id == _this.modalId) {
                        $.extend(_this.obj, _this.config.empty);
                        // console.log('After clear: ' + JSON.stringify(_this.obj))
                        $.each(floatingLabelBlocks, function (name, item) {
                            item.addClass('is-empty');
                        });
                        _form.validate().resetForm();
                    }
                });
        }
    }
};


let TableMixin = {
    computed: {
        confirmationConfig: function () {
            let _this = this;
            return {
                message: _this.config.confirmDelete.message,
                onYes: function () {
                    _this.config.resource.remove({id: _this.selectedItem.id}).then(() => {
                        _this.applyFor(_this.selectedItem, (index) => {
                            _this.items.splice(index, 1);
                        });

                        bus.$emit(EventType.NEW_ALERT, Alerts.success(_this.config.alert.removed));
                    });
                }
            }
        }
    },
    methods: {
        applyFor: function (item, func) {
            if (item === undefined || item === null) {
                return;
            }

            for (let i = 0; i < this.items.length; i++) {
                if (this.items[i].id === item.id) {
                    let res = func(i, this.items[i]);
                    if (res) {
                        return;
                    }
                }
            }
        },
        editItem: function (item) {
            bus.$emit(this.config.event.edit, item);
        },
        removeItem: function (item) {
            this.selectedItem = item;
            bus.$emit(EventType.SHOW_CONFIRMATION_DIALOG, this.confirmationConfig)
        }
    },
    mounted: function () {
        this.config.resource.get().then(response => {
            this.items.splice(0, this.items.length);
            $.each(response.body, (index, item) => {
                this.items.push(item);
            });
        });


        bus.$on(this.config.event.saved, item => {
            let found = false;
            for (let index = 0; index < this.items.length; index++) {
                if (this.items[index].id === item.id) {
                    Vue.set(this.items, index, item);
                    found = true;
                    break;
                }
            }

            if (!found) {
                this.items.push(item);
            }

            bus.$emit(EventType.NEW_ALERT, Alerts.success(this.config.alert.saved));
        })
    }
};


function getLocality(place) {
    const arrAddress = place.address_components;
    let locality = null;
    $.each(arrAddress, function (i, address_component) {
        if (address_component.types[0] == "locality") {
            locality = address_component.long_name;
            return false;
        }
    });

    return locality;
}

const AppState = new Vue({
    data: {
        profile: $.extend(true, {}, ProfileModel),
        teachers: [],
        referrals: [],
        schoolRooms: [],
        djs: [],
        school: {
            info: $.extend(true, {}, SchoolModel),
            timetable: {
                teachers: [],
                rooms: [],
                days: []
            }
        },

    }
});


jQuery.validator.setDefaults({
    highlight: function (element) {
        // $(element).closest('.form-group').removeClass('has-success');
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
        // $(element).closest('.form-group').addClass('has-success');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});

jQuery.validator.addMethod('phone', function (value, element) {
    return !value || /^[\+\d\-\(\)\s]{5,}/.test(value);
}, I18N.ERROR_INCORRECT_PHONE);


// Регистрируем глобальную пользовательскую директиву v-focus
Vue.directive('blank-image', {
    // Когда привязанный элемент вставлен в DOM...
    update: function (el) {
        if (el.src === '') {
            el.src = '/img/person_grey.png';
        }
    },
    bind: function (el) {
        if (el.src === '') {
            el.src = '/img/person_grey.png';
        }
    }
});

Vue.directive('mask', {
    inserted: function (el, binding) {
        $(el).mask(binding.value)
    }
});

function toggleIsEmpty(el, formGroup) {
    if (el.val()) {
        formGroup.removeClass('is-empty');
    } else {
        formGroup.addClass('is-empty');
    }
}

function getHomeCity(callback, urlFilter) {
    let homeCity = urlFilter.city ? urlFilter.city : Cookies.get(CookieNames.HOME_CITY);

    console.log(('homeCIty: ' + homeCity))
    if (homeCity) {
        callback(homeCity);
    } else {
        callback('');


        Vue.http.get('/api/getLocation')
            .then(response => {
                let coords = response.body;
                console.log('Coods: ' + JSON.stringify(coords));

                let geocoder = new google.maps.Geocoder({
                    types: ['(cities)']
                });
                geocoder.geocode({
                    'latLng': {
                        lat: coords.latitude,
                        lng: coords.longitude
                    }
                }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        let result;
                        if (results.length > 1) {
                            result = results[1];
                        } else {
                            result = results[0];
                        }

                        let cityName = getLocality(result);
                        callback(cityName);
                        Cookies.set(CookieNames.HOME_CITY, cityName);
                    }
                });
            });
    }
}


function urlToJson(obj) {
    decodeURIComponent(location.search).substr(1).split("&").forEach(function (item) {
        let params = item.split("=");
        let paramName = params[0];
        let paramValue = params[1];

        if (obj[paramName] || obj[paramName] == null) {
            if ($.isArray(obj[paramName])) {
                obj[paramName].push(paramValue);
            } else {
                obj[paramName] = paramValue;
            }
        }
    });
}

function copyArray(src, dst) {
    dst.splice(0, dst.length);
    $.each(src, (index, item) => {
        dst.push(item);
    });
}

function applyFor(items, item, func) {
    if (item === undefined || item === null) {
        return;
    }

    for (let i = 0; i < items.length; i++) {
        if (items[i].id === item.id) {
            let res = func(i, items[i]);
            if (res) {
                return;
            }
        }
    }
}


function appendFilterToUrl(filter) {
    let filterStr = '';
    for (let prop in filter) {
        if (!filter[prop]) {
            continue;
        }

        if (filterStr !== '') {
            filterStr += '&';
        }

        filterStr += prop + '=' + filter[prop];
    }

    let url = new URL(window.location.href);

    window.history.replaceState({}, null, url.pathname + (filterStr !== '' ? '?' + filterStr : ''));

}