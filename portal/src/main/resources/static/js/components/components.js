Vue.component('alerts', {
    data: () => {
        return {
            alerts: [],
            counter: 0
        }
    },
    template: '#alerts',
    created: function () {
        bus.$on(EventType.NEW_ALERT, alert => {
            alert.counter = this.counter++;
            this.alerts.push(alert);

            setTimeout(() => {
                this.alerts.splice(this.alerts.indexOf(alert), 1);
            }, 3000);
        })
    }
});

Vue.component('tp-address', {
    template: '#tp-address',
    props: [
        'name',
        'value',
        'required'
    ],
    data: function () {
        return {
            selected: false,
            address: $.extend(true, {}, AddressModel)
        };
    },
    methods: {
        resetInput: function () {
            if (this.address.value === null) {
                this.el.val('');
                this.formGroup.addClass('is-empty');
            } else {
                this.formGroup.removeClass('is-empty');
            }
        }
    },
    watch: {
        'value': function (newVal) {
            $.extend(this.address, newVal);
            this.el.val(this.address.value);
        }
    },
    created: function () {
        $.extend(this.address, this.value);
    },
    mounted: function () {
        let _this = this;
        this.el = $(this.$el);
        this.formGroup = this.el.closest('.form-group');

        this.el.val(this.address.value);

        let typed;

        const autocomplete = new google.maps.places.Autocomplete(this.el[0], {
            types: ['address']
        });

        autocomplete.addListener('place_changed', function () {
            const place = autocomplete.getPlace();

            _this.address.city.name = getLocality(place);
            if (_this.address.city.name !== null) {
                _this.address.value = _this.el.val();
                _this.address.pid = place.place_id;
                _this.address.lat = place.geometry.location.lat;
                _this.address.lng = place.geometry.location.lng;
                _this.selected = true;

                _this.resetInput();
            } else {
                $.extend(_this.address, AddressModel)
                _this.resetInput();
            }

            _this.$emit('input', _this.address);
        });


        let skippedKeys = [
            9, // tab
            16, // shift
            17, // ctrl
            18, // alt
            19, // pause
            20, // caps
            27, // escape
            33, // page up
            34, // page down
            35, // end
            36, // home
            37, // left arrow
            38, // up arrow
            39, // right arrow
            40, // down arrow
            91, // left win
            92, // right win
            93, // select
            112, // F1
            113, // F2
            114, // f3
            115, // F4
            116, // F5
            117, // F6
            118, // F7
            119, // F8
            120, // F9
            121, // F10
            122, // F11
            123, // F12
            144, // num lock
            145 // scroll lock
        ];

        this.el.on('focus', function () {
            typed = false;
        }).on('blur', function () {
            if (typed && _this.address.value == null) {
                $.extend(_this.address, AddressModel);
                _this.resetInput();

                _this.$emit('input', _this.address);
                // _this.$emit(EventType.ADDRESS_UPDATED, _this.address)
            }
        }).on('keydown', function (e) {
            if (e.which === 13 && $('.pac-container:visible').length) {
                return false;
            }
            if (skippedKeys.indexOf(e.which) >= 0) {
                return;
            }

            typed = true;
            _this.address.value = null;
        });

    }
});

Vue.component('tp-city', {
    template: '#tp-city',
    props: [
        'name',
        'value',
        'required'
    ],
    data: function () {
        return {
            selected: false,
            city: $.extend(true, {}, CityModel)
        };
    },
    methods: {
        resetInput: function () {
            if (this.city.name === null) {
                this.el.val('');
                this.formGroup.addClass('is-empty');
            } else {
                this.formGroup.removeClass('is-empty');
            }
        }
    },
    created: function () {
        $.extend(this.city, this.value);
    },
    mounted: function () {
        let _this = this;
        this.el = $(this.$el);
        this.formGroup = this.el.closest('.form-group');

        this.$watch('value', function (newVal) {
            $.extend(this.city, newVal);
            this.el.val(this.city.name);
            toggleIsEmpty(this.el, this.formGroup);

        }, {deep: true});

        this.el.val(this.city.name);

        let typed;

        const autocomplete = new google.maps.places.Autocomplete(this.el[0], {
            types: ['(cities)']
        });

        autocomplete.addListener('place_changed', function () {
            const place = autocomplete.getPlace();

            _this.city.name = getLocality(place);
            if (_this.city.name !== null) {
                _this.selected = true;
                _this.city.pid = place.place_id;

                _this.resetInput();
            } else {
                $.extend(_this.city, CityModel);
                _this.resetInput();
            }

            _this.$emit('input', _this.city);
        });


        let skippedKeys = [
            9, // tab
            16, // shift
            17, // ctrl
            18, // alt
            19, // pause
            20, // caps
            27, // escape
            33, // page up
            34, // page down
            35, // end
            36, // home
            37, // left arrow
            38, // up arrow
            39, // right arrow
            40, // down arrow
            91, // left win
            92, // right win
            93, // select
            112, // F1
            113, // F2
            114, // f3
            115, // F4
            116, // F5
            117, // F6
            118, // F7
            119, // F8
            120, // F9
            121, // F10
            122, // F11
            123, // F12
            144, // num lock
            145 // scroll lock
        ];

        this.el.on('focus', function () {
            typed = false;
        }).on('blur', function () {
            if (typed && _this.city.name == null) {
                $.extend(_this.city, CityModel);
                _this.resetInput();

                _this.$emit('input', _this.city);
            }
        }).on('keydown', function (e) {
            if (e.which === 13 && $('.pac-container:visible').length) {
                return false;
            }
            if (skippedKeys.indexOf(e.which) >= 0) {
                return;
            }

            typed = true;
            _this.city.name = null;
        });

    }
});

Vue.component('tp-date', {
    template: '#tp-date',
    props: [
        'name',
        'value',
        'required',
        'endOfRange',
        'fromValue',
        'toValue',
        'debug',
        'disabledDays'
    ],
    data: function () {
        return {
            internalValue: null
        };
    },
    watch: {
        'value': function (newVal) {
            this.el.val(newVal);
            this.internalValue = newVal;

            toggleIsEmpty(this.el, this.formGroup);
        },
        'internalValue': function () {
            this.$emit('input', this.internalValue);
        },
    },
    created: function () {
        this.internalValue = this.value;
    },
    mounted: function () {
        this.el = $(this.$el);
        this.formGroup = this.el.closest('.form-group');

        let maxDate = moment().add(100, 'y').toDate();
        let minDate = moment().add(-10, 'y').toDate();

        // if (this.endOfRange) {
        this.el.datetimepicker({
            locale: $('html').attr('lang'),
            format: Constants.DATE_FORMAT,
            useCurrent: false, //Important! See issue #1075
            debug: this.debug,
            daysOfWeekDisabled: this.disabledDays,
            ignoreReadonly: true,
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove',
                inline: true
            }
        });
        // }

        let _this = this;
        this.el.on("dp.change", function (e) {
            _this.internalValue = _this.el.val();
        }).on('blur', function (e) {
            toggleIsEmpty(_this.el, _this.formGroup);
        });

        // Конец диапазона подписывается на изменение from
        this.$watch('fromValue', newVal => {
            let el = this.el.data("DateTimePicker");
            if (el) {
                if (newVal) {
                    el.minDate(moment(newVal, Constants.DATE_FORMAT));
                } else {
                    el.minDate(minDate);
                }
            }
        });

        // Начало диапазона подписывается ан изменение to
        this.$watch('toValue', newVal => {
            let el = this.el.data("DateTimePicker");
            console.log(newVal)
            if (el) {
                if (newVal) {
                    el.maxDate(moment(newVal, Constants.DATE_FORMAT));
                } else {
                    el.maxDate(maxDate);
                }
            }
        });
    }
});

Vue.component('tp-time', {
    template: '#tp-time',
    props: [
        'name',
        'value',
        'required'
    ],
    data: function () {
        return {
            internalValue: null
        };
    },
    watch: {
        'value': function (newVal) {
            this.el.val(newVal);
            this.internalValue = newVal;

            toggleIsEmpty(this.el, this.formGroup);
        },
        'internalValue': function () {
            this.$emit('input', this.internalValue);
        },
    },
    created: function () {
        this.internalValue = this.value;
    },
    mounted: function () {
        let timePicker = $(this.$el);
        timePicker.clockpicker({
            autoclose: true,
            placement: "top",
            align: "top"
        });
        timePicker.find('input').on('keypress', function () {
            return false;
        });

        let _this = this;


        this.el = timePicker.find('#' + this.name);
        this.formGroup = this.el.closest('.form-group');

        this.el.on('change', function () {
            _this.internalValue = _this.el.val();
        }).on('blur', function () {
            toggleIsEmpty(_this.el, _this.formGroup);
        });
    }
});

Vue.component('image-select-modal', {
    props: [
        'id',
        'ratio',
        'size',
        'event'
    ],
    data: function () {
        return {
            choose: true,
            uploadedImageURL: null,
            cropperConfig: null
        }
    },
    template: '#image-select-modal',
    computed: {
        modalId: function () {
            return this.id + 'Modal';
        }
    },
    methods: {
        onFileChange: function (e) {
            let files = e.target.files || e.dataTransfer.files;
            if (!files.length) {
                return;
            }

            file = files[0];

            if (/^image\/\w+$/.test(file.type)) {
                let _this = this;

                if (this.uploadedImageURL) {
                    URL.revokeObjectURL(this.uploadedImageURL);
                }

                this.uploadedImageURL = URL.createObjectURL(file);

                this.image.attr('src', this.uploadedImageURL);
                if (this.cropper) {
                    this.cropper.destroy();
                }
                this.cropper = new Cropper(this.image[0], {
                    aspectRatio: this.ratio,
                    toggleDragModeOnDblclick: false,
                    zoomOnWheel: false
                });

                this.choose = false;
            } else {
                window.alert('Please choose an image file.');
            }
        },
        saveImage: function () {
            let _this = this;
            this.cropper.getCroppedCanvas().toBlob(function (blob) {
                let formData = new FormData();
                formData.append('file', blob);
                formData.append('size', new Blob([JSON.stringify(_this.size)], {type: 'application/json'}));

                PrivateResources.photo.save({}, formData)
                    .then(response => {
                        bus.$emit(_this.event, response.body.url);
                        $('#' + _this.modalId).modal('hide');
                    });
            }, "image/jpeg");
        }
    },
    mounted: function () {
        let _this = this;
        this.image = $(this.$el).find('#selectedPhoto');

        $('#' + this.modalId)
            .on('hide.bs.modal', function (e) {
                if (e.target.id === _this.modalId) {
                    _this.choose = true;
                    _this.image.attr('src', '')
                }
            });
    }
});

Vue.component('image-cropper', {
    props: [
        'id',
        'value',
        'event',
        'blankImage'
    ],
    template: '#image-cropper',
    computed: {
        blankStyle: function () {
            return $.extend(true, {
                'background-image': "url('" + (this.blankImage ? this.blankImage : 'img/person_grey.png') + "')"
            });
        }
    },
    methods: {
        chooseFile: function () {
            $('#' + this.id).modal('show');
        },
        removeImage: function () {
            this.$emit('input', null);
        }
    },
    mounted: function () {
        bus.$on(this.event, url => {
            this.$emit('input', url);
        })
    }
});

Vue.component('tp-select', {
    template: '#tp-select',
    props: [
        'name',
        'value',
        'items',
        'required',
        'multiple',
        'maxOptions',
        'placeholder',
        'liveSearch',
        'liveSearchPlaceholder'
    ],
    data: function () {
        return {
            internalValue: null
        };
    },
    watch: {
        'value': function (newVal) {
            this.internalValue = newVal;

            Vue.nextTick(() => {
                this.el.selectpicker('refresh');
            });
        },
        'internalValue': function () {
            this.$emit('input', this.internalValue);
        },
    },
    created: function () {
        this.internalValue = this.value;
    },
    mounted: function () {
        let _this = this;
        this.el = $(this.$el);

        this.form = this.el.closest('form');

        if (this.required) {
            this.el.on('changed.bs.select', function (e) {
                _this.form.valid();
            });
        }

        this.$watch('items', function () {

            Vue.nextTick(() => {
                this.el.selectpicker('refresh');
            });
        })
    }
});