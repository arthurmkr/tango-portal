Vue.component('milonga-list', {
    data: function () {
        return {
            planned: [],
            completed: []
        }
    },
    template: '#milonga-list',
    methods: {
        addMilonga: function () {
            bus.$emit(EventType.EDIT_MILONGA, $.extend(true, {}, MilongaModel));
        },
        refreshList: function () {
            PrivateResources.milonga.get()
                .then(response => {
                    this.planned.splice(0, this.planned.length);
                    this.completed.splice(0, this.completed.length);

                    let now = new Date();
                    $.each(response.body, (index, item) => {
                        let startDateTime = moment(item.startDate + ' ' + item.startTime, "DD/MM/YYYY HH:mm");

                        if (startDateTime.isBefore(now)) {
                            this.completed.push(item);
                        } else {
                            this.planned.push(item);
                        }
                    });
                });
        }
    },
    created: function () {
        bus.$on(EventType.REFRESH_PLANNED_MILONGAS, () => {
            this.refreshList();
        });

        bus.$on(EventType.MILONGA_SAVED, milonga => {
            if(!milonga.eventId) {
                this.refreshList();
            }
        })
    }
});

Vue.component('milonga-list-table', {
    props: [
        'items',
        'title',
        'editEnabled',
        'repeatEnabled',
        'removeEnabled',
        'onAdd'
    ],
    data: function () {
        return {
        }
    },
    template: '#milonga-list-table',
    methods: {
        editMilonga: function(item) {
            if(this.editEnabled) {
                bus.$emit(EventType.EDIT_MILONGA, item);
            }
        },
        repeatMilonga: function(item) {
            if(this.repeatEnabled) {
                bus.$emit(EventType.REPEAT_MILONGA, item);
            }
        },
        removeMilonga: function (item) {
            if(!this.removeEnabled) {
                return;
            }

            this.selectedItem = item;
            let _this = this;
            bus.$emit(EventType.SHOW_CONFIRMATION_DIALOG, {
                message: I18N.CONFIRMATION_DELETE_MILONGA,
                onYes: function () {
                    PrivateResources.activity.remove({id: _this.selectedItem.id}).then(() => {
                        applyFor(_this.planned, _this.selectedItem, (index) => {
                            _this.planned.splice(index, 1);
                        });

                        // bus.$emit(EventType.NEW_ALERT, Alerts.success(I18N.EDIT_SINGLE_LESSON_TITLE));
                    });
                }
            })
        }
    }
});

Vue.component('milonga-modal', {
    template: '#milonga-modal',
    mixins: [ModalMixin],
    data: function () {
        return {
            obj: $.extend(true, {}, MilongaModel),
            djs: AppState.djs,
            config: {
                id: 'milonga',
                empty: MilongaModel,
                dialogClass: 'modal-lg',
                event: {
                    edit: EventType.EDIT_MILONGA,
                    saved: EventType.MILONGA_SAVED
                },
                titles: {
                    create: I18N.CREATE_MILONGA_TITLE,
                    edit: I18N.EDIT_MILONGA_TITLE,
                },
                resource: PrivateResources.milonga,
                validatorOptions: {
                    rules: {
                        address: {
                            required: true
                        },
                        name: {
                            required: true
                        },
                        startTime: {
                            required: true
                        },
                        endTime: {
                            required: true
                        },
                        startDate: {
                            required: true
                        },
                        djId: {
                            required: true
                        },
                        cost: {
                            required: true
                        },
                        currency: {
                            required: true
                        },
                        dj: {
                            required: true
                        }
                    }
                }
            }
        };
    },
    computed: {
        title: function() {
            if(!this.obj.id) {
                if(this.obj.copy) {
                    return I18N.REPEAT_MILONGA_TITLE;
                } else {
                    return I18N.CREATE_MILONGA_TITLE;
                }
            } else {
               return I18N.EDIT_MILONGA_TITLE;
            }
        }
    },
    mounted: function () {
        this.modalInit(function(_form) {
            let startDateEl = _form.find('#startDate').data("DateTimePicker");

            let minDate = moment().startOf('day').add(1, 'd').toDate();
            startDateEl.minDate(minDate);
        });

        bus.$on(EventType.REPEAT_MILONGA, item => {
            $.extend(this.obj, $.extend(true, {}, item));

            this.obj.id = null;
            this.obj.djId = null;
            this.obj.startDate =  moment(this.obj.startDate, Constants.DATE_FORMAT)
                .add(7, 'd').format(Constants.DATE_FORMAT);
            this.obj.copy = true;

            $('#' + this.modalId).modal('show');
        })
    }
});
