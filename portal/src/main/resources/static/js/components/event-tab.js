Vue.component('event-tab', {
    template: '#event-tab',
    data: function () {
        return {
            events: [],
            selectedEventId: null,
            selectedEvent: null
        }
    },
    methods: {
        refreshEvents: function (callback) {
            PrivateResources.event.selectListData()
                .then(response => {
                    this.events.splice(0, this.events.length);

                    $.each(response.body, (index, item) => {
                        item.started = moment().isAfter(moment(item.startDate, Constants.DATE_FORMAT));
                        this.events.push(item);
                    });

                    callback && callback();
                });
        },
        editEvent: function () {
            if (this.selectedEvent) {
                bus.$emit(EventType.EDIT_EVENT, this.selectedEvent);
            }
        },
        removeEvent: function () {
            let _this = this;
            bus.$emit(EventType.SHOW_CONFIRMATION_DIALOG, {
                message: I18N.CONFIRMATION_DELETE_EVENT,
                onYes: function () {
                    PrivateResources.event.remove({id: _this.selectedEventId}).then(() => {
                        _this.selectedEvent = null;
                        _this.selectedEventId = null;
                        _this.refreshEvents();
                    });
                }
            })
        },
        addMilonga: function () {
            let obj = $.extend(true, {}, MilongaModel);
            obj.eventId = this.selectedEventId;

            bus.$emit(EventType.EDIT_MILONGA, obj);
        },
        refreshEvent: function(id) {
            PrivateResources.event.get({id: id})
                .then(response => {
                    this.selectedEvent = response.body;
                    this.selectedEvent.started = moment().isAfter(moment(this.selectedEvent.startDate, Constants.DATE_FORMAT));
                });
        }
    },
    watch: {
        selectedEventId: function (newVal) {
            this.refreshEvent(newVal);
        }
    },
    mounted: function () {
        bus.$on(EventType.REFRESH_EVENT_TAB, () => {
            this.refreshEvents();
        });

        bus.$on(EventType.EVENT_SAVED, event => {
            $.extend(this.selectedEvent, event);

            this.refreshEvents(() => {
                this.selectedEventId = event.id;
            });
        });

        bus.$on(EventType.MILONGA_SAVED, milonga => {
            console.log('On save [' +milonga.eventId+ ' == ' + this.selectedEventId+']')
            if(milonga.eventId === this.selectedEventId) {
                this.refreshEvent(this.selectedEventId);
            }
        })
    }
});
