Vue.component('room-list', {
    props: ['tableConfig'],
    data: function () {
        let _this = this;
        return {
            selectedItem: null,
            config: {
                resource: _this.tableConfig.resource,
                alert: {
                    removed: _this.tableConfig.alert.removed,
                    saved: _this.tableConfig.alert.saved
                },
                event: {
                    edit: _this.tableConfig.event.edit,
                    saved: _this.tableConfig.event.saved
                },
                confirmDelete: {
                    message: I18N.CONFIRMATION_DELETE_ROOM
                }
            }
        }
    },
    mixins: [TableMixin],
    computed: {
        items: function () {
            return this.tableConfig.items;
        }
    },
    template: '#room-list'
});

Vue.component('room-modal', {
    template: '#room-modal',
    mixins: [ModalMixin],
    data: function () {
        return {
            obj: {
                name: null,
                address: {
                    value: null,
                    pid: null,
                    lat: null,
                    lng: null,
                    city: {
                        name: null
                    }
                }
            },
            config: {
                id: 'room',
                empty: RoomModel,
                event: {
                    edit: EventType.EDIT_SCHOOL_ROOM,
                    saved: EventType.SCHOOL_ROOM_SAVED
                },
                titles: {
                    create: I18N.CREATE_ROOM_TITLE,
                    edit: I18N.EDIT_ROOM_TITLE,
                },
                resource: PrivateResources.schoolRoom,
                validatorOptions: {
                    rules: {
                        address: {
                            required: true
                        },
                        name: {
                            required: true
                        }
                    }
                }
            }
        };
    },
    mounted: function () {
        this.modalInit();
    }
});
