Vue.component('teacher-list', {
    data: () => {
        return {
            selectedItem: null,
            config: {
                resource: PrivateResources.teacher,
                alert: {
                    removed: I18N.TEACHER_IS_REMOVED,
                    saved: I18N.TEACHER_IS_SAVED
                },
                event: {
                    edit: EventType.EDIT_TEACHER,
                    saved: EventType.TEACHER_SAVED
                },
                confirmDelete: {
                    message: I18N.CONFIRMATION_DELETE_TEACHER
                }
            }
        }
    },
    mixins: [TableMixin],
    computed: {
        items: function () {
            return AppState.teachers;
        }
    },
    template: '#teacher-list'
});

Vue.component('teacher-modal', {
    template: '#teacher-modal',
    mixins: [ModalMixin],
    data: function () {
        return {
            obj: $.extend(true, {}, TeacherModel),
            config: {
                id: 'teacher',
                empty: TeacherModel,
                event: {
                    edit: EventType.EDIT_TEACHER,
                    saved: EventType.TEACHER_SAVED
                },
                titles: {
                    create: I18N.CREATE_TEACHER_TITLE,
                    edit: I18N.EDIT_TEACHER_TITLE,
                },
                resource: PrivateResources.teacher,
                validatorOptions: {
                    rules: {
                        firstName: {
                            required: true
                        },
                        secondName: {
                            required: true
                        }
                    }
                }
            }
        };
    },
    mounted: function () {
        // Отключить закрытие модального окна на mouse up
// https://stackoverflow.com/questions/12669607/how-to-close-bootstrap-modal-only-when-user-clicks-on-close-image
        this.modalInit();
    }
});