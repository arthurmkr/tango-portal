Vue.component('school-modal', {
    template: '#school-modal',
    mixins: [ModalMixin],
    data: function () {
        return {
            obj: $.extend(true, {}, SchoolModel),
            config: {
                id: 'school',
                empty: SchoolModel,
                event: {
                    edit: EventType.EDIT_SCHOOL,
                    saved: EventType.SCHOOL_SAVED
                },
                titles: {
                    create: I18N.CREATE_SCHOOL_TITLE,
                    edit: I18N.EDIT_SCHOOL_TITLE,
                },
                resource: PrivateResources.school,
                validatorOptions: {
                    rules: {
                        address: {
                            required: true
                        },
                        name: {
                            required: true
                        },
                        workPhone: {
                            phone: true
                        },
                        mobilePhone: {
                            required: true,
                            phone: true
                        },
                        siteUrl: {
                            url: true
                        },
                        fbUrl: {
                            url: true
                        },
                        vkUrl: {
                            url: true
                        }
                    }
                }
            }
        }
    },
    mounted: function () {
        this.modalInit();
    }
});
