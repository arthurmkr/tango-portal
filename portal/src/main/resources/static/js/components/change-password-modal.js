Vue.component('change-password-modal', {
    template: '#change-password-modal',
    data: function () {
        return {
            obj: $.extend(true, {}, ChangePasswordModel),
            error: null,
            unique: new Date().getTime()
        };
    },
    mounted: function () {
        let form = $(this.$el).find('form');
        let _this = this;
        form.validate({
            submitHandler: function () {
                PrivateResources.profile.changePassword({}, _this.obj)
                    .then(response => {
                        $('#changePasswordModal').modal('hide');
                    }, error => {
                        _this.error = error.body.message;
                    });

                return false;
            },
            rules: {
                oldPassword: {
                    required: true
                },
                newPassword: {
                    required: true,
                    minlength: 8
                },
                confirmPassword: {
                    equalTo: '#newPassword'
                }
            }
        });

        $('#changePasswordModal')
            .on('hide.bs.modal', e => {
                $.extend(this.obj, ChangePasswordModel);
                $(form).find('.label-floating').addClass('is-empty');

                form.validate().resetForm();

                this.unique = new Date().getTime();
                this.error = null;
            });
    }
});