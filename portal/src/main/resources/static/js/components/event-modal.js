Vue.component('event-modal', {
    template: '#event-modal',
    mixins: [ModalMixin],
    data: function () {
        return {
            obj: $.extend(true, {}, EventModel),
            config: {
                id: 'event',
                empty: EventModel,
                dialogClass: 'modal-lg',
                event: {
                    edit: EventType.EDIT_EVENT,
                    saved: EventType.EVENT_SAVED
                },
                titles: {
                    create: I18N.CREATE_EVENT_TITLE,
                    edit: I18N.EDIT_EVENT_TITLE,
                },
                resource: PrivateResources.event,
                validatorOptions: {
                    rules: {
                        city: {
                            required: true
                        },
                        name: {
                            required: true
                        },
                        siteUrl: {
                            url: true
                        },
                        fbUrl: {
                            url: true
                        },
                        vkUrl: {
                            url: true
                        },
                        startDate: {
                            required: true
                        },
                        endDate: {
                            required: true
                        }
                    }
                }
            }
        }
    },
    mounted: function () {
        this.modalInit(function(_form) {
            let startDateEl = _form.find('#startDate').data("DateTimePicker");
            let endDateEl = _form.find('#endDate').data("DateTimePicker");

            let minDate = moment().startOf('day').add(4, 'd').toDate();
            startDateEl.minDate(minDate);
            endDateEl.minDate(minDate);
        });
    }
});
