const CityModel = Object.freeze({
    name: null,
    pid: null
});

const AddressModel = Object.freeze({
    value: null,
    pid: null,
    lat: null,
    lng: null,
    city: $.extend(true, {}, CityModel)
});

const ActivityModel = Object.freeze({
    id: null,
    name: null,
    startDate: null,
    endDate: null,
    startTime: null,
    duration: null,
    regular: true,
    dayOfWeek: 'MONDAY',
    teachers: [],
    address: $.extend(true, {}, AddressModel),
    schoolId: null,
    type: 'LESSON',
    cost: null,
    roomId: null
});

const SchoolModel = Object.freeze({
    id: 0,
    name: null,
    workPhone: null,
    mobilePhone: null,
    siteUrl: null,
    fbUrl: null,
    vkUrl: null,
    photo: null,
    address: $.extend(true, {}, AddressModel),
    defaultDuration: null
});

const TeacherModel = Object.freeze({
    id: null,
    firstName: null,
    secondName: null,
    resume: null,
    avatar: null
});

const RolesModel = Object.freeze({
    dj: false,
    teacher: false,
    schoolLead: false,
    organizer: false,
    photographer: false,
    schoolAdmin: false,
    couturier: false
});

const ProfileModel = Object.freeze({
    id: null,
    firstName: null,
    secondName: null,
    avatar: null,
    vkProfile: null,
    fbProfile: null,
    email: null,
    referralLink: null,
    djNick: null,
    roles: $.extend(true, {}, RolesModel),
    city: $.extend(true, {}, CityModel)
});

const RoomModel = Object.freeze({
    id: null,
    name: null,
    address: $.extend(true, {}, AddressModel)
});

const MilongaModel = Object.freeze({
    id: null,
    name: null,
    startDate: null,
    startTime: null,
    endTime: null,
    duration: null,
    address: $.extend(true, {}, AddressModel),
    cost: null,
    currency: null,
    djId: null,
    photo: null,
    vkUrl: null,
    fbUrl: null,
    eventId: null
});

const ChangePasswordModel = Object.freeze({
    oldPassword: null,
    newPassword: null,
    confirmPassword: null
});

const EventModel = Object.freeze({
    id: null,
    name: null,
    siteUrl: null,
    fbUrl: null,
    vkUrl: null,
    photo: null,
    city: $.extend(true, {}, CityModel),
    startDate: null,
    endDate: null
});

