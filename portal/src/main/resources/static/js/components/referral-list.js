Vue.component('referral-list', {
    data: () => {
        return {
            selectedItem: null,
            removeConfirmationConfig: null,
            approveConfirmationConfig: null
        }
    },
    computed: {
        items: function () {
            return AppState.referrals;
        },
        referralLink: function() {
            return AppState.profile.referralLink;
        }
    },
    methods: {
        removeItem: function (item) {
            this.selectedItem = item;
            bus.$emit(EventType.SHOW_CONFIRMATION_DIALOG, this.removeConfirmationConfig);
        },
        approveItem: function(item) {
            this.selectedItem = item;
            bus.$emit(EventType.SHOW_CONFIRMATION_DIALOG, this.approveConfirmationConfig);
        },
        getReferralLink: function() {
            PrivateResources.referral.getReferralLink()
                .then(response => {
                    return response.text();
                })
                .then(text => {
                    AppState.profile.referralLink = text;
                });
        },
        copyReferralLink: function() {
            let link = $('#referralLink');
            link.select();
            document.execCommand("copy");
            link.blur();
        }
    },
    template: '#referral-list',
    mounted: function () {
        let _this = this;
        this.removeConfirmationConfig = {
            message: I18N.CONFIRMATION_CANCEL_REGISTRATION,
            onYes: function () {
                PrivateResources.referral.remove({id: _this.selectedItem.id}).then(() => {
                    applyFor(_this.items, _this.selectedItem, (index) => {
                        _this.items.splice(index, 1);
                    });
                });
            }
        };

        this.approveConfirmationConfig = {
            message: I18N.CONFIRMATION_CONFIRM_REGISTRATION,
            onYes: function () {
                PrivateResources.referral.approve({id: _this.selectedItem.id}, {}).then(() => {
                    applyFor(_this.items, _this.selectedItem, (index) => {
                        _this.items.splice(index, 1);
                    });
                });
            }
        };

        PrivateResources.referral.get().then(response => {
            copyArray(response.body, this.items);
        });
    }
});