Vue.component('profile-modal', {
    template: '#profile-modal',
    mixins: [ModalMixin],
    data: function () {
        return {
            obj: $.extend(true, {}, ProfileModel),
            config: {
                id: 'profile',
                empty: ProfileModel,
                event: {
                    edit: EventType.EDIT_PROFILE,
                    saved: EventType.PROFILE_SAVED
                },
                titles: {
                    edit: I18N.EDIT_PROFILE_TITLE,
                },
                resource: PrivateResources.profile,
                validatorOptions: {
                    rules: {
                        firstName: {
                            required: true
                        },
                        secondName: {
                            required: true
                        },
                        fbProfile: {
                            url: true
                        },
                        vkProfile: {
                            url: true
                        }
                    }
                }
            }
        };
    },
    mounted: function () {
        this.modalInit();
    }
});