Vue.component('single-lesson-modal', {
    template: '#single-lesson-modal',
    mixins: [ModalMixin],
    data: function () {
        return {
            teachers: AppState.teachers,
            rooms: AppState.schoolRooms,
            obj: $.extend(true, {}, ActivityModel),
            config: {
                empty: $.extend(true, {}, ActivityModel),
                id: 'singleLesson',
                model: ActivityModel,
                event: {
                    edit: EventType.EDIT_SINGLE_LESSON,
                    saved: EventType.SINGLE_LESSON_SAVED
                },
                titles: {
                    create: I18N.CREATE_SINGLE_LESSON_TITLE,
                    edit: I18N.EDIT_SINGLE_LESSON_TITLE,
                },
                resource: PrivateResources.activity,
                validatorOptions: {
                    rules: {
                        name: {
                            required: true
                        },
                        teachers: {
                            required: true
                        },
                        duration: {
                            required: true
                        },
                        startTime: {
                            required: true
                        },
                        startDate: {
                            required: true
                        }
                    }
                }
            },
        }
    },
    mounted: function () {
        this.modalInit(null, function (_form, _this) {
            _this.obj.regular = false;
            _this.obj.schoolId = AppState.school.info.id;
            _this.obj.duration = _this.obj.duration > 0 ? _this.obj.duration : AppState.school.info.defaultDuration;
            _this.obj.roomId = _this.obj.roomId > 0 ? _this.obj.roomId : AppState.schoolRooms[0].id;
        });
    }
});