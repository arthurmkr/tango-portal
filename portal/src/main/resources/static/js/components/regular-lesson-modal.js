Vue.component('regular-lesson-modal', {
    template: '#regular-lesson-modal',
    mixins: [ModalMixin],
    data: function () {
        return {
            teachers: AppState.teachers,
            rooms: AppState.schoolRooms,
            obj: $.extend(true, {}, ActivityModel),
            config: {
                id: 'regularLesson',
                model: ActivityModel,
                event: {
                    edit: EventType.EDIT_REGULAR_LESSON,
                    saved: EventType.REGULAR_LESSON_SAVED
                },
                titles: {
                    create: I18N.CREATE_REGULAR_LESSON_TITLE,
                    edit: I18N.EDIT_REGULAR_LESSON_TITLE,
                },
                resource: PrivateResources.activity,
                validatorOptions: {
                    rules: {
                        name: {
                            required: true
                        },
                        teachers: {
                            required: true
                        },
                        duration: {
                            required: true
                        },
                        startTime: {
                            required: true
                        },
                        startDate: {
                            required: true
                        }
                    }
                }
            },
        }
    },
    mounted: function () {
        this.modalInit(null, function (_form, _this) {
            _this.obj.schoolId = AppState.school.info.id;
            _this.obj.duration = _this.obj.duration > 0 ? _this.obj.duration : AppState.school.info.defaultDuration;
            _this.obj.roomId = _this.obj.roomId > 0 ? _this.obj.roomId : AppState.schoolRooms[0].id;
        });
    }
});