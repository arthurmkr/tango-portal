new Vue({
    el: '#loginModal',
    data: {
        username: null,
        password: null,
        error: null
    },
    mounted: function () {
        let _this = this;
        $('#loginModal')
            .on('hide.bs.modal', function (e) {
                _this.error = null;
            });

        $('#loginForm').validate({
            submitHandler: function () {
                Vue.http.post("/api/public/auth/login", {
                    username: _this.username,
                    password: _this.password
                }).then(response => {
                    let body = response.body;
                    if (body.errorCode) {
                        _this.error = body.errorCode;
                    } else {
                        location.reload();
                    }
                });

                return false;
            },
            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                }
            }
        });
    }
});