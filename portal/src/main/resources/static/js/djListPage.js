function initDjListPage() {
    new Vue({
        el: '#djList',
        data: {
            firstLoaded: false,
            pageData: {
                items: []
            },
            filter: {
                city: $.extend(true, {}, CityModel),
            }
        },
        computed: {
            isEmpty: function () {
                return this.pageData.items.length === 0;
            }
        },
        methods: {
            refreshDjList: function () {
                PublicResources.dj.get({
                    city: this.filter.city.name
                }).then(response => {
                    this.pageData.items = response.body;
                    this.firstLoaded = true;
                });
            },
            initFilter: function (city) {
                this.filter.city.name = city;

                this.refreshDjList();
            }
        },
        mounted: function () {
            PublicResources.dictionary.tangoMapByType({type: 'DJ'})
                .then(response => {
                    let mapData = response.body;

                    let map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 2,
                        center: {
                            lat: 18,
                            lng: 10
                        }
                    });

                    let cityMarkers = mapData.markers;
                    for (let i = 0; i < cityMarkers.length; i++) {
                        let cityMarker = cityMarkers[i];

                        let marker = new google.maps.Marker({
                            position: {
                                lat: cityMarker.lat,
                                lng: cityMarker.lng
                            },
                            map: map,
                            label: '' + cityMarker.count
                        });

                        marker.addListener('click', () => {
                            this.filter.city.name = cityMarker.name;
                        });
                    }

                    this.initialized = true;
                });

            this.$watch('pageData', () => {
                    Vue.nextTick(() => {
                        this.grid = $('.grid').packery({
                            itemSelector: '.grid-item',
                            percentPosition: true
                        });

                        this.grid.packery('reloadItems')
                            .packery();
                    })
            }, {deep: true});

            let urlFilter = {
                city: null
            };

            urlToJson(urlFilter);
            this.filter.city.name = urlFilter.city;
            this.refreshDjList();

            this.$watch('filter', () => {
                appendFilterToUrl({
                    city: this.filter.city.name
                });

                this.refreshDjList();
            }, {deep: true});


        }
    })
}