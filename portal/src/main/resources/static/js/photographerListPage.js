function initPhotographerListPage() {
    new Vue({
        el: '#photographerList',
        data: {
            firstLoaded: false,
            pageData: {
                items: []
            },
            filter: {
                city: $.extend(true, {}, CityModel),
            }
        },
        computed: {
            isEmpty: function () {
                return this.pageData.items.length === 0;
            }
        },
        mounted: function () {
            PublicResources.photographer.get().then(response => {
                this.pageData.items = response.body;
                this.firstLoaded = true;

                Vue.nextTick(() => {
                    this.grid = $('.grid').packery({
                        itemSelector: '.grid-item',
                        percentPosition: true
                    });

                    this.grid.packery('reloadItems')
                        .packery();
                })
            });
        }
    })
}