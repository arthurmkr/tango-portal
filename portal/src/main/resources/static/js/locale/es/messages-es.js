﻿let I18N = Object.freeze({
    // Mensajes
    SCHOOL_IS_SAVED: 'La escuela guardada',
    TEACHER_IS_SAVED: 'El profesor guardado',
    TEACHER_IS_REMOVED: 'El profesor eliminado',
    ROOM_IS_SAVED: 'La sala guardada',
    ROOM_IS_REMOVED: 'La sala eliminada',

    // Los días de la semana
    MONDAY: 'Lunes',
    TUESDAY: 'Martes',
    WEDNESDAY: 'Miércoles',
    THURSDAY: 'Jueves',
    FRIDAY: 'Viernes',
    SATURDAY: 'Sábado',
    SUNDAY: 'Domingo',
//
    ACTIVITY_DEFAULT_LABEL_PRACTICE: 'Práctica',
    ACTIVITY_DEFAULT_LABEL_LESSON: 'Clase',
    // Títulos de las ventanas modales
    CREATE_SCHOOL_TITLE: 'Crear una escuela',
    EDIT_SCHOOL_TITLE: 'Editar la escuela',
    CREATE_TEACHER_TITLE: 'Crear cuenta de profesor',
    EDIT_TEACHER_TITLE: 'Editar cuenta de profesor',
    CREATE_REGULAR_LESSON_TITLE: 'Crear una clase regular',
    EDIT_REGULAR_LESSON_TITLE: 'Editar la clase regular',
    CREATE_SINGLE_LESSON_TITLE: 'Crear una clase singular',
    EDIT_SINGLE_LESSON_TITLE: 'Editar la clase singular',
    CREATE_PRACTICE_TITLE: 'Crear una práctica',
    EDIT_PRACTICE_TITLE: 'Editar la práctica',
    CREATE_ROOM_TITLE: 'Crear una sala',
    EDIT_ROOM_TITLE: 'Editar la sala',
    EDIT_PROFILE_TITLE: 'Editar el perfil',
    CREATE_MILONGA_TITLE: 'Crear una milonga',
    EDIT_MILONGA_TITLE: 'Editar la milonga',
    REPEAT_MILONGA_TITLE: 'Repetir la milonga',
    CREATE_EVENT_TITLE: 'Crear un evento',
    EDIT_EVENT_TITLE: 'Editar el evento',
//
    ERROR_USER_IS_INACTIVE: 'El usario no activado',
    ERROR_BAD_CREDENTIALS: 'Contraseña o usario incorrecto(s)',
    ERROR_MAX_FILE_SIZE_EXCEEDED: 'El límite del tamano del fichero está sobrepasado (5 mb)',
    ERROR_INCORRECT_PHONE: 'Por favor, ponga un número de teléfono',
    ERROR_EMAIL_IS_ALREADY_USED: 'El correo indicado ya se había usado',
    ERROR_INCORRECT_CONFIRM_PASSWORD: 'Repetir contraseña incorrecta',
    ERROR_INCORRECT_OLD_PASSWORD: 'Antigua contraseña incorrecta',
    ERROR_PASSWORD_IS_TOO_SHORT: 'La contraseña es demasiado corta',
//
    CONFIRMATION_DELETE_LESSON: '¿Está seguro de que quiere eliminar la clase?',
    CONFIRMATION_DELETE_PRACTICE: '¿Está seguro de que quiere eliminar la práctica?',
    CONFIRMATION_DELETE_ROOM: '¿Está seguro de que quiere eliminar la sala?',
    CONFIRMATION_DELETE_TEACHER:  '¿Está seguro de que quiere eliminar la cuenta de profesor?',
    CONFIRMATION_CANCEL_REGISTRATION: '¿Está seguro de que quiere cancelar creación de la cuenta?',
    CONFIRMATION_CONFIRM_REGISTRATION: '¿Está seguro de que quiere confirmar la cuenta?',
    CONFIRMATION_DELETE_MILONGA: '¿Está seguro de que quiere eliminar la milonga?',
    CONFIRMATION_DELETE_EVENT: '¿Está seguro de que quiere eliminar el evento?'
});
