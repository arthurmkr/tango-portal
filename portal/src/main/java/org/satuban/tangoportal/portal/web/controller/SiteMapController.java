package org.satuban.tangoportal.portal.web.controller;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.satuban.tangoportal.portal.service.SiteMapService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Danilov on 21.08.2017.
 */
@Controller
public class SiteMapController {
    private final SiteMapService siteMapService;
    private final Configuration configuration;

    public SiteMapController(SiteMapService siteMapService,
                             Configuration configuration) {
        this.siteMapService = siteMapService;
        this.configuration = configuration;
    }

    @RequestMapping(value = "/sitemap.xml", method = RequestMethod.GET)
    public void getSiteMapXml (HttpServletRequest request, HttpServletResponse response, Model model) throws IOException, TemplateException {
        model.addAttribute("cities", siteMapService.getCities("http://"
                + request.getServerName()));

        Template siteMapTemplate = configuration.getTemplate("sitemap.ftl");

        siteMapTemplate.process(model, response.getWriter());
    }
}
