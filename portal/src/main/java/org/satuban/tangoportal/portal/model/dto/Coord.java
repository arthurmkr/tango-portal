package org.satuban.tangoportal.portal.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Artur on 15.07.2017.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Coord {
    private double lat;
    private double lng;
}
