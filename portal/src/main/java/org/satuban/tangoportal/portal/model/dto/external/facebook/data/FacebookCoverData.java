package org.satuban.tangoportal.portal.model.dto.external.facebook.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.satuban.tangoportal.portal.model.dto.external.facebook.FacebookCover;

/**
 * Created by Danilov on 06.10.2017.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = false)
public class FacebookCoverData {
    private FacebookCover cover;
}
