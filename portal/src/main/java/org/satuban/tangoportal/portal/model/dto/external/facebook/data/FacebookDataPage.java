package org.satuban.tangoportal.portal.model.dto.external.facebook.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.satuban.tangoportal.portal.model.dto.external.facebook.FacebookPaging;

import java.util.List;

/**
 * Created by Danilov on 25.09.2017.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FacebookDataPage<T> {
    private List<T> data;
    private FacebookPaging paging;
}
