package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;

/**
 * Created by Artur on 15.07.2017.
 */
@Data
public class CitySimpleMarker {
    private String pid;
    private float lat;
    private float lng;
    private int count;
    private String name;
}
