package org.satuban.tangoportal.portal.model.dto.external.facebook.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Created by Danilov on 06.10.2017.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FacebookData<T> {
    private T data;
}