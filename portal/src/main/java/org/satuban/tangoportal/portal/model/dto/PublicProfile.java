package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;

/**
 * Created by Artur on 23.07.2017.
 */
@Data
public class PublicProfile {
    private String firstName;
    private String secondName;
    private String djNick;
    private String avatar;
    private String vkProfile;
    private String fbProfile;
    private String city;
}
