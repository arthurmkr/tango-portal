package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.dto.*;
import org.satuban.tangoportal.portal.model.entity.Address;
import org.satuban.tangoportal.portal.model.entity.City;
import org.satuban.tangoportal.portal.model.enums.MapType;

/**
 * Created by Artur on 06.06.2017.
 */
public interface AddressService {
    Address getOrCreateAddress(AddressDto address);

    Address obtainAddressFromGoogle(float lat, float lng);

    CityDto getCityDto(Long id);

    AddressDto getById(Long id);

    City getOrCreateCity(String cityName);

    TangoMap<CityMarker> getCityMarkers();
    TangoMap<CitySimpleMarker> getCityMarkersByType(MapType type);
}
