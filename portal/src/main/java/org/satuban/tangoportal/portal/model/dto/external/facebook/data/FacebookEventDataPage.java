package org.satuban.tangoportal.portal.model.dto.external.facebook.data;

import org.satuban.tangoportal.portal.model.dto.external.facebook.FacebookEventDto;

/**
 * Created by Danilov on 25.09.2017.
 */
public class FacebookEventDataPage extends FacebookDataPage<FacebookEventDto> {
}
