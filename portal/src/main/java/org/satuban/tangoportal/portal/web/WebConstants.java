package org.satuban.tangoportal.portal.web;

/**
 * Created by Artur on 08.06.2017.
 */
public interface WebConstants {
    String API = "/api";
    String PUBLIC_API = API + "/public";
    String PRIVATE_API = API + "/private";
}
