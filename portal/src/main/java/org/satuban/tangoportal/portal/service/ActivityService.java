package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.dto.ActivityDto;
import org.satuban.tangoportal.portal.model.dto.Playbill;
import org.satuban.tangoportal.portal.model.filters.PlaybillFilter;

import java.util.List;

/**
 * Created by Artur on 19.06.2017.
 */
public interface ActivityService {
    void save(ActivityDto request);

    void saveWithOutValidate(ActivityDto activityDto);

    ActivityDto saveMilonga(ActivityDto request);

    Playbill getActivities(PlaybillFilter filter);

    ActivityDto getById(long id);

    void remove(long id);

    List<ActivityDto> getMilongasForUser();
}
