package org.satuban.tangoportal.portal.dao.impl;

import org.satuban.tangoportal.portal.dao.TeacherDao;
import org.satuban.tangoportal.portal.model.entity.Teacher;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

/**
 * Created by Artur on 07.06.2017.
 */
@Repository
public class TeacherDaoImpl extends AbstractDao<Teacher> implements TeacherDao {
    TeacherDaoImpl(NamedParameterJdbcTemplate template) {
        super(template, Teacher.class);
    }

    @Override
    public List<Teacher> getBySchool(long schoolId) {
        return template.query("SELECT * FROM teacher WHERE school_id = :schoolId", Collections.singletonMap("schoolId", schoolId), rowMapper);
    }
}
