package org.satuban.tangoportal.portal.web.controller.api.priv;

import org.satuban.tangoportal.portal.service.CrudService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Artur on 11.06.2017.
 */
public abstract class CrudApiController<T> {
    protected abstract CrudService<T> getService();

    @GetMapping
    public List<T> get() {
        return getService().getAll();
    }

    @DeleteMapping("/{id}")
    public HttpStatus remove(@PathVariable long id) {
        getService().remove(id);
        return HttpStatus.OK;
    }

    @PostMapping
    public T save(@RequestBody T dto) {
        return getService().save(dto);
    }
}
