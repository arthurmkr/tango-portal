package org.satuban.tangoportal.portal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Artur on 16.05.2017.
 */
@SpringBootApplication
@PropertySource(value = "app.properties")
@PropertySource(value = "env.properties", ignoreResourceNotFound = true)
@PropertySource(value = "env_local.properties", ignoreResourceNotFound = true)
@PropertySource(value = "file:./env.properties", ignoreResourceNotFound = true)
@PropertySource(value = "file:/etc/tangoportal/env.properties", ignoreResourceNotFound = true)
public class Application extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

}
