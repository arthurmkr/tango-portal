package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.dto.WeekTimetable;

import java.util.Date;

/**
 * Created by Artur on 13.06.2017.
 */
public interface TimetableService {
    WeekTimetable getForSchool(Date startFrom);

    WeekTimetable getForSchool(long schoolId, Date today);
}
