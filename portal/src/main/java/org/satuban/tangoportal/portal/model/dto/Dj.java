package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;

/**
 * Created by Artur on 07.08.2017.
 */
@Data
public class Dj {
    private long id;
    private String fullName;
    private String avatar;
    private String nick;
    private Long cityId;
}
