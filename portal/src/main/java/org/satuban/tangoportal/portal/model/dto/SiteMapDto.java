package org.satuban.tangoportal.portal.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.satuban.tangoportal.portal.model.enums.SiteMapType;

/**
 * Created by Danilov on 20.08.2017.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SiteMapDto {
    private String cityName;
    private String url;
    private SiteMapType type;
}
