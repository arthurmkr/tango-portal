package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.dto.CabinetInfo;
import org.satuban.tangoportal.portal.model.dto.DailyEventMap;
import org.satuban.tangoportal.portal.model.entity.School;
import org.satuban.tangoportal.portal.model.dto.ActivityDto;

import java.util.List;

/**
 * Created by Artur on 18.05.2017.
 */
public interface CabinetService {
    List<School> getAllSchools();

    void saveLesson(ActivityDto request);

    CabinetInfo getCabinetInfo();

    School getSchool(long schoolId);

    DailyEventMap getDailyEventMap(String city);
}