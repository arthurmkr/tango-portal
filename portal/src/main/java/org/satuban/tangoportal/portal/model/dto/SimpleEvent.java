package org.satuban.tangoportal.portal.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.satuban.tangoportal.portal.model.enums.ActivityType;
import org.satuban.tangoportal.portal.model.enums.Currency;
import org.satuban.tangoportal.portal.model.json.TimeSerializer;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Artur on 16.05.2017.
 */
@Data
public class SimpleEvent implements Serializable {
    private static final long serialVersionUID = -2305382011348305080L;

    private ActivityType type;
    private String name;
    @JsonSerialize(using = TimeSerializer.class)
    private Date startTime;
    @JsonSerialize(using = TimeSerializer.class)
    private Date endTime;
    private Long schoolId;
    private String schoolName;
    private AddressDto address;
    private Long firstTeacherId;
    private Long secondTeacherId;
    private int cost;
    private Currency currency;
    private Dj dj;
    private String vkUrl;
    private String fbUrl;
    private String photo;
}
