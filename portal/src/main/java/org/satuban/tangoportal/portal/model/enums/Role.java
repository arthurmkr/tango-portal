package org.satuban.tangoportal.portal.model.enums;

/**
 * Created by Artur on 20.05.2017.
 */
public enum Role {
    USER;

    private final String role;

    Role() {
        role = "ROLE_" + toString();
    }

    public String role() {
        return role;
    }
}
