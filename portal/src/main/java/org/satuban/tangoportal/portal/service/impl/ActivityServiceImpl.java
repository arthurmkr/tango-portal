package org.satuban.tangoportal.portal.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.satuban.tangoportal.portal.dao.*;
import org.satuban.tangoportal.portal.error.ErrorCode;
import org.satuban.tangoportal.portal.error.TPCityNotFoundException;
import org.satuban.tangoportal.portal.error.TPException;
import org.satuban.tangoportal.portal.model.dto.*;
import org.satuban.tangoportal.portal.model.dto.teacher.TeacherLink;
import org.satuban.tangoportal.portal.model.entity.*;
import org.satuban.tangoportal.portal.model.enums.ActivityType;
import org.satuban.tangoportal.portal.model.filters.PlaybillFilter;
import org.satuban.tangoportal.portal.service.ActivityService;
import org.satuban.tangoportal.portal.service.AddressService;
import org.satuban.tangoportal.portal.service.UserHolder;
import org.satuban.tangoportal.portal.util.ConverterUtil;
import org.satuban.tangoportal.portal.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

/**
 * Created by Artur on 19.06.2017.
 */
@Slf4j
@Service
@Transactional
public class ActivityServiceImpl implements ActivityService {
    private static final ActivityValidator DEFAULT = request -> {
        throw new RuntimeException("Error");
    };


    private final UserHolder holder;
    private final ConverterUtil converter;
    private final UserDao userDao;
    private final ActivityDao activityDao;
    private final AddressService addressService;
    private final SchoolDao schoolDao;
    private final RoomDao roomDao;
    private final TeacherDao teacherDao;
    private final GrabbedGroupDao grabbedGroupDao;

    private Map<ActivityType, ActivityValidator> validators = new HashMap<>();

    {
        validators.put(ActivityType.LESSON, new BaseValidator() {
            @Override
            public void validate(ActivityDto request) {
                super.validate(request);

                if (request.getSchoolId() != null) {// школьный урок
                    School school = schoolDao.findByUserId(holder.getUserId());

                    if (school == null || !Objects.equals(school.getId(), request.getSchoolId())) {
                        // TODO error
                        throw new RuntimeException();
                    }
                } else { // фестивальный урок

                }

                if (CollectionUtils.isEmpty(request.getTeachers())) {
                    throw new RuntimeException(); // TODO error
                }
            }
        });
        validators.put(ActivityType.PRACTICE, new ActivityValidator() {
            @Override
            public void validate(ActivityDto request) {

            }
        });
        validators.put(ActivityType.MILONGA, new ActivityValidator() {
            @Override
            public void validate(ActivityDto request) {

            }
        });
        validators.put(ActivityType.ACTION, new ActivityValidator() {
            @Override
            public void validate(ActivityDto request) {

            }
        });
    }

    @Autowired
    public ActivityServiceImpl(UserHolder holder, ConverterUtil converter, UserDao userDao, ActivityDao activityDao, AddressService addressService, SchoolDao schoolDao, RoomDao roomDao, TeacherDao teacherDao, GrabbedGroupDao grabbedGroupDao) {
        this.holder = holder;
        this.converter = converter;
        this.userDao = userDao;
        this.activityDao = activityDao;
        this.addressService = addressService;
        this.schoolDao = schoolDao;
        this.roomDao = roomDao;
        this.teacherDao = teacherDao;
        this.grabbedGroupDao = grabbedGroupDao;
    }

    @Override
    public void save(ActivityDto request) {
        ActivityValidator validator = validators.getOrDefault(request.getType(), DEFAULT);

        validator.validate(request);

        saveWithOutValidate(request);
    }

    @Override
    public void saveWithOutValidate(ActivityDto request) {
        Activity activity = Optional.ofNullable(request.getId())
                .map(activityDao::findById)
                .orElse(null);

        if (activity == null) {
            activity = new Activity();

            activity.setSchoolId(request.getSchoolId());
        }

        activity.setName(request.getName());

        if (!request.isRegular()) {
            request.setDayOfWeek(Utils.convertToMondayWeekEnum(request.getStartDate()));
        }

        activity.setDayOfWeek(request.getDayOfWeek());
        activity.setStartTime(request.getStartTime());
        activity.setStartDate(request.getStartDate());
        activity.setEndDate(request.getEndDate());
        activity.setDuration(request.getDuration());
        activity.setType(request.getType());
        activity.setDescription(request.getDescription());
        activity.setRegular(request.isRegular());
        activity.setGrabbedGroupId(request.getGrabbedGroupId());
        activity.setVkUrl(request.getVkUrl());
        activity.setFbUrl(request.getFbUrl());

        if (request.getRoomId() != null) {
            Room room = roomDao.getById(request.getRoomId());
            activity.setRoomId(request.getRoomId());
            activity.setAddressId(room.getAddressId());
        } else {
            Long addressId = Optional.ofNullable(request.getAddress())
                    .map(addressService::getOrCreateAddress)
                    .map(Address::getId)
                    .orElse(null);
            activity.setAddressId(addressId);
        }

        activity.setEventId(request.getEventId());

        List<Long> teacherIds = request.getTeachers();

        if (teacherIds.size() > 0) {
            activity.setFirstTeacherId(teacherIds.get(0));
        }

        if (teacherIds.size() > 1) {
            activity.setSecondTeacherId(teacherIds.get(1));
        }

        User user = holder.get();
        activity.setUserId(user == null ? null : user.getId());

        activityDao.save(activity);
    }

    @Override
    public ActivityDto saveMilonga(ActivityDto request) {
        Activity milonga;
        if (request.getId() == null) {
            milonga = new Activity();
        } else {
            milonga = activityDao.getById(request.getId());
        }

        validateMilonga(request, milonga);


        // TODO возможно стоит объединить с основным методом save
        milonga.setType(ActivityType.MILONGA);
        milonga.setStartDate(request.getStartDate());
        milonga.setStartTime(request.getStartTime());
        milonga.setDayOfWeek(Utils.convertToMondayWeekEnum(request.getStartDate()));
        milonga.setDjId(request.getDjId());
        milonga.setDuration(Utils.calcDurationByDateInterval(request.getStartTime(), request.getEndTime()));
        milonga.setAddressId(addressService.getOrCreateAddress(request.getAddress()).getId());
        milonga.setCost(request.getCost());
        milonga.setCurrency(request.getCurrency());
        milonga.setUserId(holder.getUserId());
        milonga.setName(request.getName());
        milonga.setRegular(false);
        milonga.setVkUrl(request.getVkUrl());
        milonga.setFbUrl(request.getFbUrl());
        milonga.setPhoto(request.getPhoto());
        milonga.setEventId(request.getEventId());
        milonga.setDescription(request.getDescription());

        activityDao.save(milonga);

        return converter.convertActivityToMilongaDto(milonga);
    }

    @Override
    public Playbill getActivities(PlaybillFilter filter) {
        Date limit = DateUtils.addDays(filter.getFrom(), 30);
        Date to = filter.getTo() == null || filter.getTo().after(limit) ? limit : filter.getTo();

        City city;
        try {
            city = addressService.getOrCreateCity(filter.getCity());
        } catch (TPCityNotFoundException e) {
            city = null;
        }

        Playbill playbill = new Playbill();
        if (city == null) {
            return playbill;
        }
        List<Activity> activities = activityDao.findAllByRange(filter.getFrom(), to, city.getPid());

        Map<Date, ActivityDay> actionDates = new HashMap<>();
        List<ActivityDay> days = new ArrayList<>();

        List<Long> roomIds = activities.stream()
                .map(Activity::getRoomId)
                .collect(toList());

        Map<Long, Room> rooms = roomDao.findByIds(roomIds).stream().collect(toMap(Room::getId, identity()));
        Map<Long, School> schools = schoolDao.getAll().stream().collect(toMap(School::getId, identity()));
        Set<Long> teacherIds = new HashSet<>();

        for (Activity activity : activities) {
            Date actionDate = activity.getActionDate();

            ActivityDay day = actionDates.get(actionDate);
            if (day == null) {
                day = new ActivityDay();

                day.setDate(actionDate);

                days.add(day);
                actionDates.put(actionDate, day);
            }

            teacherIds.add(activity.getFirstTeacherId());
            teacherIds.add(activity.getSecondTeacherId());


            day.addEvent(createEvent(activity, schools, rooms));
        }

        playbill.setDays(days);
        playbill.setTeachers(fetchTeacherNames(teacherIds));
        return playbill;
    }

    @Override
    public ActivityDto getById(long id) {
        return entityToDto(activityDao.getById(id));
    }

    @Override
    public void remove(long id) {
        Activity activity = activityDao.getById(id);

        log.info("Acitivty: {}", activity);
        if (activity.getUserId() == holder.getUserId()) {
            activityDao.removeById(id);
        } else {
            log.warn("Not owner[{}] try to remove activity[{}]", holder.getUserId(), id);
        }
    }

    @Override
    public List<ActivityDto> getMilongasForUser() {
        List<Activity> milongas = activityDao.findMilongasForUser(holder.getUserId());
        return milongas.stream()
                .map(converter::convertActivityToMilongaDto)
                .collect(toList());
    }

    private void validateMilonga(ActivityDto request, Activity milonga) {
        // проверить обязательные поля
        if (StringUtils.isBlank(request.getName()) ||
                request.getAddress() == null ||
                StringUtils.isBlank(request.getAddress().getValue()) ||
                request.getStartDate() == null ||
                request.getStartTime() == null ||
                request.getEndTime() == null ||
                request.getDjId() == null) {
            throw new TPException(ErrorCode.NOT_ALL_REQUIRED_FIELDS);
        }


        Date today = DateUtils.addDays(new Date(), 1);

        Date startDateTime;
        if (request.getId() == null) {
            startDateTime = new Date(request.getStartDate().getTime() + request.getStartTime().getTime());

        } else {
            startDateTime = new Date(milonga.getStartDate().getTime() + milonga.getStartTime().getTime());
        }

        // проверить что событие случится в будущем
        if (today.after(startDateTime)) {
            throw new TPException(ErrorCode.MILONGA_IS_SHOOLD_BE_IN_FUTURE);
        }
    }

    private ActivityDto entityToDto(Activity activity) {
        ActivityDto dto = new ActivityDto();

        dto.setId(activity.getId());
        dto.setName(activity.getName());
        dto.setCost(activity.getCost());
        dto.setDayOfWeek(activity.getDayOfWeek());
        dto.setRegular(activity.isRegular());
        dto.setDuration(activity.getDuration());
        dto.setStartDate(activity.getStartDate());
        dto.setEndDate(activity.getEndDate());
        dto.setStartTime(activity.getStartTime());
        dto.setRoomId(activity.getRoomId());
        dto.setSchoolId(activity.getSchoolId());
        dto.setEventId(activity.getEventId());
        dto.setType(activity.getType());
        dto.setDescription(activity.getDescription());
        dto.setFbUrl(activity.getFbUrl());
        dto.setVkUrl(activity.getVkUrl());

        if (activity.getGrabbedGroupId() != null) {
            GrabbedGroup grabbedGroup = grabbedGroupDao.getById(activity.getGrabbedGroupId());
            if (activity.getAddressId() != null && !activity.getAddressId().equals(grabbedGroup.getAddressId())) {
                dto.setAddress(addressService.getById(activity.getAddressId()));
            }
        } else if (activity.getAddressId() != null) {
            dto.setAddress(addressService.getById(activity.getAddressId()));
        }

        List<Long> teacherIds = new ArrayList<>();
        if (activity.getFirstTeacherId() != null) {
            teacherIds.add(activity.getFirstTeacherId());
        }

        if (activity.getSecondTeacherId() != null) {
            teacherIds.add(activity.getSecondTeacherId());
        }
        dto.setTeachers(teacherIds);

        return dto;
    }

    private Map<Long, TeacherLink> fetchTeacherNames(Set<Long> teacherIds) {
        return teacherDao.findByIds(teacherIds)
                .stream()
                .map(t -> {
                    TeacherLink link = new TeacherLink();
                    link.setId(t.getId());
                    link.setFirstName(t.getFirstName());
                    link.setSecondName(t.getSecondName());
                    return link;
                })
                .collect(toMap(TeacherLink::getId, identity()));
    }

    private SimpleEvent createEvent(Activity activity, Map<Long, School> schools, Map<Long, Room> rooms) {
        SimpleEvent simpleEvent = new SimpleEvent();

        simpleEvent.setStartTime(activity.getStartTime());
        simpleEvent.setEndTime(Utils.createEndTime(activity.getStartTime(), activity.getDuration()));
        simpleEvent.setName(activity.getName());
        simpleEvent.setType(activity.getType());
        if (activity.getSchoolId() != null) {
            simpleEvent.setSchoolId(activity.getSchoolId());
            simpleEvent.setSchoolName(schools.get(activity.getSchoolId()).getName());
        }


        simpleEvent.setFirstTeacherId(activity.getFirstTeacherId());
        simpleEvent.setSecondTeacherId(activity.getSecondTeacherId());

        simpleEvent.setDj(userDao.getDjById(activity.getDjId()));
        simpleEvent.setVkUrl(activity.getVkUrl());
        simpleEvent.setFbUrl(activity.getFbUrl());
        simpleEvent.setPhoto(activity.getPhoto());

        Room room = rooms.get(activity.getRoomId());
        Long addressId;
        if (room != null) {
            addressId = room.getAddressId();
        } else {
            addressId = activity.getAddressId();
        }

        simpleEvent.setAddress(addressService.getById(addressId));

        simpleEvent.setCurrency(activity.getCurrency());
        simpleEvent.setCost(activity.getCost());

        return simpleEvent;
    }

    interface ActivityValidator {
        void validate(ActivityDto request);
    }

    class BaseValidator implements ActivityValidator {

        @Override
        public void validate(ActivityDto request) {
            if (request.isRegular()) {
                if (request.getStartDate() == null ||
                        request.getDayOfWeek() == null) {
                    throw new RuntimeException(); // TODO error
                }
            } else {

            }

            if (request.getStartTime() == null) {
                throw new RuntimeException(); // TODO error
            }

            if (request.getDuration() <= 0) {
                throw new RuntimeException(); // TODO error
            }

            // TODO check address
        }
    }
}
