package org.satuban.tangoportal.portal.dao;

import org.satuban.tangoportal.portal.dao.common.BaseDao;
import org.satuban.tangoportal.portal.model.dto.JsError;

/**
 * Created by Artur on 21.07.2017.
 */
public interface JsErrorDao extends BaseDao<JsError> {
}
