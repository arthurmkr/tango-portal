package org.satuban.tangoportal.portal.dao.impl;

import org.satuban.tangoportal.portal.dao.CityDao;
import org.satuban.tangoportal.portal.model.dto.CityMarker;
import org.satuban.tangoportal.portal.model.dto.CitySimpleMarker;
import org.satuban.tangoportal.portal.model.entity.City;
import org.satuban.tangoportal.portal.model.enums.UserStatus;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

/**
 * Created by Artur on 31.05.2017.
 */
@Repository
public class CityDaoImpl extends AbstractDao<City> implements CityDao {
    private final BeanPropertyRowMapper<CityMarker> cityMarkerMapper = new BeanPropertyRowMapper<>(CityMarker.class);
    private final BeanPropertyRowMapper<CitySimpleMarker> citySimpleMarkerMapper = new BeanPropertyRowMapper<>(CitySimpleMarker.class);

    CityDaoImpl(NamedParameterJdbcTemplate template) {
        super(template, City.class);
    }

    @Override
    public City findByName(String cityName) {
        List<City> list = template.query("SELECT * FROM city WHERE name = :name",
                Collections.singletonMap("name", cityName), rowMapper);
        return list.isEmpty() ? null : list.get(0);
    }

    @Override
    public List<CityMarker> getCityMarkers() {

        return template.query("SELECT * " +
                " FROM ( " +
                "       SELECT DISTINCT " +
                "         c0.pid, " +
                "         c0.name, " +
                "         c0.lng, " +
                "         c0.lat, " +
                "         coalesce(dj.count, 0)     dj_count, " +
                "         coalesce(event.count, 0)  event_count, " +
                "         coalesce(school.count, 0) school_count, " +
                "         coalesce(acts.count, 0)   activity_count " +
                "       FROM city c0 " +
                "         LEFT JOIN " +
                "         (SELECT " +
                "            c.pid, " +
                "            count(u.id) count " +
                "          FROM user_profile u " +
                "            JOIN city c ON c.id = u.city_id " +
                "          WHERE u.dj " +
                "          GROUP BY c.pid) dj ON dj.pid = c0.pid " +
                "         LEFT JOIN " +
                "         (SELECT " +
                "            c.pid, " +
                "            count(e.id) count " +
                "          FROM event e " +
                "            JOIN city c ON c.id = e.city_id " +
                "          WHERE e.start_date > now() " +
                "          GROUP BY c.pid) event ON event.pid = c0.pid " +
                "         LEFT JOIN " +
                "         (SELECT " +
                "            c.pid, " +
                "            count(s.id) count " +
                "          FROM school s " +
                "            JOIN address a ON a.id = s.address_id " +
                "            JOIN city c ON c.id = a.city_id " +
                "          GROUP BY c.pid) school ON school.pid = c0.pid " +
                "         LEFT JOIN " +
                "         (SELECT " +
                "            c.pid, " +
                "            count(act.id) count " +
                "          FROM activity act " +
                "            JOIN address a ON a.id = act.address_id " +
                "            JOIN city c ON c.id = a.city_id " +
                "          GROUP BY c.pid) acts ON acts.pid = c0.pid) stats " +
                " WHERE (dj_count + event_count + school_count + activity_count) > 0", cityMarkerMapper);
    }

    @Override
    public List<CitySimpleMarker> getCityEventMarkers() {
        return template.query("SELECT c.pid, c.lat, c.lng, max(c.name) \"name\", count(e.id) FROM city c " +
                        " JOIN event e ON e.city_id = c.id " +
                        " WHERE e.end_date > now() " +
                        " GROUP BY c.pid, c.lat, c.lng",
                Collections.emptyMap(), citySimpleMarkerMapper);
    }

    @Override
    public List<CitySimpleMarker> getCityDjMarkers() {
        return template.query("SELECT c.pid, c.lat, c.lng, max(c.name) \"name\", count(u.id) FROM city c " +
                        " JOIN user_profile u ON u.city_id = c.id " +
                        " WHERE u.dj AND u.status = '" + UserStatus.ACTIVE + "' " +
                        " GROUP BY c.pid, c.lat, c.lng",
                Collections.emptyMap(), citySimpleMarkerMapper);
    }

    @Override
    public List<CitySimpleMarker> getCitySchoolMarkers() {
        return template.query("SELECT c.pid, c.lat, c.lng, max(c.name) \"name\", count(s.id) FROM school s " +
                        " JOIN address a ON a.id = s.address_id " +
                        " JOIN city c ON c.id = a.city_id " +
                        " GROUP BY c.pid, c.lat, c.lng",
                Collections.emptyMap(), citySimpleMarkerMapper);
    }
}
