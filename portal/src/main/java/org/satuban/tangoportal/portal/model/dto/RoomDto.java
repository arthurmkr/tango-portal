package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Artur on 08.06.2017.
 */
@Data
public class RoomDto implements Serializable {
    private static final long serialVersionUID = -5243290935709068281L;

    private Long id;
    private String name;
    private AddressDto address;
}
