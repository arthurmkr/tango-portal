package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;
import org.satuban.tangoportal.portal.model.dto.AddressDto;

import java.io.Serializable;

/**
 * Created by Artur on 21.05.2017.
 */
@Data
public class SchoolRequest implements Serializable {
    private static final long serialVersionUID = 4483731056425656855L;

    private Long id;
    private String name;
    private String workPhone;
    private String mobilePhone;
    private String siteUrl;
    private String fbUrl;
    private String vkUrl;

    private AddressDto address;
}
