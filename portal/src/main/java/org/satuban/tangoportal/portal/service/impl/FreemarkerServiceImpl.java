package org.satuban.tangoportal.portal.service.impl;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.error.ErrorCode;
import org.satuban.tangoportal.portal.error.TPException;
import org.satuban.tangoportal.portal.model.enums.MailTemplates;
import org.satuban.tangoportal.portal.service.FreemarkerService;
import org.satuban.tangoportal.portal.util.Languages;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassRelativeResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.ui.freemarker.SpringTemplateLoader;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Artur on 19.07.2017.
 */
@Slf4j
@Service
public class FreemarkerServiceImpl implements FreemarkerService {
    private final Map<String, TemplateStorage> templates = new HashMap<>();
    private final Languages supportedLanguages;

    private final String templateLoaderPath;
    private Configuration cfg;

    public FreemarkerServiceImpl(@Value("${spring.freemarker.template-loader-path}") String templateLoaderPath,
                                 Languages supportedLanguages) {
        this.templateLoaderPath = templateLoaderPath + "mails/";
        this.supportedLanguages = supportedLanguages;
    }

    @PostConstruct
    public void init() {
        templates.put(supportedLanguages.getDefaultLanguage(), new TemplateStorage());

        for (MailTemplates template : MailTemplates.values()) {
            Template temp = getTemplate(template, supportedLanguages.getDefaultLanguage());

            if (temp == null) {
                throw new TPException(ErrorCode.NOT_FOUND_TEMPLATE_FOR_DEFAULT_LANGUAGE,
                        "Template[" + template + " for language [" + supportedLanguages.getDefaultLanguage() + "]");
            }
        }

        for (String language : supportedLanguages.getLanguages()) {
            if (!language.equals(supportedLanguages.getDefaultLanguage())) {

                templates.put(language, new TemplateStorage());

                for (MailTemplates template : MailTemplates.values()) {
                    getTemplate(template, language);
                }
            }
        }
    }

    @Override
    public String generate(MailTemplates template, String language, Map<String, Object> params) {
        log.debug("Generate content by template[{}] with params: {}", template, params);

        long time = System.currentTimeMillis();
        try {
            Template freemarkerTemplate = getTemplate(template, language);
            if (freemarkerTemplate != null) {
                String result = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerTemplate, params);

                log.info("Generate for: {} mls", (System.currentTimeMillis() - time));
                return result;
            } else {
                log.warn("Freemarker template [{}] is not found", template);
            }
        } catch (IOException | TemplateException e) {
            log.error("Freemarker error:", e);
        }
        return null;
    }

    private Template getTemplate(MailTemplates template, String language) {
        TemplateStorage storage = templates.get(language);
        if (storage == null) {
            storage = templates.get(supportedLanguages.getDefaultLanguage());
        }

        return storage.templates.computeIfAbsent(template, templateName -> {
            Configuration configuration = getConfiguration();
            try {
                return configuration.getTemplate(language + "/" + template.getTemplateName() + ".ftl", "UTF-8");
            } catch (IOException e) {
                log.error("Freemarker error:", e);

                TemplateStorage defStorage = templates.get(supportedLanguages.getDefaultLanguage());
                return defStorage.templates.get(templateName);
            }
        });
    }

    private Configuration getConfiguration() {
        if (cfg == null) {
            cfg = new Configuration(Configuration.VERSION_2_3_23);
            cfg.setTemplateLoader(new SpringTemplateLoader(new ClassRelativeResourceLoader(FreemarkerServiceImpl.class), templateLoaderPath));
            cfg.setEncoding(Locale.getDefault(), "UTF-8");
        }

        return cfg;
    }

    private final class TemplateStorage {
        final Map<MailTemplates, Template> templates = new HashMap<>();
    }
}
