package org.satuban.tangoportal.portal.dao;

import org.satuban.tangoportal.portal.dao.common.BaseDao;
import org.satuban.tangoportal.portal.model.entity.GrabbedActivity;

import java.util.List;

/**
 * Created by Artur on 25.09.2017.
 */
public interface GrabbedActivityDao extends BaseDao<GrabbedActivity> {
    List<String> findAllFbIds(List<String> fbIds);
}
