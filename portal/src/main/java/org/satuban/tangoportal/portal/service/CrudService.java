package org.satuban.tangoportal.portal.service;

import java.util.List;

/**
 * Created by Artur on 11.06.2017.
 */
public interface CrudService<T> {
    T save(T dto);

    void remove(long id);

    List<T> getAll();
}
