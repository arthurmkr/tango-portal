package org.satuban.tangoportal.portal.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.model.dto.GeoCodeResponse;
import org.satuban.tangoportal.portal.service.GoogleApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Artur on 31.05.2017.
 */
@Slf4j
@Service
public class GoogleApiImpl implements GoogleApi {
    private final RestTemplate restTemplate;
    private final String GOOGLE_API_GET_CITY_URL;

    public GoogleApiImpl(RestTemplate restTemplate, @Value("${google.api_key}") String apiKey) {
        this.restTemplate = restTemplate;
        GOOGLE_API_GET_CITY_URL = String.format("https://maps.googleapis.com/maps/api/geocode/json?components=locality&key=%1$s&address=", apiKey);
    }

    @Override
    public GeoCodeResponse.Result getPlaceForCity(String cityName) {
        GeoCodeResponse response = restTemplate.getForObject(GOOGLE_API_GET_CITY_URL + cityName, GeoCodeResponse.class);
        log.info("resp: {}", response);
        if(response.getResults().isEmpty()) {
            return null;
        }
        return response.getResults().get(0);
    }
}
