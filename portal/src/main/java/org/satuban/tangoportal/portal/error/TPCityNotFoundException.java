package org.satuban.tangoportal.portal.error;

/**
 * Created by Artur on 11.08.2017.
 */
public class TPCityNotFoundException extends TPException {
    private static final long serialVersionUID = -3905627654490007299L;

    public TPCityNotFoundException() {
        super(ErrorCode.CITY_IS_NOT_FOUND);
    }
}
