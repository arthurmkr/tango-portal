package org.satuban.tangoportal.portal.model.entity;

import lombok.Data;
import org.satuban.tangoportal.portal.dao.common.Column;
import org.satuban.tangoportal.portal.dao.common.Table;

/**
 * Created by Artur on 07.06.2017.
 */
@Data
@Table
public class Room implements BaseEntity {
    private static final long serialVersionUID = -8660316937827465302L;

    private Long id;
    @Column
    private String name;
    @Column
    private long addressId;
    @Column
    private long schoolId;
    @Column
    private long userId;
}
