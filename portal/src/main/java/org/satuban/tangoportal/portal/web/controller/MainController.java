package org.satuban.tangoportal.portal.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.error.TPObjectNotFoundException;
import org.satuban.tangoportal.portal.model.dto.RegistrationConfirmResponse;
import org.satuban.tangoportal.portal.model.dto.SchoolDto;
import org.satuban.tangoportal.portal.model.dto.WeekTimetable;
import org.satuban.tangoportal.portal.model.dto.teacher.TeacherDto;
import org.satuban.tangoportal.portal.model.entity.School;
import org.satuban.tangoportal.portal.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Artur on 16.05.2017.
 */
@Controller
@Slf4j
public class MainController {
    private static final String ANONYMOUS_NAME = "anonymousUser";

    private final CabinetService facadeService;
    private final SchoolService schoolService;
    private final TeacherService teacherService;
    private final TimetableService timetableService;
    private final InviteService inviteService;
    private final boolean debug;

    @Autowired
    public MainController(CabinetService facadeService,
                          SchoolService schoolService,
                          TeacherService teacherService,
                          TimetableService timetableService,
                          InviteService inviteService,
                          @Value("${app.debug:true}") boolean debug) {
        this.facadeService = facadeService;
        this.schoolService = schoolService;
        this.teacherService = teacherService;
        this.timetableService = timetableService;
        this.inviteService = inviteService;
        this.debug = debug;
    }

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/schools")
    public String schoolList(Model model) {
        List<School> all = facadeService.getAllSchools();

        model.addAttribute("schools", all);

        return "school_list";
    }

    @GetMapping("/school/{id}")
    public String school(@PathVariable long id, Model model) {
        SchoolDto school = schoolService.getById(id);
        List<TeacherDto> teachers = teacherService.getAllBySchool(school.getId());

        WeekTimetable timetable = timetableService.getForSchool(id, new Date());

        Map<String, String> teacherNames = new HashMap<>();
        for (Map.Entry<Long, String> entry : timetable.getTeachers().entrySet()) {
            teacherNames.put(String.valueOf(entry.getKey()), entry.getValue());
        }

        model.addAttribute("school", school);
        model.addAttribute("teachers", teachers);
        model.addAttribute("timetable", timetable);
        model.addAttribute("teacherNames", teacherNames);

        return "school";
    }

    @GetMapping("/about")
    public String about(HttpServletRequest request) {
        return "about";
    }

    @GetMapping("/cabinet")
    public String cabinet() {
        return "cabinet";
    }

    @GetMapping("/map")
    public String tangoMap() {
        return "map";
    }

    @GetMapping("/djs")
    public String djList() {
        return "dj_list";
    }

    @GetMapping("/events")
    public String eventList() {
        return "event_list";
    }

    @GetMapping("/photographers")
    public String photographerList() {
        return "photographer_list";
    }

    @GetMapping("/login")
    public String login(@RequestParam(defaultValue = "false") boolean error, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication == null || !authentication.isAuthenticated()
                || ANONYMOUS_NAME.equals(authentication.getName())) {
            model.addAttribute("error", error);
            return "login";
        } else {
            return "redirect:/";
        }
    }

    @GetMapping("/register/{token}")
    public String register(@PathVariable String token, Model model) {
        if (inviteService.isValidToken(token)) {
            model.addAttribute("inviteToken", token);
            return "register";
        }

        throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/confirm/{token}")
    public String registerConfirm(@PathVariable String token, Model model) {
        RegistrationConfirmResponse response = inviteService.confirm(token);

        model.addAttribute("response", response);
        return "registerConfirm";
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = TPObjectNotFoundException.class)
    public String objectNotFound(HttpServletRequest req, TPObjectNotFoundException e, Model model) {
        return "error/404";
    }
}
