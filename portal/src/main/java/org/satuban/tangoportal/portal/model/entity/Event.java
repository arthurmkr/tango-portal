package org.satuban.tangoportal.portal.model.entity;

import lombok.Data;
import org.satuban.tangoportal.portal.dao.common.Column;
import org.satuban.tangoportal.portal.dao.common.Table;

import java.util.Date;

/**
 * Created by Artur on 11.08.2017.
 */
@Data
@Table
public class Event implements BaseEntity {
    private static final long serialVersionUID = 2918699585290768078L;

    public Long id;
    @Column
    private String name;
    @Column
    private String photo;
    @Column
    private Date startDate;
    @Column
    private Date endDate;
    @Column
    private String siteUrl;
    @Column
    private String vkUrl;
    @Column
    private String fbUrl;
    @Column
    private Long cityId;
    @Column
    private Long userId;
}
