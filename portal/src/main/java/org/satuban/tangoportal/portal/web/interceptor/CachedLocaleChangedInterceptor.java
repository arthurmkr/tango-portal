package org.satuban.tangoportal.portal.web.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.LocaleContextResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * Created by Artur on 29.07.2017.
 */
@Slf4j
public class CachedLocaleChangedInterceptor extends LocaleChangeInterceptor {
    private final Locale defaultLocale;
    public CachedLocaleChangedInterceptor() {
        defaultLocale = Locale.forLanguageTag("ru");
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws ServletException {
        LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
        if (localeResolver == null) {
            throw new IllegalStateException(
                    "No LocaleResolver found: not in a DispatcherServlet request?");
        }

        Locale curLocale = localeResolver.resolveLocale(request);

        String newLocale = request.getParameter(getParamName());
        if (newLocale != null) {
            if (StringUtils.equalsIgnoreCase(curLocale.getLanguage(), newLocale)) {
                return true;
            } else {
                log.info("Change locale from [{}] to [{}]", curLocale.getLanguage(), newLocale);
                return super.preHandle(request, response, handler);
            }
        }

        return true;
    }
}
