package org.satuban.tangoportal.portal.model.enums;

/**
 * Created by Artur on 18.07.2017.
 */
public enum UserStatus {
    ACTIVE,
    INVITED,
    INACTIVE,
    FAKE
}
