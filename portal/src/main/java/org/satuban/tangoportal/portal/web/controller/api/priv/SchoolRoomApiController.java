package org.satuban.tangoportal.portal.web.controller.api.priv;

import org.satuban.tangoportal.portal.model.dto.RoomDto;
import org.satuban.tangoportal.portal.service.SchoolRoomService;
import org.satuban.tangoportal.portal.web.WebConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Artur on 11.06.2017.
 */
@RestController
@RequestMapping(WebConstants.PRIVATE_API + "/schoolRoom")
public class SchoolRoomApiController extends CrudApiController<RoomDto> {
    private final SchoolRoomService schoolSchoolRoomService;

    @Autowired
    public SchoolRoomApiController(SchoolRoomService schoolSchoolRoomService) {
        this.schoolSchoolRoomService = schoolSchoolRoomService;
    }

    @Override
    protected SchoolRoomService getService() {
        return schoolSchoolRoomService;
    }
}
