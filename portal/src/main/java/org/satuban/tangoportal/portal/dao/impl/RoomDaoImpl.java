package org.satuban.tangoportal.portal.dao.impl;

import org.satuban.tangoportal.portal.dao.RoomDao;
import org.satuban.tangoportal.portal.model.entity.Room;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

/**
 * Created by Artur on 11.06.2017.
 */
@Repository
public class RoomDaoImpl extends AbstractDao<Room> implements RoomDao {
    RoomDaoImpl(NamedParameterJdbcTemplate template) {
        super(template, Room.class);
    }

    @Override
    public List<Room> findAllSchoolForUser(long userId) {
        return template.query("SELECT * FROM room r WHERE r.user_id = :userId ORDER BY r.id"
                , Collections.singletonMap("userId", userId), rowMapper);
    }

    @Override
    public List<Room> findAllForSchool(long schoolId) {
        return template.query("SELECT * FROM room r WHERE r.school_id = :schoolId ORDER BY r.id"
                , Collections.singletonMap("schoolId", schoolId), rowMapper);
    }
}
