package org.satuban.tangoportal.portal.model.dto.external.facebook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by Danilov on 06.10.2017.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FacebookPicture {
    @JsonProperty("is_silhouette")
    private boolean isSilhouette;
    private String url;
}
