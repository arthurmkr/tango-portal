package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Artur on 23.05.2017.
 */
@Data
public class CabinetInfo implements Serializable {
    private static final long serialVersionUID = -3559598243903388331L;

    private SchoolDto school;
}
