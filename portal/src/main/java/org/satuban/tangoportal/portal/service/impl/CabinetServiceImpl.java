package org.satuban.tangoportal.portal.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;
import org.satuban.tangoportal.portal.dao.*;
import org.satuban.tangoportal.portal.model.dto.*;
import org.satuban.tangoportal.portal.model.entity.Activity;
import org.satuban.tangoportal.portal.model.entity.City;
import org.satuban.tangoportal.portal.model.entity.School;
import org.satuban.tangoportal.portal.model.dto.ActivityDto;
import org.satuban.tangoportal.portal.service.CabinetService;
import org.satuban.tangoportal.portal.service.GoogleApi;
import org.satuban.tangoportal.portal.service.UserHolder;
import org.satuban.tangoportal.portal.util.ConverterUtil;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

/**
 * Created by Artur on 18.05.2017.
 */
@Slf4j
@Service
public class CabinetServiceImpl extends AbstractService implements CabinetService {
    private static final Date FAR_DATE;

    static {
        Calendar instance = Calendar.getInstance();
        instance.clear();
        instance.set(2100, 0, 1);
        FAR_DATE = instance.getTime();
    }

    private final SchoolDao schoolDao;

    private final UserDao userDao;
    private final ActivityDao activityDao;
    private final AddressDao addressDao;
    private final CityDao cityDao;
    private final GoogleApi googleApi;

    public CabinetServiceImpl(SchoolDao schoolDao,
                              ConverterUtil converter,
                              UserDao userDao,
                              ActivityDao activityDao,
                              AddressDao addressDao,
                              CityDao cityDao, GoogleApi googleApi,
                              UserHolder userHolder) {
        super(userHolder);
        this.schoolDao = schoolDao;
        this.userDao = userDao;
        this.activityDao = activityDao;
        this.addressDao = addressDao;
        this.cityDao = cityDao;
        this.googleApi = googleApi;
    }

    @Override
    public List<School> getAllSchools() {
        return schoolDao.getAll();
    }

    @Override
    public void saveLesson(ActivityDto request) {

    }

    @Override
    public CabinetInfo getCabinetInfo() {
        School school = schoolDao.findByUserId(getUserId());

        CabinetInfo cabinetInfo = new CabinetInfo();

//        cabinetInfo.setSchool(converter.toSchoolDto(school));

        return cabinetInfo;
    }



    @Override
    public School getSchool(long schoolId) {
        return schoolDao.getById(schoolId);
    }

    @Override
    public DailyEventMap getDailyEventMap(String cityName) {
        City city = cityDao.findByName(cityName);

        DailyEventMap res = new DailyEventMap();
        if (city != null) {
            res.setCenterLat(city.getLat());
            res.setCenterLng(city.getLng());

            Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);

            List<Activity> activities = activityDao.findAllByRange(today, today, city.getPid());

            res.setEvents(activities.stream()
                    .map(a -> {
                        EventMarker marker = new EventMarker();
                        marker.setType(a.getType());
                        marker.setLat(a.getLat());
                        marker.setLng(a.getLng());
                        return marker;
                    })
                    .collect(Collectors.toList()));
        }
        return res;
    }
}
