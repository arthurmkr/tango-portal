package org.satuban.tangoportal.portal.web.controller.api.priv;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.model.dto.ChangePasswordRequest;
import org.satuban.tangoportal.portal.model.dto.Profile;
import org.satuban.tangoportal.portal.service.UserService;
import org.satuban.tangoportal.portal.web.WebConstants;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * Created by Artur on 04.07.2017.
 */
@Slf4j
@RestController
@RequestMapping(WebConstants.PRIVATE_API + "/profile")
public class ProfileApiController {
    private final UserService userService;

    public ProfileApiController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public Profile get() {
        return userService.getProfile();
    }

    @PostMapping
    public Profile save(@RequestBody Profile dto) throws IOException {
        log.info("dto: {}", dto);
        return userService.save(dto);
    }

    @PostMapping("/changePassword")
    public HttpStatus changePassword(@RequestBody ChangePasswordRequest request) {
        userService.changePassword(request);
        return HttpStatus.OK;
    }

}
