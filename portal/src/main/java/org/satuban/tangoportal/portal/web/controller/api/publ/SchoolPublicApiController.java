package org.satuban.tangoportal.portal.web.controller.api.publ;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.model.dto.SchoolDto;
import org.satuban.tangoportal.portal.model.filters.SchoolListFilter;
import org.satuban.tangoportal.portal.service.SchoolService;
import org.satuban.tangoportal.portal.web.WebConstants;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Artur on 24.06.2017.
 */
@Slf4j
@RestController
@RequestMapping(WebConstants.PUBLIC_API + "/school")
public class SchoolPublicApiController {
    private final SchoolService schoolService;

    public SchoolPublicApiController(SchoolService schoolService) {
        this.schoolService = schoolService;
    }

    @GetMapping
    public List<SchoolDto> get(SchoolListFilter filter) {
        return schoolService.getByFilter(filter);
    }
}
