package org.satuban.tangoportal.portal.service.impl;

import org.satuban.tangoportal.portal.dao.common.BaseDao;
import org.satuban.tangoportal.portal.model.entity.BaseEntity;
import org.satuban.tangoportal.portal.service.CrudService;
import org.satuban.tangoportal.portal.service.UserHolder;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Artur on 11.06.2017.
 */
public abstract class AbstractCrudServiceImpl<D, E extends BaseEntity> extends AbstractService implements CrudService<D> {
    AbstractCrudServiceImpl(UserHolder userHolder) {
        super(userHolder);
    }

    protected abstract BaseDao<E> getDao();

    @Override
    @Transactional
    public D save(D dto) {
        return entityToDto(getDao().save(dtoToEntity(dto)));
    }

    @Override
    @Transactional
    public void remove(long id) {
        getDao().removeById(id);
    }

    @Override
    @Transactional
    public List<D> getAll() {
        return getDao().getAll()
                .stream()
                .map(this::entityToDto)
                .collect(Collectors.toList());
    }

    protected abstract E dtoToEntity(D dto);

    protected abstract D entityToDto(E entity);
}
