package org.satuban.tangoportal.portal.dao.impl;

import org.satuban.tangoportal.portal.dao.RegisterRequestDao;
import org.satuban.tangoportal.portal.model.entity.RegisterRequest;
import org.satuban.tangoportal.portal.model.enums.RegisterRequestStatus;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Created by Artur on 19.07.2017.
 */
@Repository
public class RegisterRequestDaoImpl extends AbstractDao<RegisterRequest> implements RegisterRequestDao {
    RegisterRequestDaoImpl(NamedParameterJdbcTemplate template) {
        super(template, RegisterRequest.class);
    }

    @Override
    public List<RegisterRequest> getNewForSending() {
        return template.query("SELECT * FROM register_request WHERE sent IS NULL AND  status = '" +
                        RegisterRequestStatus.NEW + "\'",
                Collections.emptyMap(), rowMapper);
    }

    @Override
    public List<RegisterRequest> getConfirmedForSending() {
        return template.query("SELECT * FROM register_request WHERE sent IS NULL AND status = \'" +
                        RegisterRequestStatus.CONFIRMED + "\'",
                Collections.emptyMap(), rowMapper);
    }

    @Override
    public void markAsSent(List<RegisterRequest> requests) {
        List<Long> requestIds = requests.stream()
                .map(RegisterRequest::getId)
                .collect(toList());


        template.update("UPDATE register_request SET sent = now() WHERE id IN (:ids)",
                Collections.singletonMap("ids", requestIds));
    }

    @Override
    public RegisterRequest getByToken(String token) {
        return first(template.query("SELECT * FROM register_request WHERE token = :token",
                Collections.singletonMap("token", token), rowMapper));
    }

    @Override
    public void rejectRequest(long id) {
        template.update("UPDATE register_request SET status = \'" + RegisterRequestStatus.REJECTED + "\'",
                Collections.singletonMap("id", id));
    }

    @Override
    public RegisterRequest getByUser(long userId) {
        return first(template.query("SELECT * FROM register_request WHERE user_id = :userId",
                Collections.singletonMap("userId", userId), rowMapper));
    }

    @Override
    public List<RegisterRequest> getApprovedForSending() {
        return template.query("SELECT * FROM register_request WHERE sent IS NULL AND status = \'" +
                        RegisterRequestStatus.APPROVED + "\'",
                Collections.emptyMap(), rowMapper);
    }
}
