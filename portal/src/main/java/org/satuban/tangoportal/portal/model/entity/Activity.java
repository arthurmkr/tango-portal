package org.satuban.tangoportal.portal.model.entity;

import lombok.Data;
import org.satuban.tangoportal.portal.dao.common.Column;
import org.satuban.tangoportal.portal.dao.common.Table;
import org.satuban.tangoportal.portal.model.enums.ActivityType;
import org.satuban.tangoportal.portal.model.enums.Currency;
import org.satuban.tangoportal.portal.model.enums.LessonLevel;

import java.sql.Timestamp;
import java.time.DayOfWeek;
import java.util.Date;
import java.util.List;

/**
 * Created by Artur on 26.05.2017.
 */
@Data
@Table
public class Activity implements BaseEntity {
    private static final long serialVersionUID = -93869365065550668L;

    private Long id;
    @Column
    private String name;
    @Column
    private Long userId;
    @Column
    private Long eventId;
    @Column
    private Long schoolId;
    @Column
    private int cost;
    @Column
    private Long addressId;
    @Column
    private ActivityType type;
    @Column
    private boolean regular;
    @Column
    private Long djId;
    @Column
    private int lessonLevel;
    @Column(enumOrder=true)
    private DayOfWeek dayOfWeek;
    @Column
    private Date startDate;
    @Column
    private Date startTime;
    @Column
    private int duration;
    @Column
    private Date endDate;
    @Column
    private Long roomId;
    @Column
    private Long firstTeacherId;
    @Column
    private Long secondTeacherId;
    @Column
    private String vkUrl;
    @Column
    private String fbUrl;
    @Column
    private Currency currency;
    @Column
    private String photo;
    @Column
    private Long grabbedGroupId;
    @Column
    private String description;

    private Date actionDate;
    private double lat;
    private double lng;
    private String address;
    private String roomName;
}
