package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.enums.MailTemplates;

import java.util.Map;

/**
 * Created by Artur on 19.07.2017.
 */
public interface FreemarkerService {
    String generate(MailTemplates template, String language, Map<String, Object> params);
}
