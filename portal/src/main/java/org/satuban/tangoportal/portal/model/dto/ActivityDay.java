package org.satuban.tangoportal.portal.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.satuban.tangoportal.portal.model.json.DateSerializer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Artur on 16.05.2017.
 */
@Data
public class ActivityDay implements Serializable {
    private static final long serialVersionUID = -5882980806428814549L;

    @JsonSerialize(using = DateSerializer.class)
    private Date date;
    private List<SimpleEvent> events;

    public void addEvent(SimpleEvent simpleEvent) {
        if (events == null) {
            events = new ArrayList<>();
        }

        events.add(simpleEvent);
    }
}
