package org.satuban.tangoportal.portal.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.satuban.tangoportal.portal.model.entity.User;
import org.satuban.tangoportal.portal.model.enums.MailTemplates;
import org.satuban.tangoportal.portal.service.FreemarkerService;
import org.satuban.tangoportal.portal.service.MailService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Artur on 16.07.2017.
 */
@Slf4j
@Service
public class MailServiceImpl implements MailService {
    private final FreemarkerService freemarkerService;
    private final MessageSource messages;

    private final String smtpUser;
    private final String smtpPassword;
    private final int smtpPort;
    private final String smtpHost;

    public MailServiceImpl(FreemarkerService freemarkerService,
                           MessageSource messages,
                           @Value("${mail.username}") String smtpUser,
                           @Value("${mail.password}") String smtpPassword,
                           @Value("${mail.port}") int smtpPort,
                           @Value("${mail.host}") String smtpHost) {
        this.freemarkerService = freemarkerService;
        this.messages = messages;
        this.smtpUser = smtpUser;
        this.smtpPassword = smtpPassword;
        this.smtpPort = smtpPort;
        this.smtpHost = smtpHost;
    }

    @Override
    public void send(String to, String subject, String body) {
        long time = System.currentTimeMillis();
        try {
            HtmlEmail email = new HtmlEmail();
            email.setHostName(smtpHost);
            email.setSmtpPort(smtpPort);
            email.setAuthenticator(new DefaultAuthenticator(smtpUser, smtpPassword));
            email.setSSLOnConnect(true);
            email.setFrom(smtpUser);
            email.setCharset("UTF-8");

            email.setSubject(subject);
            email.setHtmlMsg(body);
            email.addTo(to);
            email.send();

            log.info("Send email: {} mls", (System.currentTimeMillis() - time));
        } catch (EmailException e) {
            log.error("Send mail error: ", e);
        }
    }

    @Override
    public void sendRegisterConfirmationToUser(String userEmail, String registerToken, String language) {
        Map<String, Object> params = new HashMap<>(1);
        params.put("registerToken", registerToken);

        sendMail(userEmail, params, language, MailTemplates.CONFIRM_REGISTER_REQUEST, "mail.confirmRegister.subject");
    }

    @Override
    public void sendRegisterConfirmationToReferrers(String referrerEmail, List<User> users, String language) {
        Map<String, Object> params = new HashMap<>(1);
        params.put("users", users);

        sendMail(referrerEmail, params, language, MailTemplates.USERS_WAIT_REGISTRATION_CONFIRM, "mail.newUser.subject");
    }

    @Override
    public void sendApproveRegistrationMails(String userEmail, String password, String language) {
        Map<String, Object> params = new HashMap<>(2);
        params.put("login", userEmail);
        params.put("password", password);

        sendMail(userEmail, params, language, MailTemplates.REGISTRATION_APPROVED, "mail.registerApproved.subject");
    }

    private void sendMail(String userEmail, Map<String, Object> params, String language, MailTemplates templateName, String subject) {
        Locale locale = Locale.forLanguageTag(language);

        String body = freemarkerService.generate(templateName, language, params);
        send(userEmail, "Tango World: " + messages.getMessage(subject, null, locale), body);
    }
}
