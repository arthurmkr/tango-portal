package org.satuban.tangoportal.portal.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * Created by Artur on 01.06.2017.
 */
@Data
public class GeoCodeResponse {
    private List<Result> results;

    @Data
    public static class Result {
        @JsonProperty("place_id")
        private String placeId;
        private Geometry geometry;

        @Data
        public static class Geometry {
            private Location location;

            @Data
            public static class Location {
                private float lat;
                private float lng;
            }
        }
    }
}
