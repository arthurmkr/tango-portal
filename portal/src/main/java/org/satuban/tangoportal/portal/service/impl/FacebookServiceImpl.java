package org.satuban.tangoportal.portal.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.dao.ExternalTokenDao;
import org.satuban.tangoportal.portal.model.dto.external.facebook.FacebookCover;
import org.satuban.tangoportal.portal.model.dto.external.facebook.FacebookEventDto;
import org.satuban.tangoportal.portal.model.dto.external.facebook.FacebookType;
import org.satuban.tangoportal.portal.model.dto.external.facebook.data.FacebookCoverData;
import org.satuban.tangoportal.portal.model.dto.external.facebook.data.FacebookEventDataPage;
import org.satuban.tangoportal.portal.model.entity.Event;
import org.satuban.tangoportal.portal.model.entity.ExternalToken;
import org.satuban.tangoportal.portal.model.enums.PhotoSize;
import org.satuban.tangoportal.portal.model.enums.TokenType;
import org.satuban.tangoportal.portal.service.FacebookService;
import org.satuban.tangoportal.portal.service.PhotoService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Danilov on 06.10.2017.
 */
@Service
@Slf4j
public class FacebookServiceImpl implements FacebookService {
    private final ExternalTokenDao externalTokenDao;
    private final PhotoService photoService;
    private final RestTemplate restTemplate;

    public FacebookServiceImpl(ExternalTokenDao externalTokenDao,
                               PhotoService photoService) {
        this.externalTokenDao = externalTokenDao;
        this.photoService = photoService;
        this.restTemplate = new RestTemplate();
    }

    @Override
    @Transactional(readOnly = true)
    public String getFacebookPictureUrlById(String id) {
        if(id == null) {
            return null;
        }

        ExternalToken token = externalTokenDao.findAnyAdminTokenByType(TokenType.FACEBOOK);

        if(token == null) {
            log.warn("Need facebook token!");
            return null;
        }

        String pictureUrl = fbIdToPictureUrl(id, token.getToken());

        log.info("Picture url: {}", pictureUrl);

        try {
            ResponseEntity<FacebookCoverData> facebookCoverDataResponse = restTemplate
                    .getForEntity(pictureUrl, FacebookCoverData.class);

            FacebookCover facebookCover = facebookCoverDataResponse.getBody().getCover();

            if(facebookCover != null) {
                log.debug("From ID: {} source: {}", id, facebookCover.getSource());

                return facebookCover.getSource();
            } else {
                return null;
            }
        } catch (RestClientException ex) {
            log.error("Can't pull picture from id: {}", id, ex);
            throw new RuntimeException("Can't pull picture from id:" + id, ex);
        }
    }

    @Override
    public List<Event> updateEventsPhoto(List<Event> events) {
        for (Event event : events) {
            String fbUrl = event.getFbUrl();

            log.debug("Event ID:{} with FB url: {}", event.getId(), fbUrl);

            FacebookType facebookType = getFacebookTypeFromUrl(fbUrl);
            if(facebookType == null) {
                log.debug("Event {} is skip", event.getId());
                continue;
            }

            String sourcePictureUrl = null;

            if(facebookType == FacebookType.EVENT) {
               sourcePictureUrl = getPictureUrlFromEventUrl(fbUrl);
            } else if(facebookType == FacebookType.GROUP) {
                sourcePictureUrl = getPictureUrlFromGroupUrl(fbUrl, event);
            }

            if(sourcePictureUrl != null) {
                String newPhotoUrl = photoService.savePhoto(PhotoSize.EVENT_PHOTO, sourcePictureUrl);
                log.debug("Save photo: {}", newPhotoUrl);
                event.setPhoto(newPhotoUrl);
            } else {
                log.warn("For event ID: {} can't find picture", event.getId());
            }
        }

        return events;
    }

    private String getPictureUrlFromGroupUrl(String groupUrl, Event event) {
        String idFromGroupUrl = getIdFromFbUrl(groupUrl);

        log.debug("FB ID group: {} from url: {}", idFromGroupUrl, groupUrl);

        return findEventFromFbGroup(idFromGroupUrl, event);
    }

    private String findEventFromFbGroup(String idGroup, Event event) {
        ExternalToken token = externalTokenDao.findAnyAdminTokenByType(TokenType.FACEBOOK);

        if(token == null) {
            log.warn("Need facebook token!");
            return null;
        }

        String eventsUrlFromGroup = fbIdToEventsFromGroupUrl(idGroup, token.getToken());

        log.debug("FB URL group: {}", eventsUrlFromGroup);

        try {
            ResponseEntity<FacebookEventDataPage> dataResponse = restTemplate.getForEntity(eventsUrlFromGroup, FacebookEventDataPage.class);
            FacebookEventDataPage facebookEventData = dataResponse.getBody();
            List<FacebookEventDto> fbEvents = facebookEventData.getData();

            log.debug("Find events from id: {} size: {}", idGroup, fbEvents.size());

            if(fbEvents.isEmpty()) {
                return null;
            }

            String pictureUrl = fbEvents.stream()
                    .filter(fbEvent -> event.getName().equals(fbEvent.getName()))
                    .findFirst()
                    .map(FacebookEventDto::getId)
                    .map(this::getFacebookPictureUrlById)
                    .orElse(null);

            if(pictureUrl == null) {
                pictureUrl = this.getFacebookPictureUrlById(fbEvents.get(0).getId());
            }

            return pictureUrl;
        } catch (RestClientException ex) {
            log.error("Can't pull picture from id: {}", idGroup, ex);
            throw new RuntimeException("Can't pull picture from id: " + idGroup, ex);
        }
    }

    private String getPictureUrlFromEventUrl(String eventUrl) {
        String idFromEventsUrl = getIdFromFbUrl(eventUrl);

        log.debug("FB ID event: {} from url: {}", idFromEventsUrl, eventUrl);

        return getFacebookPictureUrlById(idFromEventsUrl);
    }

    private FacebookType getFacebookTypeFromUrl(String url) {
        if(url.contains("/events")) {
            return FacebookType.EVENT;
        }

        if(url.contains("/groups")) {
            return FacebookType.GROUP;
        }

        return null;
    }

    private String getIdFromFbUrl(String url) {
        String regexp = "\\d[0-9]*";

        Pattern compile = Pattern.compile(regexp);

        Matcher matcher = compile.matcher(url);
        return matcher.find()
                ? matcher.group()
                : null;
    }

    private String fbIdToEventsFromGroupUrl(String id, String token) {
        return "https://graph.facebook.com/" + id + "/events?access_token=" + token;
    }

    private String fbIdToPictureUrl(String id, String token) {
        return "https://graph.facebook.com/" + id + "?fields=cover&access_token=" + token;
    }
}
