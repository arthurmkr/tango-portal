package org.satuban.tangoportal.portal.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by Artur on 04.08.2017.
 */
@Component
public class Languages {
    private String[] languages;
    private String defaultLanguage = "en";

    public Languages(@Value("${supportedLanguages}") String supportedLanguages) {
        this.languages = supportedLanguages.split(",");
    }

    public String[] getLanguages() {
        return languages;
    }

    public String getDefaultLanguage() {
        return defaultLanguage;
    }
}
