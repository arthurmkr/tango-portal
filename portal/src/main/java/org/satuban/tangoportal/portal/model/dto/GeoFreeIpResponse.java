package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;

/**
 * Created by Artur on 14.07.2017.
 */
@Data
public class GeoFreeIpResponse {
    private float latitude;
    private float longitude;
}

