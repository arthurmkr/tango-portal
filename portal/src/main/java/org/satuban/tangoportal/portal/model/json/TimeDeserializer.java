package org.satuban.tangoportal.portal.model.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Artur on 11.07.2017.
 */
public class TimeDeserializer extends StdDeserializer<Date> {
    private static final long serialVersionUID = 3096100514088740176L;

    private SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");

    public TimeDeserializer() {
        this(null);
    }

    public TimeDeserializer(Class t) {
        super(t);
    }

    @Override
    public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String date = p.getText();
        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }


}