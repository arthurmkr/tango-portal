package org.satuban.tangoportal.portal.service.impl;

import org.satuban.tangoportal.portal.service.CrudService;
import org.satuban.tangoportal.portal.service.UserHolder;

/**
 * Created by Artur on 08.06.2017.
 */
public abstract class AbstractService  {
    protected final UserHolder userHolder;

    public AbstractService(UserHolder userHolder) {
        this.userHolder = userHolder;
    }

    protected long getUserId() {
        return userHolder.get().getId();
    }


}
