package org.satuban.tangoportal.portal.web.controller.api.publ;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.model.dto.Playbill;
import org.satuban.tangoportal.portal.model.filters.PlaybillFilter;
import org.satuban.tangoportal.portal.service.ActivityService;
import org.satuban.tangoportal.portal.web.WebConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Artur on 19.06.2017.
 */
@Slf4j
@RestController
@RequestMapping(WebConstants.PUBLIC_API + "/activity")
public class ActivityPublicApiController {
    private final ActivityService activityService;

    @Autowired
    public ActivityPublicApiController(ActivityService activityService) {
        this.activityService = activityService;
    }

    @GetMapping
    public Playbill getPlaybill(@ModelAttribute PlaybillFilter filter, HttpServletRequest request) {
        log.info("Filter: {}", filter);
        return activityService.getActivities(filter);
    }
}
