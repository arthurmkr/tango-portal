package org.satuban.tangoportal.portal.service.impl;

import com.sun.net.httpserver.Headers;
import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.dao.JsErrorDao;
import org.satuban.tangoportal.portal.model.dto.JsError;
import org.satuban.tangoportal.portal.model.dto.JsErrorRequest;
import org.satuban.tangoportal.portal.service.UserHolder;
import org.satuban.tangoportal.portal.service.UtilService;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created by Artur on 21.07.2017.
 */
@Slf4j
@Service
@Transactional
public class UtilServiceImpl implements UtilService {
    private final JsErrorDao jsErrorDao;
    private final UserHolder userHolder;

    public UtilServiceImpl(JsErrorDao jsErrorDao, UserHolder userHolder) {
        this.jsErrorDao = jsErrorDao;
        this.userHolder = userHolder;
    }

    @Override
    public void saveJsError(JsErrorRequest error, HttpServletRequest request) {
        JsError entity = new JsError();

        entity.setColNumber(error.getColNumber());
        entity.setLineNumber(error.getLineNumber());
        entity.setMessage(error.getMessage());
        entity.setUrl(error.getUrl());

        entity.setUserId(userHolder.getUserId());
        entity.setCreated(new Date());
        entity.setIp(request.getRemoteAddr());
        entity.setReferrer(request.getHeader(HttpHeaders.REFERER));
        entity.setUserAgent(request.getHeader(HttpHeaders.USER_AGENT));

        jsErrorDao.save(entity);
    }
}
