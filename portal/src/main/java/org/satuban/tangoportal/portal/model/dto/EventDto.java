package org.satuban.tangoportal.portal.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.satuban.tangoportal.portal.model.json.DateDeserializer;
import org.satuban.tangoportal.portal.model.json.DateSerializer;

import java.util.Date;
import java.util.List;

/**
 * Created by Artur on 11.08.2017.
 */
@Data
public class EventDto {
    private Long id;
    private String name;
    private String siteUrl;
    private String fbUrl;
    private String vkUrl;
    private CityDto city;
    private String photo;
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
    private Date startDate;

    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
    private Date endDate;

    private List<ActivityDto> milongas;
}
