package org.satuban.tangoportal.portal.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.satuban.tangoportal.portal.model.json.TimeDeserializer;
import org.satuban.tangoportal.portal.model.json.TimeSerializer;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Artur on 13.06.2017.
 */
@Data
public class TimetableRow implements Serializable {
    private static final long serialVersionUID = -5836235064928144068L;

    @JsonSerialize(using = TimeSerializer.class)
    private Date startTime;

    @JsonSerialize(using = TimeSerializer.class)
    private Date endTime;
    private TimetableItem[] items;

}
