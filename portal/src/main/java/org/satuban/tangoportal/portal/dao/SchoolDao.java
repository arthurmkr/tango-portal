package org.satuban.tangoportal.portal.dao;

import org.satuban.tangoportal.portal.dao.common.BaseDao;
import org.satuban.tangoportal.portal.model.entity.School;
import org.satuban.tangoportal.portal.model.filters.SchoolListFilter;
import org.satuban.tangoportal.portal.model.dto.SchoolRequest;

import java.util.List;

/**
 * Created by Artur on 16.05.2017.
 */

public interface SchoolDao extends BaseDao<School> {
    School findByUserId(long userId);

    SchoolRequest getFormByUserId(long userId);

    List<School> findByFilter(String pid);

    //TODO Метод лишний, в BaseDao уже есть метод getAll()
    List<School> findAll();
}
