package org.satuban.tangoportal.portal.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by Artur on 03.06.2017.
 */
@Data
@AllArgsConstructor
public class Result implements Serializable {
    private static final long serialVersionUID = -1600776504655896030L;

    private final ResultType type;
    private final Object data;

    public static Result success(Object data) {
        return new Result(ResultType.SUCCESS, data);
    }

    enum ResultType {
        SUCCESS;

        private final String code;

        ResultType() {
            code = this.name().toLowerCase();
        }

        public String getCode() {
            return code;
        }
    }
}
