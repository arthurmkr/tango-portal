package org.satuban.tangoportal.portal.model.entity;

import java.io.Serializable;

/**
 * Created by Artur on 18.05.2017.
 */
public interface BaseEntity extends Serializable {
    Long getId();

    void setId(Long id);
}
