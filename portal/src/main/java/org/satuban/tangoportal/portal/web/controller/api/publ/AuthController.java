package org.satuban.tangoportal.portal.web.controller.api.publ;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.error.ErrorCode;
import org.satuban.tangoportal.portal.model.entity.User;
import org.satuban.tangoportal.portal.web.WebConstants;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * Created by Artur on 24.06.2017.
 */
@Slf4j
@RestController
@RequestMapping(WebConstants.PUBLIC_API + "/auth")
public class AuthController {
    private final AuthenticationManager authenticationManager;

    public AuthController(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @PostMapping("/login")
    public LoginStatus login(@RequestBody AuthRequest request, HttpSession session) {

        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(request.getUsername(),
                request.getPassword()
        );
        User details = new User();
        details.setUsername(request.getUsername());
        token.setDetails(details);

        try {
            Authentication auth = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(auth);
            session.setAttribute("user", (User) auth.getPrincipal());
            return new LoginStatus(auth.isAuthenticated(), auth.getName(), null);
        }catch (DisabledException e) {
            return new LoginStatus(false, null, ErrorCode.USER_IS_INACTIVE);
        } catch (BadCredentialsException e) {
            return new LoginStatus(false, null, ErrorCode.BAD_CREDENTIALS);
        } catch (Exception e) {
            log.error("Auth error:", e);
            return new LoginStatus(false, null, ErrorCode.COMMON_ERROR);
        }
    }

    @Data
    public static class AuthRequest {
        private String username;
        private String password;
    }

    @Data
    @AllArgsConstructor
    public class LoginStatus {
        private boolean loggedIn;
        private String username;
        private ErrorCode errorCode;
    }
}
