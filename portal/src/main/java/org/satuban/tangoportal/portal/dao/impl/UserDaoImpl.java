package org.satuban.tangoportal.portal.dao.impl;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.dao.UserDao;
import org.satuban.tangoportal.portal.model.dto.Dj;
import org.satuban.tangoportal.portal.model.dto.PublicProfile;
import org.satuban.tangoportal.portal.model.entity.User;
import org.satuban.tangoportal.portal.model.enums.RegisterRequestStatus;
import org.satuban.tangoportal.portal.model.enums.UserStatus;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by Artur on 20.05.2017.
 */
@Slf4j
@Repository
public class UserDaoImpl extends AbstractDao<User> implements UserDao {
    private final RowMapper<PublicProfile> publicProfileRowMapper = new BeanPropertyRowMapper<>(PublicProfile.class);
    private final RowMapper<Dj> djRowMapper = new BeanPropertyRowMapper<>(Dj.class);

    UserDaoImpl(NamedParameterJdbcTemplate template) {
        super(template, User.class);
    }

    @Override
    public User findByUsername(String username) {

        List<User> users = template.query("SELECT * FROM user_profile WHERE username = :username",
                Collections.singletonMap("username", username),
                rowMapper);
        return users.isEmpty() ? null : users.get(0);
    }

    @Override
    public User findByInviteToken(String token) {
        try {
            return template.queryForObject("SELECT * FROM user_profile WHERE invite_token = :inviteToken",
                    Collections.singletonMap("inviteToken", token), rowMapper);
        } catch (EmptyResultDataAccessException e) {
            log.info("Not found user with token: {}", token);

            return null;
        }
    }

    @Override
    public User getReferrerByUser(Long userId) {
        return first(template.query("SELECT u.* FROM user_profile u " +
                        " JOIN user_profile u2 ON u.id = u2.invited_by " +
                        " WHERE u2.id = :userId",
                Collections.singletonMap("userId", userId), rowMapper));
    }

    @Override
    public List<User> findReferralsByUser(long userId) {
        return template.query("SELECT up.* FROM user_profile up " +
                        " JOIN register_request rr ON rr.user_id = up.id " +
                        " WHERE up.invited_by = :userId " +
//                        " AND rr.status = 'CONFIRMED'" +
                        " AND rr.status = \'" + RegisterRequestStatus.CONFIRMED + "\'" +
                        " ORDER BY rr.created",
                Collections.singletonMap("userId", userId), rowMapper);
    }

    @Override
    public List<PublicProfile> findDjsByCity(String pid) {
        Map<String, Object> values = new HashMap<>();
        values.put("pid", pid);
        values.put("status", UserStatus.ACTIVE.name());

        return template.query("SELECT u.*, c.name city FROM user_profile u " +
                        "   LEFT JOIN city c ON c.id = u.city_id " +
                        "   WHERE (:pid = '' OR c.pid = :pid) " +
                        "      AND u.dj = TRUE " +
                        "      AND u.status = :status",
                values, publicProfileRowMapper);
    }

    @Override
    public List<PublicProfile> findPhotographers() {
        Map<String, Object> values = new HashMap<>();
        values.put("status", UserStatus.ACTIVE.name());

        return template.query("SELECT u.*, c.name city FROM user_profile u " +
                        " LEFT JOIN city c ON c.id = u.city_id " +
                        " WHERE u.photographer = TRUE AND u.status = :status",
                values, publicProfileRowMapper);
    }

    @Override
    public List<Dj> getAllDjs() {
        return template.query("SELECT u.id, (u.first_name || ' ' || u.second_name) fullName, u.avatar, u.city_id " +
                        " FROM user_profile u " +
                        " WHERE u.status in (:statuses) " +
                        " ORDER BY fullName",
                Collections.singletonMap("statuses", Arrays.asList(UserStatus.ACTIVE.name(), UserStatus.FAKE.name())), djRowMapper);
    }

    @Override
    public Dj getDjById(Long id) {
        if(id == null) {
            return null;
        }

        List<Dj> list = template.query("SELECT u.id, (u.first_name || ' ' || u.second_name) fullName, u.avatar, u.city_id, u.dj_nick nick " +
                        " FROM user_profile u " +
                        " WHERE u.id = :id",
                Collections.singletonMap("id", id), djRowMapper);
        return list.isEmpty() ? null : list.get(0);
    }
}
