package org.satuban.tangoportal.portal.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.satuban.tangoportal.portal.dao.AddressDao;
import org.satuban.tangoportal.portal.dao.RoomDao;
import org.satuban.tangoportal.portal.dao.SchoolDao;
import org.satuban.tangoportal.portal.error.ErrorCode;
import org.satuban.tangoportal.portal.error.TPCityNotFoundException;
import org.satuban.tangoportal.portal.error.TPException;
import org.satuban.tangoportal.portal.model.dto.SchoolDto;
import org.satuban.tangoportal.portal.model.entity.Address;
import org.satuban.tangoportal.portal.model.entity.City;
import org.satuban.tangoportal.portal.model.entity.Room;
import org.satuban.tangoportal.portal.model.entity.School;
import org.satuban.tangoportal.portal.model.enums.PhotoSize;
import org.satuban.tangoportal.portal.model.filters.SchoolListFilter;
import org.satuban.tangoportal.portal.service.AddressService;
import org.satuban.tangoportal.portal.service.PhotoService;
import org.satuban.tangoportal.portal.service.SchoolService;
import org.satuban.tangoportal.portal.service.UserHolder;
import org.satuban.tangoportal.portal.util.ConverterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Artur on 06.06.2017.
 */
@Slf4j
@Service
public class SchoolServiceImpl implements SchoolService {
    private final SchoolDao schoolDao;
    private final AddressService addressService;
    private final ConverterUtil converterUtil;
    private final AddressDao addressDao;
    private final RoomDao roomDao;
    private final UserHolder userHolder;
    private final PhotoService photoService;

    @Autowired
    public SchoolServiceImpl(SchoolDao schoolDao, AddressService addressService, ConverterUtil converterUtil, UserHolder userHolder, AddressDao addressDao, RoomDao roomDao, PhotoService photoService) {
        this.userHolder = userHolder;
        this.schoolDao = schoolDao;
        this.addressService = addressService;
        this.converterUtil = converterUtil;
        this.addressDao = addressDao;
        this.roomDao = roomDao;
        this.photoService = photoService;
    }

    private SchoolDto entityToDto(School entity) {
        SchoolDto dto = new SchoolDto();
        if (entity == null) {
            return dto;
        }


        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setMobilePhone(entity.getMobilePhone());
        dto.setWorkPhone(entity.getWorkPhone());
        dto.setSiteUrl(entity.getSiteUrl());
        dto.setFbUrl(entity.getFbUrl());
        dto.setVkUrl(entity.getVkUrl());
        dto.setDefaultDuration(entity.getDefaultDuration());
        dto.setPhoto(entity.getPhoto());

        dto.setAddress(addressService.getById(entity.getAddressId()));
        return dto;
    }

    private void appendDefaultRoom(School school, Address address) {
        Room room = new Room();
        room.setUserId(userHolder.getUserId());
        room.setName("зал");
        room.setSchoolId(school.getId());
        room.setAddressId(address.getId());

        roomDao.save(room);
    }

    private void validate(SchoolDto request) {
        if (StringUtils.isBlank(request.getName()) ||
                StringUtils.isBlank(request.getMobilePhone())) {
            throw new TPException(ErrorCode.INVALID_FORM_PARAM);
        }
    }

    @Override
    public SchoolDto get() {
        return entityToDto(schoolDao.findByUserId(userHolder.getUserId()));
    }

    @Override
    public List<SchoolDto> getByFilter(SchoolListFilter filter) {
        try {
            City city = addressService.getOrCreateCity(filter.getCity());
            if(city == null) {
                return Collections.emptyList();
            }

            return schoolDao.findByFilter(city.getPid())
                    .stream()
                    .map(this::entityToDto)
                    .collect(Collectors.toList());
        } catch (TPCityNotFoundException e) {
            return Collections.emptyList();
        }
    }

    @Override
    @Transactional
    public SchoolDto save(SchoolDto dto) {
        log.info("Save school: {}", dto);
        validate(dto);

        Address address = addressService.getOrCreateAddress(dto.getAddress());

        School school = schoolDao.findByUserId(userHolder.getUserId());

        boolean isNew = false;
        if (school == null) {
            school = new School();
            school.setUserId(userHolder.getUserId());

            isNew = true;
        }

        school.setName(dto.getName());
        school.setAddressId(address == null ? null : address.getId());
        school.setWorkPhone(dto.getWorkPhone());
        school.setMobilePhone(dto.getMobilePhone());
        school.setVkUrl(dto.getVkUrl());
        school.setFbUrl(dto.getFbUrl());
        school.setSiteUrl(dto.getSiteUrl());
        school.setPhoto(dto.getPhoto());

        school = schoolDao.save(school);

        if (isNew) {
            log.info("Created school: {}", school);
            appendDefaultRoom(school, address);
        }

        SchoolDto dto1 = entityToDto(school);
        log.info("Successful save school[{}]", school.getId());
        return dto1;
    }

    @Override
    @Transactional
    public void remove(long id) {
        schoolDao.removeById(id);
    }

    @Override
    public SchoolDto getById(long id) {
        return entityToDto(schoolDao.getById(id));
    }
}
