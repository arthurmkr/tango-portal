package org.satuban.tangoportal.portal.model.enums;

/**
 * Created by Artur on 17.08.2017.
 */
public enum MapType {
    EVENT,
    SCHOOL,
    DJ
}
