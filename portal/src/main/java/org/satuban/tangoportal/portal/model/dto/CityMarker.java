package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;

/**
 * Created by Artur on 15.07.2017.
 */
@Data
public class CityMarker extends CitySimpleMarker{
    private int schoolCount;
    private int eventCount;
    private int djCount;
    private int activityCount;

    public int getTotalCount() {
        return schoolCount + eventCount + djCount + activityCount;
    }
}
