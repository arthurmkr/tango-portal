package org.satuban.tangoportal.portal.model.entity;

import lombok.Data;
import org.satuban.tangoportal.portal.dao.common.Column;
import org.satuban.tangoportal.portal.dao.common.Table;

/**
 * Created by Artur on 31.05.2017.
 */
@Table
@Data
public class Address implements BaseEntity {
    private static final long serialVersionUID = -5664813962581822060L;

    private Long id;
    @Column
    private String value;
    @Column
    private String pid;
    @Column
    private long cityId;
    @Column
    private double lat;
    @Column
    private double lng;
}
