package org.satuban.tangoportal.portal.web.controller.api.priv;

import org.satuban.tangoportal.portal.model.dto.RoomDto;
import org.satuban.tangoportal.portal.service.SchoolRoomService;
import org.satuban.tangoportal.portal.web.WebConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Artur on 11.06.2017.
 */
@RestController
@RequestMapping(WebConstants.PRIVATE_API + "/festivalRoom")
public class FestivalRoomApiController extends CrudApiController<RoomDto> {
    private final SchoolRoomService festivalSchoolRoomService;

    @Autowired
    public FestivalRoomApiController(SchoolRoomService festivalSchoolRoomService) {
        this.festivalSchoolRoomService = festivalSchoolRoomService;
    }

    @Override
    protected SchoolRoomService getService() {
        return festivalSchoolRoomService;
    }
}
