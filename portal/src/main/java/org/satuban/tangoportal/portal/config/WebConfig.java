package org.satuban.tangoportal.portal.config;

import org.satuban.tangoportal.portal.web.interceptor.AddSessionParamsInterceptor;
import org.satuban.tangoportal.portal.web.interceptor.CachedLocaleChangedInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.URLEncoder;

/**
 * Created by Artur on 28.04.2017.
 */
@Configuration
@EnableSwagger2
@EnableScheduling
@ComponentScan("org.satuban")
public class WebConfig extends WebMvcConfigurerAdapter {
    private final AddSessionParamsInterceptor addSessionParamsInterceptor;
    @Value("${uploadImagesUrl}")
    private String uploadImagesUrl;
    @Value("${uploadFolder}")
    private String uploadFolder;

    public WebConfig(AddSessionParamsInterceptor addSessionParamsInterceptor) {
        this.addSessionParamsInterceptor = addSessionParamsInterceptor;
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
        registry.addInterceptor(addSessionParamsInterceptor);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(uploadImagesUrl + "**").addResourceLocations(uploadFolder);
        registry.addResourceHandler("/swagger-ui.html**").addResourceLocations("classpath:/META-INF/resources/swagger-ui.html");
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(createApiInfo());
    }

    private ApiInfo createApiInfo() {
        return new ApiInfo(
                "Танго-портал",
                "Проект \"Танго-Портал\"",
                "0.0.1",
                null,
                (Contact) null,
                null,
                null);
    }

    @Bean
    public TaskScheduler taskScheduler() {
        return new ConcurrentTaskScheduler();
    }

    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
//        slr.setDefaultLocale(Locale.forLanguageTag("ru"));
        return slr;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new CachedLocaleChangedInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    @Bean
    public AuthenticationEntryPoint ajaxAuthenticationEntryPoint() {
        return (request, response, e) -> {
            String prevUrl = request.getRequestURI();

            if(request.getQueryString() != null && !request.getQueryString().trim().isEmpty()){
                prevUrl += "?" + request.getQueryString();
            }

            response.sendRedirect("/login?prev_url=" + URLEncoder.encode(prevUrl, "UTF-8"));
        };
    }
}
