package org.satuban.tangoportal.portal.model.dto.external.facebook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by Danilov on 06.10.2017.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FacebookCover {
    @JsonProperty("offset_x")
    private Long offsetX;
    @JsonProperty("offset_y")
    private Long offsetY;
    private String source;
    private Long id;
}
