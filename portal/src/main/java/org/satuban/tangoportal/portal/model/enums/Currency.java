package org.satuban.tangoportal.portal.model.enums;

/**
 * Created by Artur on 09.08.2017.
 */
public enum Currency {
    RUB,
    EUR,
    USD,
    GEL
}
