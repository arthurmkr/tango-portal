package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Artur on 15.07.2017.
 */
@Data
public class TangoMap<T extends CitySimpleMarker> {
    private List<T> markers = new ArrayList<>();
    private Coord center;
}
