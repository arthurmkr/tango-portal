package org.satuban.tangoportal.portal.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.satuban.tangoportal.portal.dao.ActivityDao;
import org.satuban.tangoportal.portal.dao.EventDao;
import org.satuban.tangoportal.portal.error.ErrorCode;
import org.satuban.tangoportal.portal.error.TPCityNotFoundException;
import org.satuban.tangoportal.portal.error.TPException;
import org.satuban.tangoportal.portal.model.dto.EventDto;
import org.satuban.tangoportal.portal.model.dto.EventListItem;
import org.satuban.tangoportal.portal.model.entity.Activity;
import org.satuban.tangoportal.portal.model.entity.City;
import org.satuban.tangoportal.portal.model.entity.Event;
import org.satuban.tangoportal.portal.model.filters.EventListFilter;
import org.satuban.tangoportal.portal.service.AddressService;
import org.satuban.tangoportal.portal.service.EventService;
import org.satuban.tangoportal.portal.service.UserHolder;
import org.satuban.tangoportal.portal.util.ConverterUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

/**
 * Created by Artur on 11.08.2017.
 */
@Slf4j
@Service
@Transactional
public class EventServiceImpl implements EventService {
    private final EventDao eventDao;
    private final UserHolder userHolder;
    private final AddressService addressService;
    private final ActivityDao activityDao;
    private final ConverterUtil converterUtil;

    public EventServiceImpl(EventDao eventDao, UserHolder userHolder, AddressService addressService, ActivityDao activityDao, ConverterUtil converterUtil) {
        this.eventDao = eventDao;
        this.userHolder = userHolder;
        this.addressService = addressService;
        this.activityDao = activityDao;
        this.converterUtil = converterUtil;
    }


    @Override
    public List<EventListItem> getListData() {
        return eventDao.getAllListItemsForUser(userHolder.getUserId());
    }

    @Override
    public EventDto getById(long id) {
        Event event = eventDao.getById(id, userHolder.getUserId());

        if (event != null) {
            return convertToDto(event);
        } else {
            throw new TPException(ErrorCode.OBJECT_NOT_FOUND);
        }
    }

    @Override
    public EventDto save(EventDto dto) {
        Event event;

        if (dto.getId() == null) {
            event = new Event();
        } else {
            event = eventDao.getById(dto.getId(), userHolder.getUserId());
        }
        validate(event, dto);

        event.setName(dto.getName());
        event.setStartDate(dto.getStartDate());
        event.setEndDate(dto.getEndDate());
        event.setPhoto(dto.getPhoto());
        event.setUserId(userHolder.getUserId());
        event.setVkUrl(dto.getVkUrl());
        event.setFbUrl(dto.getFbUrl());
        event.setSiteUrl(dto.getSiteUrl());
        event.setCityId(addressService.getOrCreateCity(dto.getCity().getName()).getId());

        event = eventDao.save(event);

        return convertToDto(event);
    }

    @Override
    public void remove(long id) {
        Event event = eventDao.getById(id, userHolder.getUserId());
        if (event != null) {
            if (event.getStartDate().before(new Date())) {
                throw new TPException(ErrorCode.DELETE_FORBIDDEN_STARTED_EVENT);
            }

            eventDao.removeById(id);
        }
    }

    @Override
    public List<EventDto> getEvents(EventListFilter filter) {
        try {
            City city = addressService.getOrCreateCity(filter.getCity());
            return eventDao.findAllActual(city != null ? city.getPid() : "").stream()
                    .map(this::convertToDto)
                    .collect(toList());
        } catch (TPCityNotFoundException e) {
            return Collections.emptyList();
        }
    }

    private EventDto convertToDto(Event event) {
        EventDto dto = new EventDto();
        dto.setId(event.getId());
        dto.setName(event.getName());
        dto.setCity(addressService.getCityDto(event.getCityId()));
        dto.setVkUrl(event.getVkUrl());
        dto.setFbUrl(event.getFbUrl());
        dto.setSiteUrl(event.getSiteUrl());
        dto.setPhoto(event.getPhoto());
        dto.setStartDate(event.getStartDate());
        dto.setEndDate(event.getEndDate());


        List<Activity> milongas = activityDao.findMilongasByEvent(event.getId());
        dto.setMilongas(milongas.stream()
                .map(converterUtil::convertActivityToMilongaDto)
                .collect(toList()));

        return dto;
    }

    private void validate(Event event, EventDto dto) {
        if (StringUtils.isBlank(dto.getName()) ||
                dto.getEndDate() == null ||
                dto.getStartDate() == null ||
                dto.getCity() == null ||
                StringUtils.isBlank(dto.getCity().getName()) ||
                dto.getEndDate().before(dto.getStartDate())
                ) {
            throw new TPException(ErrorCode.NOT_ALL_REQUIRED_FIELDS);
        }


        Date today = DateUtils.addDays(new Date(), 3);

        Date startDate;
        if (dto.getId() == null) {
            startDate = dto.getStartDate();
        } else {
            startDate = event.getStartDate();

            // нельзя редактировать уже завершенное событие
            if (event.getEndDate().before(new Date())) {
                throw new TPException(ErrorCode.EVENT_IS_COMPLETED);
            }
        }

        if (today.after(startDate)) {
            throw new TPException(ErrorCode.EVENT_IS_SHOOLD_BE_IN_FUTURE);
        }
    }
}
