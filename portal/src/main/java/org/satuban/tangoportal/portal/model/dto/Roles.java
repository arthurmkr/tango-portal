package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;

/**
 * Created by Artur on 18.07.2017.
 */
@Data
public class Roles {
    private boolean dj;
    private boolean teacher;
    private boolean schoolLead;
    private boolean organizer;
    private boolean photographer;
    private boolean schoolAdmin;
    private boolean couturier;
    private boolean admin;
}
