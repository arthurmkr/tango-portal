package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.dto.ChangePasswordRequest;
import org.satuban.tangoportal.portal.model.dto.Dj;
import org.satuban.tangoportal.portal.model.dto.Profile;
import org.satuban.tangoportal.portal.model.dto.PublicProfile;
import org.satuban.tangoportal.portal.model.filters.DjListFilter;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * Created by Artur on 07.06.2017.
 */
public interface UserService {
    Profile getProfile();

    Profile save(Profile dto) throws IOException;

    List<Profile> getReferrals();

    List<PublicProfile> getDjsByFilter(DjListFilter filter);

    List<PublicProfile> getPhotographers();

    List<Dj> getAllDjs();

    void changePassword(ChangePasswordRequest request);
}
