package org.satuban.tangoportal.portal.model.dto.external.facebook;

import lombok.Data;

/**
 * Created by Danilov on 25.09.2017.
 */
@Data
public class FacebookPlaceDto {
    private String name;
    private FacebookLocationDto location;
    private String id;
}
