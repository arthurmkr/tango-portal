package org.satuban.tangoportal.portal.model.entity;

import lombok.Data;
import org.satuban.tangoportal.portal.dao.common.Column;
import org.satuban.tangoportal.portal.dao.common.Table;
import org.satuban.tangoportal.portal.model.enums.TokenType;

import java.util.Date;

/**
 * Created by Danilov on 28.09.2017.
 */
@Data
@Table
public class ExternalToken implements BaseEntity {
    private Long id;
    @Column
    private long userId;
    @Column
    private String token;
    @Column
    private Date deadTime;
    @Column
    private TokenType type;
}
