package org.satuban.tangoportal.portal.component;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.dao.EventDao;
import org.satuban.tangoportal.portal.dao.RegisterRequestDao;
import org.satuban.tangoportal.portal.dao.UserDao;
import org.satuban.tangoportal.portal.grabber.Grabber;
import org.satuban.tangoportal.portal.model.dto.ActivityDto;
import org.satuban.tangoportal.portal.model.entity.Event;
import org.satuban.tangoportal.portal.model.entity.RegisterRequest;
import org.satuban.tangoportal.portal.model.entity.User;
import org.satuban.tangoportal.portal.service.ExternalEventReader;
import org.satuban.tangoportal.portal.service.FacebookService;
import org.satuban.tangoportal.portal.service.MailService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

/**
 * Created by Artur on 19.07.2017.
 */
@Slf4j
@Component
public class TangoPortalScheduler {
    private final EventDao eventDao;
    private final FacebookService facebookService;
    private final ExternalEventReader externalEventReader;
    private final RegisterRequestDao registerRequestDao;
    private final MailService mailService;
    private final UserDao userDao;
    private final List<Grabber> grabbers;

    public TangoPortalScheduler(EventDao eventDao,
                                FacebookService facebookService,
                                ExternalEventReader externalEventReader,
                                RegisterRequestDao registerRequestDao,
                                MailService mailService,
                                UserDao userDao,
                                List<Grabber> grabbers) {
        this.eventDao = eventDao;
        this.facebookService = facebookService;
        this.externalEventReader = externalEventReader;
        this.registerRequestDao = registerRequestDao;
        this.mailService = mailService;
        this.userDao = userDao;
        this.grabbers = grabbers;
    }

    @Scheduled(cron = "${schedule.updateEventPictureFromFB}")
    @Transactional
    public void updatePictureFromFacebook() {
        List<Event> events = eventDao.findAllWithOutPictureAndFbLink();

        log.info("Update picture {} events...", events.size());

        if(!events.isEmpty()) {
            facebookService.updateEventsPhoto(events)
                    .stream()
                    .filter(event -> event.getPhoto() != null)
                    .forEach(eventDao::save);
        }

    }

    @Scheduled(cron = "${schedule.checkNewEvents}")
    public void checkNewEvents() {
        log.info("Check new events...");
        List<ActivityDto> activityDtos = externalEventReader.updateEvents();
        log.info("Added {} events", activityDtos.size());
    }

    @Scheduled(cron = "${schedule.sendConfirmationRegisterMails}")
    @Transactional
    public void sendConfirmationRegisterMails() {
        List<RegisterRequest> requests = registerRequestDao.getNewForSending();
        if (requests.isEmpty()) {
            return;
        }

        log.info("Send confirmation about registration");
        List<Long> userIds = requests.stream()
                .map(RegisterRequest::getUserId)
                .collect(toList());

        if (userIds.isEmpty()) {
            return;
        }

        Map<Long, User> users = userDao.findByIds(userIds).stream()
                .collect(toMap(User::getId, identity()));

        for (RegisterRequest request : requests) {
            User user = users.get(request.getUserId());

            if (user != null) {
                log.info("Send confirm mail for user[{}]. Request[{}]", user.getId(), request.getId());
                mailService.sendRegisterConfirmationToUser(user.getUsername(), request.getToken(), user.getLanguage());
            } else {
                log.warn("RegisterRequest[{}] references to user[{}]. But user is no exists.", request.getId(), request.getUserId());
            }
        }

        registerRequestDao.markAsSent(requests);
    }

    @Scheduled(cron = "${schedule.sendMailsToReferrers}")
    @Transactional
    public void sendMailsToReferrers() {
        List<RegisterRequest> requests = registerRequestDao.getConfirmedForSending();
        if (requests.isEmpty()) {
            return;
        }

        log.info("Send notification mails to referrers");
        List<Long> userIds = requests.stream()
                .map(RegisterRequest::getUserId)
                .collect(toList());

        if (userIds.isEmpty()) {
            return;
        }

        // получить список пользователей который ждут подтверждения, сгрупированных по пригласителю
        Map<Long, List<User>> groupByReferrers = getUsersGroupedByReferrer(userIds);

        // получить список пригласителей
        List<User> referrers = userDao.findByIds(groupByReferrers.keySet());

        for (User referrer : referrers) {
            // выслать каждому пригласителю письмо со списком имен
            log.info("Send confirm mail to referrer[{}]", referrer.getId());

            mailService.sendRegisterConfirmationToReferrers(referrer.getUsername(), groupByReferrers.get(referrer.getId()), referrer.getLanguage());
        }

        registerRequestDao.markAsSent(requests);
    }

    @Scheduled(cron = "${schedule.approveRegistration}")
    @Transactional
    public void sendApproveRegistrationNotification() {
        List<RegisterRequest> requests = registerRequestDao.getApprovedForSending();
        if (requests.isEmpty()) {
            return;
        }

        log.info("Send notification about approve registrations");

        List<Long> userIds = requests.stream()
                .map(RegisterRequest::getUserId)
                .collect(toList());

        if (userIds.isEmpty()) {
            return;
        }

        List<User> newUsers = userDao.findByIds(userIds);

        for (User user : newUsers) {
            // выслать каждому пригласителю письмо со списком имен
            log.info("Send approve registration mail to user[{}]", user.getId());

            mailService.sendApproveRegistrationMails(user.getUsername(), user.getPassword(), user.getLanguage());
        }

        registerRequestDao.markAsSent(requests);
    }

//    @Scheduled(cron = "0 * * * * *")
    @Transactional
    public void grabFacebook() {
        try {
            grabbers.get(0).grab();
        } catch (IOException e) {
            log.error("Error:", e);
        }
    }

    private Map<Long, List<User>> getUsersGroupedByReferrer(List<Long> userIds) {
        List<User> users = userDao.findByIds(userIds);

        Map<Long, List<User>> groupByReferrers = new HashMap<>();
        for (User user : users) {
            List<User> group = groupByReferrers.computeIfAbsent(user.getInvitedBy(), aLong -> new ArrayList<>());

            group.add(user);
        }
        return groupByReferrers;
    }
}
