package org.satuban.tangoportal.portal.web.controller.api.priv;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.model.dto.ActivityDto;
import org.satuban.tangoportal.portal.service.ActivityService;
import org.satuban.tangoportal.portal.web.WebConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Artur on 19.06.2017.
 */
@Slf4j
@RestController
@RequestMapping(WebConstants.PRIVATE_API + "/activity")
public class ActivityPrivateApiController {
    private final ActivityService activityService;

    @Autowired
    public ActivityPrivateApiController(ActivityService activityService) {
        this.activityService = activityService;
    }

    @GetMapping("/{id}")
    public ActivityDto get(@PathVariable long id) {
        return activityService.getById(id);
    }

    @PostMapping
    public HttpStatus saveActivity(@RequestBody ActivityDto request) {
        log.info("Request: {}", request);
        activityService.save(request);
        return HttpStatus.OK;
    }


    @DeleteMapping("/{id}")
    public HttpStatus remove(@PathVariable long id) {
        activityService.remove(id);
        return HttpStatus.OK;
    }

}
