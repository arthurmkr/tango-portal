package org.satuban.tangoportal.portal.model.dto.external.facebook.data;

import org.satuban.tangoportal.portal.model.dto.external.facebook.FacebookPicture;

/**
 * Created by Danilov on 06.10.2017.
 */
public class FacebookPictureData extends FacebookData<FacebookPicture> {
}
