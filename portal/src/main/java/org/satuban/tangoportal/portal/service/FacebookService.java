package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.entity.Event;

import java.util.List;

/**
 * Created by Danilov on 06.10.2017.
 */
public interface FacebookService {
    String FACEBOOK_URL = "https://facebook.com";
    String FACEBOOK_GRAPH_URL = "https://graph.facebook.com";
    String FACEBOOK_API_VERSION = "v2.10";
    String FACEBOOK_EVENTS_URL = FACEBOOK_URL + "/events/";
    String FACEBOOK_GROUPS_URL = FACEBOOK_URL + "/groups/";

    String getFacebookPictureUrlById(String id);
    List<Event> updateEventsPhoto(List<Event> events);
}
