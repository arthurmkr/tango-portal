package org.satuban.tangoportal.portal.web.controller.api.priv;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.model.dto.teacher.TeacherDto;
import org.satuban.tangoportal.portal.service.TeacherService;
import org.satuban.tangoportal.portal.web.WebConstants;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * Created by Artur on 07.06.2017.
 */
@Slf4j
@RestController
@RequestMapping(WebConstants.PRIVATE_API + "/teacher")
public class TeacherApiController {
    private final TeacherService teacherService;

    public TeacherApiController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }


    @GetMapping
    public List<TeacherDto> get() {
        return teacherService.getAll();
    }

    @DeleteMapping("/{id}")
    public HttpStatus remove(@PathVariable long id) {
        teacherService.remove(id);
        return HttpStatus.OK;
    }

    @PostMapping
    public TeacherDto save(@RequestBody TeacherDto dto)  {
        log.info("dto: {}", dto);
        return teacherService.save(dto);
    }


}
