package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.dto.external.facebook.FacebookAccessToken;

/**
 * Created by Danilov on 26.09.2017.
 */
public interface FacebookOauthService {
    FacebookAccessToken generateApplicationToken();
    FacebookAccessToken generateAccessTokenByCode(String code);

    String generateOauthUrl();
}
