package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;
import org.satuban.tangoportal.portal.model.entity.User;

/**
 * Created by Artur on 23.07.2017.
 */
@Data
public class RegistrationConfirmResponse {
    private boolean already;
    private boolean userInvalid;
    private User user;
}
