package org.satuban.tangoportal.portal.dao.impl;

import org.satuban.tangoportal.portal.dao.AddressDao;
import org.satuban.tangoportal.portal.model.entity.Address;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

/**
 * Created by Artur on 31.05.2017.
 */
@Repository
public class AddressDaoImpl extends AbstractDao<Address> implements AddressDao {
    AddressDaoImpl(NamedParameterJdbcTemplate template) {
        super(template, Address.class);
    }

    @Override
    public Address findByValue(String value) {
        List<Address> list = template.query("SELECT * FROM address WHERE value = :value",
                Collections.singletonMap("value", value),
                rowMapper);
        return list.isEmpty() ? null : list.get(0);
    }
}
