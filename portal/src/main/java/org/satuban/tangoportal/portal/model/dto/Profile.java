package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Artur on 04.07.2017.
 */
@Data
public class Profile implements Serializable {
    private static final long serialVersionUID = -5150244894840298410L;

    private long id;
    private String firstName;
    private String secondName;
    private String fbProfile;
    private String vkProfile;
    private String email;
    private CityDto city;
    private String avatar;
    private String djNick;
    private Roles roles;
    private String referralLink;
}
