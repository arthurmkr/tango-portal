package org.satuban.tangoportal.portal.error;

/**
 * Created by Artur on 20.07.2017.
 */
public class TPObjectNotFoundException extends TPException {
    private static final long serialVersionUID = 8389930674051861427L;

    public TPObjectNotFoundException() {
        super(ErrorCode.OBJECT_NOT_FOUND);
    }
}
