package org.satuban.tangoportal.portal.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;
import org.satuban.tangoportal.portal.dao.ActivityDao;
import org.satuban.tangoportal.portal.dao.RoomDao;
import org.satuban.tangoportal.portal.dao.SchoolDao;
import org.satuban.tangoportal.portal.dao.TeacherDao;
import org.satuban.tangoportal.portal.model.dto.DayTimetable;
import org.satuban.tangoportal.portal.model.dto.TimetableItem;
import org.satuban.tangoportal.portal.model.dto.TimetableRow;
import org.satuban.tangoportal.portal.model.dto.WeekTimetable;
import org.satuban.tangoportal.portal.model.entity.Activity;
import org.satuban.tangoportal.portal.model.entity.Room;
import org.satuban.tangoportal.portal.model.entity.School;
import org.satuban.tangoportal.portal.model.entity.Teacher;
import org.satuban.tangoportal.portal.model.enums.LessonLevel;
import org.satuban.tangoportal.portal.service.TimetableService;
import org.satuban.tangoportal.portal.service.UserHolder;
import org.satuban.tangoportal.portal.util.Utils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.DayOfWeek;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

/**
 * Created by Artur on 13.06.2017.
 */
@Slf4j
@Service
public class TimetableServiceImpl extends AbstractService implements TimetableService {
    private static List<DayOfWeek> sortedDays = Collections.unmodifiableList(Arrays.asList(
            DayOfWeek.MONDAY,
            DayOfWeek.TUESDAY,
            DayOfWeek.WEDNESDAY,
            DayOfWeek.THURSDAY,
            DayOfWeek.FRIDAY,
            DayOfWeek.SATURDAY,
            DayOfWeek.SUNDAY
    ));
    private final ActivityDao activityDao;
    private final RoomDao roomDao;
    private final TeacherDao teacherDao;
    private final SchoolDao schoolDao;

    public TimetableServiceImpl(UserHolder userHolder, ActivityDao activityDao, RoomDao roomDao, TeacherDao teacherDao, SchoolDao schoolDao) {
        super(userHolder);
        this.activityDao = activityDao;
        this.roomDao = roomDao;
        this.teacherDao = teacherDao;
        this.schoolDao = schoolDao;
    }


    @Override
    @Transactional
    public WeekTimetable getForSchool(Date today) {
        School school = schoolDao.findByUserId(getUserId());
        if (school == null) {
            return new WeekTimetable();
        }

        return getForSchool(school.getId(), today);
    }

    @Override
    public WeekTimetable getForSchool(long schoolId, Date today) {
        WeekTimetable timetable = new WeekTimetable();

        // высчитать индексы залов и сохранить названия
        List<Room> rooms = roomDao.findAllForSchool(schoolId);
        Map<Long, Integer> roomIndexes = new HashMap<>();
        List<String> roomNames = new ArrayList<>();
        for (int i = 0; i < rooms.size(); i++) {
            Room room = rooms.get(i);

            roomIndexes.put(room.getId(), i);
            roomNames.add(room.getName());
        }
        // установить список залов
        timetable.setRooms(roomNames.size() > 1 ? roomNames : Collections.singletonList(""));

        // найти все активности на текущей неделе
        List<Activity> schoolActivities = findTimetableActivities(schoolId, today);

        // подготовить коллекцию дневных расписаний
        Map<DayOfWeek, DayTimetable> dayActivities = prepareDayTimetables(roomIndexes, schoolActivities);

        // упорядочить дневные расписания
        List<DayTimetable> sortedDayActivities = sortedDays
                .stream()
                .map(dayOfWeek -> dayActivities.computeIfAbsent(dayOfWeek, this::createDayTimetable))
                .collect(Collectors.toList());

        timetable.setDays(sortedDayActivities);

        // установить список преподавателей
        timetable.setTeachers(createTeacherMap(schoolActivities));

        return timetable;
    }

    private Map<DayOfWeek, DayTimetable> prepareDayTimetables(Map<Long, Integer> roomIndexes, List<Activity> schoolActivities) {
        Map<DayOfWeek, DayTimetable> dayActivities = new HashMap<>();
        Map<Date, TimetableRow> rows = new HashMap<>();
        Calendar calendar = Calendar.getInstance();

        for (Activity activity : schoolActivities) {
            DayTimetable dayTimetable = dayActivities.computeIfAbsent(activity.getDayOfWeek(), this::createDayTimetable);

            if (dayTimetable.getRows().isEmpty()) {
                rows.clear();
            }

            Date endTime = DateUtils.addMinutes(activity.getStartTime(), activity.getDuration());
            TimetableRow row = rows.computeIfAbsent(endTime,
                    date -> {
                        TimetableRow newRow = createTimetableRow(roomIndexes, activity, endTime);

                        dayTimetable.getRows().add(newRow); // Добавить новую строку в дневное расписание
                        return newRow;
                    });


            TimetableItem item = createTimetableItem(activity, endTime);

            row.getItems()[roomIndexes.get(activity.getRoomId())] = item;
        }
        return dayActivities;
    }

    private List<Activity> findTimetableActivities(long schoolId, Date today) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        Date start = cal.getTime();

        cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        Date end = DateUtils.addDays(start, 6);

        List<Activity> activities = activityDao.findAllForSchool(schoolId, start, end);

        log.info("schoolId: {}, StartDate: {}, EndDate: {}, count of activities: {}", schoolId, start, end, activities.size());
        return activities;
    }

    private Map<Long, String> createTeacherMap(List<Activity> schoolActivities) {
        Set<Long> firstTeachers = schoolActivities.stream()
                .map(Activity::getFirstTeacherId)
                .distinct()
                .collect(Collectors.toSet());

        Set<Long> secondTeachers = schoolActivities.stream()
                .map(Activity::getSecondTeacherId)
                .distinct()
                .collect(Collectors.toSet());

        firstTeachers.addAll(secondTeachers);

        List<Teacher> activityTeachers = teacherDao.findByIds(firstTeachers);

        return activityTeachers.stream()
                .collect(toMap(Teacher::getId, t -> t.getFirstName() + " " + t.getSecondName()));
    }

    private DayTimetable createDayTimetable(DayOfWeek dayOfWeek) {
        DayTimetable object = new DayTimetable();
        object.setDayOfWeek(dayOfWeek);
        return object;
    }

    private TimetableRow createTimetableRow(Map<Long, Integer> roomIndexes, Activity activity, Date endTime) {
        TimetableRow newRow = new TimetableRow();
        newRow.setStartTime(activity.getStartTime());
        newRow.setEndTime(endTime);
        newRow.setItems(new TimetableItem[roomIndexes.size()]);
        return newRow;
    }

    private TimetableItem createTimetableItem(Activity activity, Date endTime) {
        TimetableItem item = new TimetableItem();
        item.setId(activity.getId());
        item.setName(activity.getName());
        item.setType(activity.getType());
        item.setLevel(LessonLevel.toSet(activity.getLessonLevel()));
        item.setStartTime(activity.getStartTime());
        item.setEndTime(endTime);
        item.setFirstTeacherId(activity.getFirstTeacherId());
        item.setSecondTeacherId(activity.getSecondTeacherId());
        return item;
    }
}
