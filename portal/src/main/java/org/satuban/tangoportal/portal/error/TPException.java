package org.satuban.tangoportal.portal.error;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by Artur on 06.06.2017.
 */
public class TPException extends RuntimeException {
    private static final long serialVersionUID = 6699078061134453163L;

    private ErrorCode code;

    public TPException(ErrorCode code) {
        this.code = code;
    }

    public TPException(ErrorCode code, String message) {
        super(message);
        this.code = code;
    }

    public ErrorCode getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return  code.name() + (StringUtils.isNotBlank(super.getMessage()) ?  ": " + super.getMessage() : "");
    }
}
