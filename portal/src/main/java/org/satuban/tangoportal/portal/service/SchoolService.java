package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.dto.SchoolDto;
import org.satuban.tangoportal.portal.model.filters.SchoolListFilter;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by Artur on 06.06.2017.
 */
public interface SchoolService {
    SchoolDto get();

    List<SchoolDto> getByFilter(SchoolListFilter filter);

    SchoolDto save(SchoolDto dto);

    void remove(long id);

    SchoolDto getById(long id);
}
