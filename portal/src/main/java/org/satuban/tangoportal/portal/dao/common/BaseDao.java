package org.satuban.tangoportal.portal.dao.common;

import org.satuban.tangoportal.portal.model.entity.BaseEntity;
import org.satuban.tangoportal.portal.model.entity.School;
import org.satuban.tangoportal.portal.model.entity.Teacher;

import java.util.Collection;
import java.util.List;

/**
 * Created by Artur on 26.04.2017.
 */
public interface BaseDao<T extends BaseEntity> {
    /**
     * Найти все сущности
     */
    List<T> getAll();

    /**
     * Найти сущность по ID
     */
    T findById(Long id);

    List<T> findByIds(Collection<Long> ids);

    /**
     * Удалить сущность по ID
     */
    boolean removeById(Long id);

    T save(T entity);

    T getById(long id);

    T getOrCreate(Long id);

    String getTableName();
}