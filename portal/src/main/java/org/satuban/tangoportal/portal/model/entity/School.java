package org.satuban.tangoportal.portal.model.entity;

import lombok.Data;
import org.satuban.tangoportal.portal.dao.common.Column;
import org.satuban.tangoportal.portal.dao.common.Table;

/**
 * Created by Artur on 16.05.2017.
 */
@Table
@Data
public class School implements BaseEntity {
    private static final long serialVersionUID = -5829244800732228439L;

    private Long id;
    @Column
    private String name;
    @Column
    private String workPhone;
    @Column
    private String mobilePhone;
    @Column
    private Long addressId;
    @Column
    private String siteUrl;
    @Column
    private String fbUrl;
    @Column
    private String vkUrl;
    @Column
    private Integer defaultDuration;
    @Column
    private long userId;
    @Column
    private String photo;
}
