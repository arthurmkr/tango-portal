package org.satuban.tangoportal.portal.dao.impl;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.dao.ActivityDao;
import org.satuban.tangoportal.portal.model.entity.Activity;
import org.satuban.tangoportal.portal.model.enums.ActivityType;
import org.satuban.tangoportal.portal.model.enums.UserStatus;
import org.satuban.tangoportal.portal.util.Utils;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by Artur on 26.05.2017.
 */
@Slf4j
@Repository
public class ActivityDaoImpl extends AbstractDao<Activity> implements ActivityDao {

    public static final int MILLISECOND_PER_DAY = 24 * 60 * 60 * 1000;

    ActivityDaoImpl(NamedParameterJdbcTemplate template) {
        super(template, Activity.class);
    }

    @Override
    public List<Activity> findAllByRange(Date from, Date to, String cityPid) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(from);
        int i = calendar.get(Calendar.DAY_OF_WEEK);
        int curDay = (i + 5) % 7;

        Map<String, Object> params = new HashMap<>();
        params.put("fromDate", from);
        params.put("toDate", to);
        params.put("countDays", (int) ((to.getTime() - from.getTime()) / MILLISECOND_PER_DAY));
        params.put("curDay", curDay);
        params.put("cityPid", cityPid);

        return template.query("SELECT " +
                        "   act.*, " +
                        "   (to_date(:fromDate, 'yyyy-mm-dd') + day_shifts.shift) action_date " +
                        " FROM activity act " +
                        " JOIN address addr ON addr.id = act.address_id " +
                        " JOIN city ON city.id = addr.city_id " +
                        ", (SELECT generate_series(0, :countDays) shift) day_shifts " +
                        " WHERE " +
                        " ((regular = TRUE " +
                        " AND to_date(:fromDate, 'yyyy-mm-dd') + day_shifts.shift >= start_date " +
                        " AND (end_date IS NULL OR to_date(:fromDate, 'yyyy-mm-dd') + day_shifts.shift <= end_date ) " +
                        " AND (:curDay + day_shifts.shift) % 7 = day_of_week) " +
                        " OR " +
                        "(regular = FALSE AND start_date = to_date(:fromDate, 'yyyy-mm-dd') + day_shifts.shift)) " +
                        " AND city.pid = :cityPid " +
                        " ORDER BY action_date, start_time ASC ",
                params, rowMapper);
    }

    @Override
    public List<Activity> findAllForSchool(long schoolId, Date from, Date to) {
        int curDay = Utils.convertToMondayWeek(from);

        Map<String, Object> params = new HashMap<>();
        params.put("fromDate", from);
        params.put("toDate", to);
        params.put("countDays", (int) ((to.getTime() - from.getTime()) / MILLISECOND_PER_DAY));
        params.put("curDay", curDay);
        params.put("schoolId", schoolId);

        return template.query("SELECT " +
                        "   act.*, " +
                        "   (to_date(:fromDate, 'yyyy-mm-dd') + day_shifts.shift) action_date" +
                        " FROM activity act " +
                        " JOIN room r ON  r.id = act.room_id " +
                        " JOIN school s ON s.id = r.school_id " +
                        ", (SELECT generate_series(0, :countDays) shift) day_shifts " +
                        " WHERE " +
                        " ((regular = TRUE " +
                        " AND to_date(:fromDate, 'yyyy-mm-dd') + day_shifts.shift >= start_date " +
                        " AND (end_date IS NULL OR to_date(:fromDate, 'yyyy-mm-dd') + day_shifts.shift <= end_date ) " +
                        " AND (:curDay + day_shifts.shift) % 7 = day_of_week) " +
                        " OR " +
                        "(regular = FALSE AND start_date = to_date(:fromDate, 'yyyy-mm-dd') + day_shifts.shift)) " +
                        " AND s.id = :schoolId" +
                        " ORDER BY day_of_week, start_time, start_time + make_interval(mins := duration ), r.id ",
                params, rowMapper);
    }

    @Override
    public List<Activity> findMilongasForUser(long userId) {
        return template.query("SELECT a.* FROM activity a " +
                        " JOIN user_profile u ON u.id = a.user_id AND u.id = :userId" +
                        " WHERE a.event_id is null " +
                        "   AND a.type = '" + ActivityType.MILONGA + "' " +
                        "   AND u.status = '" + UserStatus.ACTIVE + "'",
                Collections.singletonMap("userId", userId), rowMapper);
    }

    @Override
    public List<Activity> findMilongasByEvent(long eventId) {
        return template.query("SELECT a.* FROM activity a " +
                        " WHERE a.type = '" + ActivityType.MILONGA + "' " +
                        "   AND a.event_id = :eventId",
                Collections.singletonMap("eventId", eventId), rowMapper);
    }
}
