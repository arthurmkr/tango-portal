package org.satuban.tangoportal.portal.error;

/**
 * Created by Danilov on 26.09.2017.
 */
public class ExternalExeption extends RuntimeException {
    public ExternalExeption(String message) {
        super(message);
    }

    public ExternalExeption(String message, Throwable cause) {
        super(message, cause);
    }
}
