package org.satuban.tangoportal.portal.model.dto.external.facebook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Created by Danilov on 27.09.2017.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FacebookPaging {
    private String next;
    private String previous;
}
