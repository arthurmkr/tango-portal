package org.satuban.tangoportal.portal.dao;

import org.satuban.tangoportal.portal.dao.common.BaseDao;
import org.satuban.tangoportal.portal.model.entity.Address;

import java.util.List;

/**
 * Created by Artur on 31.05.2017.
 */
public interface AddressDao extends BaseDao<Address> {
    Address findByValue(String value);
}
