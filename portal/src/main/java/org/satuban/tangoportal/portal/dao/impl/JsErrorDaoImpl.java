package org.satuban.tangoportal.portal.dao.impl;

import org.satuban.tangoportal.portal.dao.JsErrorDao;
import org.satuban.tangoportal.portal.model.dto.JsError;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Created by Artur on 21.07.2017.
 */
@Repository
public class JsErrorDaoImpl extends AbstractDao<JsError> implements JsErrorDao {
    JsErrorDaoImpl(NamedParameterJdbcTemplate template) {
        super(template, JsError.class);
    }
}
