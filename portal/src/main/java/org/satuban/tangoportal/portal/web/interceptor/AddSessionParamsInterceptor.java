package org.satuban.tangoportal.portal.web.interceptor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by Artur on 29.07.2017.
 */
@Component
public class AddSessionParamsInterceptor extends HandlerInterceptorAdapter {
    private final String appServerUrl;
    private final boolean debug;

    public AddSessionParamsInterceptor(@Value("${app.server.url}") String appServerUrl,
                                       @Value("${app.debug:true}") boolean debug) {
        this.appServerUrl = appServerUrl;
        this.debug = debug;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        session.setAttribute("serverUrl", appServerUrl);
        session.setAttribute("debug", debug);

        return true;
    }
}
