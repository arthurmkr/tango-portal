package org.satuban.tangoportal.portal.model.enums;

/**
 * Created by Artur on 09.08.2017.
 */
public enum PhotoRatio {
    SQUARE(1.),
    RATIO_3_TO_1(3./1.),
    RATIO_16_TO_9(16./9.);

    private double ratio;

    PhotoRatio(double ratio) {
        this.ratio = ratio;
    }

    public double getRatio() {
        return ratio;
    }
}
