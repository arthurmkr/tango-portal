package org.satuban.tangoportal.portal.model.filters;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Artur on 10.07.2017.
 */
@Data
public class PlaybillFilter implements Serializable {
    private static final long serialVersionUID = -1487701858938670392L;
    private String city;
    private Date from;
    private Date to;
}
