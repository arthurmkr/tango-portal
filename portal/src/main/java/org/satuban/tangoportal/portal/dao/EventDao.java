package org.satuban.tangoportal.portal.dao;

import org.satuban.tangoportal.portal.dao.common.BaseDao;
import org.satuban.tangoportal.portal.model.dto.EventListItem;
import org.satuban.tangoportal.portal.model.entity.Event;

import java.util.List;

/**
 * Created by Artur on 11.08.2017.
 */
public interface EventDao extends BaseDao<Event> {
    List<EventListItem> getAllListItemsForUser(long userId);

    Event getById(long id, long userId);

    List<Event> findAllActual(String pid);

    List<Event> findAllWithOutPictureAndFbLink();
}
