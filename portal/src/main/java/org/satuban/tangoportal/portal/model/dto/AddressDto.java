package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Artur on 04.06.2017.
 */
@Data
public class AddressDto implements Serializable {
    private static final long serialVersionUID = -5273764900587546810L;

    private Long id;
    private String pid;
    private String value;
    private double lat;
    private double lng;
    private CityDto city;
}
