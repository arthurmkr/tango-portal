package org.satuban.tangoportal.portal.model.dto.external.facebook;

import lombok.Data;

/**
 * Created by Danilov on 25.09.2017.
 */
@Data
public class FacebookLocationDto {
    private String city;
    private String country;
    private String street;
    private Double latitude;
    private Double longitude;
}
