package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;
import org.satuban.tangoportal.portal.model.dto.teacher.TeacherLink;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Artur on 23.06.2017.
 */
@Data
public class Playbill {
    private Map<Long, TeacherLink> teachers = new HashMap<>();
    private List<ActivityDay> days = new ArrayList<>();
}
