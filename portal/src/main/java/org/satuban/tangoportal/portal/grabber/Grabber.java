package org.satuban.tangoportal.portal.grabber;

import java.io.IOException;

/**
 * Created by Artur on 25.09.2017.
 */
public interface Grabber {
    void grab() throws IOException;
}
