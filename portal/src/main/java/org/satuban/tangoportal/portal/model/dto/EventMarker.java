package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;
import org.satuban.tangoportal.portal.model.enums.ActivityType;

import java.io.Serializable;

/**
 * Created by Artur on 01.06.2017.
 */
@Data
public class EventMarker implements Serializable {
    private static final long serialVersionUID = -2491836193964364360L;

    private double lat;
    private double lng;
    private ActivityType type;
}
