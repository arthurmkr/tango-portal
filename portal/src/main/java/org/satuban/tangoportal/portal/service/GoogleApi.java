package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.dto.GeoCodeResponse;
import org.satuban.tangoportal.portal.service.impl.GoogleApiImpl;

/**
 * Created by Artur on 31.05.2017.
 */
public interface GoogleApi {
    GeoCodeResponse.Result getPlaceForCity(String cityName);
}
