package org.satuban.tangoportal.portal.dao;

import org.satuban.tangoportal.portal.dao.common.BaseDao;
import org.satuban.tangoportal.portal.model.entity.Teacher;

import java.util.List;

/**
 * Created by Artur on 07.06.2017.
 */
public interface TeacherDao extends BaseDao<Teacher> {
    List<Teacher> getBySchool(long schoolId);
}
