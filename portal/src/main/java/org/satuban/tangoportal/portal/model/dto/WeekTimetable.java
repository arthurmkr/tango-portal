package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Artur on 13.06.2017.
 */
@Data
public class WeekTimetable implements Serializable {
    private static final long serialVersionUID = 4200159109372538931L;

    private List<String> rooms;
    private List<DayTimetable> days = new ArrayList<>();
    private Map<Long, String> teachers = new HashMap<>();
}
