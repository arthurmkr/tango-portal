package org.satuban.tangoportal.portal.model.dto.external;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.satuban.tangoportal.portal.model.enums.ActivityType;

import java.util.Date;

/**
 * Created by Danilov on 26.09.2017.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExternalEvent {
    private String id;
    private String description;
    private Date endTime;
    private Date startTime;
    private String city;
    private String country;
    private String street;
    private Double latitude;
    private Double longitude;
    private String name;
    private String url;
    private ActivityType activityType;
}
