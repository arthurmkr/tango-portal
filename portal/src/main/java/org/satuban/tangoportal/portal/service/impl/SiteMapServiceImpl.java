package org.satuban.tangoportal.portal.service.impl;

import org.satuban.tangoportal.portal.dao.AddressDao;
import org.satuban.tangoportal.portal.dao.CityDao;
import org.satuban.tangoportal.portal.dao.SchoolDao;
import org.satuban.tangoportal.portal.dao.UserDao;
import org.satuban.tangoportal.portal.model.dto.Dj;
import org.satuban.tangoportal.portal.model.dto.SiteMapDto;
import org.satuban.tangoportal.portal.model.entity.Address;
import org.satuban.tangoportal.portal.model.entity.City;
import org.satuban.tangoportal.portal.model.entity.School;
import org.satuban.tangoportal.portal.model.enums.SiteMapType;
import org.satuban.tangoportal.portal.service.SiteMapService;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Danilov on 17.08.2017.
 */
@Service
public class SiteMapServiceImpl implements SiteMapService {
    private final static String TEMP_PATH_SITEMAP = "";
    private final static String TEMP_FILE_SITEMAP = "sitemap.xml";
    private final static String SHCOOL_URL = "/schools";
    private final static String DJ_URL = "/djs";

    private final SchoolDao schoolDao;
    private final UserDao userDao;
    private final AddressDao addressDao;
    private final CityDao cityDao;

    public SiteMapServiceImpl(SchoolDao schoolDao,
                              UserDao userDao,
                              AddressDao addressDao,
                              CityDao cityDao) {
        this.schoolDao = schoolDao;
        this.userDao = userDao;
        this.addressDao = addressDao;
        this.cityDao = cityDao;
    }

    @Override
    public List<SiteMapDto> getCities(String currentUrl) {
        Set<String> citiesFormSchool = getCityFromSchool().stream()
                .map(City::getName)
                .collect(Collectors.toSet());
        Set<String> citiesFormDj = getCityFromDj().stream()
                .map(City::getName)
                .collect(Collectors.toSet());

        Stream<SiteMapDto> schoolSiteMapDtoStream = citiesFormSchool.stream()
                .map(city -> new SiteMapDto(city,
                        currentUrl + SHCOOL_URL + "?city=" + city,
                        SiteMapType.SCHOOLS));

        Stream<SiteMapDto> djSiteMapDtoStream = citiesFormDj.stream()
                .map(city -> new SiteMapDto(city,
                        currentUrl + DJ_URL + "?city=" + city,
                        SiteMapType.DJS));

        return Stream.concat(schoolSiteMapDtoStream, djSiteMapDtoStream)
                .collect(Collectors.toList());
    }

    private List<City> getCityFromDj() {
        List<Dj> allDjs = userDao.getAllDjs();

        List<Long> cityIds = allDjs.stream()
                .map(Dj::getCityId)
                .collect(Collectors.toList());

        return cityDao.findByIds(cityIds);
    }

    private List<City> getCityFromSchool() {
        List<School> schoolList = schoolDao.findAll();

        Set<Long> addressIds = schoolList.stream()
                .map(School::getAddressId)
                .collect(Collectors.toSet());

        List<Address> addresses = addressDao.findByIds(new ArrayList<>(addressIds));

        List<Long> cityIds = addresses.stream()
                .map(Address::getCityId)
                .collect(Collectors.toList());

        return cityDao.findByIds(cityIds);
    }
}
