package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.dto.JsError;
import org.satuban.tangoportal.portal.model.dto.JsErrorRequest;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Artur on 21.07.2017.
 */
public interface UtilService {
    void saveJsError(JsErrorRequest error, HttpServletRequest request);
}
