package org.satuban.tangoportal.portal.web.controller.api.priv;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.model.enums.PhotoSize;
import org.satuban.tangoportal.portal.service.PhotoService;
import org.satuban.tangoportal.portal.web.WebConstants;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Artur on 08.08.2017.
 */
@Slf4j
@RestController
@RequestMapping(WebConstants.PRIVATE_API + "/photo")
public class PhotoPrivateApiController {
    private final PhotoService photoService;

    public PhotoPrivateApiController(PhotoService photoService) {
        this.photoService = photoService;
    }

    @Data
    @AllArgsConstructor
    private class ImageResponse {
        private String url;
    }

    @PostMapping(consumes = {"multipart/form-data"})
    public ImageResponse saveAvatar(@RequestPart(value = "size") PhotoSize size,
                                    @RequestPart(value = "file") MultipartFile file) {
        log.info("Save [{}] with size [{}]", file, size);
        return new ImageResponse(photoService.savePhoto(size, file));
    }
}
