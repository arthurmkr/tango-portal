package org.satuban.tangoportal.portal.grabber;

import lombok.Builder;
import lombok.ToString;
import org.satuban.tangoportal.portal.model.enums.ActivityType;

import java.util.Date;

/**
 * Created by Artur on 25.09.2017.
 */
@Builder
@ToString
public class GrabberItem {
    private String id;
    private String url;
    private String name;
    private Date from;
    private Date to;
    private ActivityType type;
}
