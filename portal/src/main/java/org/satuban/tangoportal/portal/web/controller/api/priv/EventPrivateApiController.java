package org.satuban.tangoportal.portal.web.controller.api.priv;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.model.dto.EventListItem;
import org.satuban.tangoportal.portal.model.dto.EventDto;
import org.satuban.tangoportal.portal.service.EventService;
import org.satuban.tangoportal.portal.web.WebConstants;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Artur on 11.08.2017.
 */
@Slf4j
@RestController
@RequestMapping(WebConstants.PRIVATE_API + "/event")
public class EventPrivateApiController {
    private final EventService eventService;

    public EventPrivateApiController(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping("/{id}")
    public EventDto get(@PathVariable long id) {
        return eventService.getById(id);
    }

    @PostMapping
    public EventDto save(@RequestBody EventDto dto) {
        return eventService.save(dto);
    }

    @GetMapping("/selectListData")
    private List<EventListItem> getSelectListData() {
        return eventService.getListData();
    }


    @DeleteMapping("/{id}")
    public HttpStatus remove(@PathVariable long id) {
        eventService.remove(id);
        return HttpStatus.OK;
    }

}
