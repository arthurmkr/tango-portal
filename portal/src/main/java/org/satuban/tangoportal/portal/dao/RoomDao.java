package org.satuban.tangoportal.portal.dao;

import org.satuban.tangoportal.portal.dao.common.BaseDao;
import org.satuban.tangoportal.portal.model.entity.Room;

import java.util.List;

/**
 * Created by Artur on 11.06.2017.
 */
public interface RoomDao extends BaseDao<Room> {
    List<Room> findAllSchoolForUser(long userId);

    List<Room> findAllForSchool(long schoolId);
}
