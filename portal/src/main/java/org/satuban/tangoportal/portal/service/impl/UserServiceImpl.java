package org.satuban.tangoportal.portal.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.satuban.tangoportal.portal.dao.UserDao;
import org.satuban.tangoportal.portal.error.ErrorCode;
import org.satuban.tangoportal.portal.error.TPCityNotFoundException;
import org.satuban.tangoportal.portal.error.TPException;
import org.satuban.tangoportal.portal.model.dto.*;
import org.satuban.tangoportal.portal.model.entity.City;
import org.satuban.tangoportal.portal.model.entity.User;
import org.satuban.tangoportal.portal.model.filters.DjListFilter;
import org.satuban.tangoportal.portal.service.AddressService;
import org.satuban.tangoportal.portal.service.UserHolder;
import org.satuban.tangoportal.portal.service.UserService;
import org.satuban.tangoportal.portal.util.Generator;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Artur on 20.05.2017.
 */
@Slf4j
@Service
@Transactional
public class UserServiceImpl implements UserDetailsService, UserService {
    private static final int MIN_PASSWORD_LENGTH = 8;
    private final UserDao userDao;
    private final UserHolder userHolder;
    private final AddressService addressService;
    private final Generator generator;

    public UserServiceImpl(UserDao userDao, UserHolder userHolder, AddressService addressService, Generator generator) {
        this.userDao = userDao;
        this.userHolder = userHolder;
        this.addressService = addressService;
        this.generator = generator;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.findByUsername(username);

        if(user == null) {
            throw new UsernameNotFoundException("User[" + username + "] is not found");
        }

        return user;
    }

    @Override
    public Profile getProfile() {
        User user = userDao.getById(userHolder.getUserId());

        return entityToDto(user);
    }

    @Override
    public Profile save(Profile dto) throws IOException {
        User user = userDao.getById(userHolder.getUserId());
        Roles roles = dto.getRoles();

        user.setFirstName(dto.getFirstName());
        user.setSecondName(dto.getSecondName());
        user.setFbProfile(dto.getFbProfile());
        user.setVkProfile(dto.getVkProfile());
        user.setAvatar(dto.getAvatar());
        user.setDjNick(roles.isDj() ? dto.getDjNick() : null);
        user.setCityId(addressService.getOrCreateCity(dto.getCity().getName()).getId());

        user.setRoles(roles);

        User saved = userDao.save(user);

        return entityToDto(saved);
    }

    @Override
    public List<Profile> getReferrals() {
        return userDao.findReferralsByUser(userHolder.getUserId()).stream()
                .map(this::entityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<PublicProfile> getDjsByFilter(DjListFilter filter) {
        try {
            City city = addressService.getOrCreateCity(filter.getCity());
            return userDao.findDjsByCity(city != null ? city.getPid() : "");
        } catch (TPCityNotFoundException e) {
            return Collections.emptyList();
        }
    }

    @Override
    public List<Dj> getAllDjs() {
        return userDao.getAllDjs();
    }

    @Override
    public void changePassword(ChangePasswordRequest request) {
        User user = userDao.getById(userHolder.getUserId());

        if(!StringUtils.equals(request.getOldPassword(), user.getPassword())) {
            throw new TPException(ErrorCode.INCORRECT_OLD_PASSWORD);
        }

        if(!StringUtils.equals(request.getNewPassword(), request.getConfirmPassword())) {
            throw new TPException(ErrorCode.INCORRECT_CONFIRM_PASSWORD);
        }

        if(StringUtils.length(request.getNewPassword()) < MIN_PASSWORD_LENGTH) {
            throw new TPException(ErrorCode.PASSWORD_IS_TOO_SHORT);
        }

        user.setPassword(request.getNewPassword());
        userDao.save(user);
    }

    @Override
    public List<PublicProfile> getPhotographers() {
        return userDao.findPhotographers();
    }

    private Profile entityToDto(User user) {
        if (user == null) {
            return null;
        }

        Profile profile = new Profile();
        profile.setId(user.getId());
        profile.setFirstName(user.getFirstName());
        profile.setSecondName(user.getSecondName());
        profile.setFbProfile(user.getFbProfile());
        profile.setVkProfile(user.getVkProfile());
        profile.setCity(addressService.getCityDto(user.getCityId()));
        profile.setAvatar(user.getAvatar());
        profile.setDjNick(user.getDjNick());
        profile.setReferralLink(generator.generateReferralLink(user.getInviteToken()));

        Roles roles = new Roles();
        roles.setDj(user.isDj());
        roles.setOrganizer(user.isOrganizer());
        roles.setPhotographer(user.isPhotographer());
        roles.setSchoolLead(user.isSchoolLead());
        roles.setTeacher(user.isTeacher());
        roles.setSchoolAdmin(user.isSchoolAdmin());
        roles.setCouturier(user.isCouturier());
        roles.setAdmin(user.isAdmin());

        profile.setRoles(roles);

        return profile;
    }
}
