package org.satuban.tangoportal.portal.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.satuban.tangoportal.portal.model.enums.ActivityType;
import org.satuban.tangoportal.portal.model.enums.Currency;
import org.satuban.tangoportal.portal.model.json.DateDeserializer;
import org.satuban.tangoportal.portal.model.json.DateSerializer;
import org.satuban.tangoportal.portal.model.json.TimeDeserializer;
import org.satuban.tangoportal.portal.model.json.TimeSerializer;

import java.io.Serializable;
import java.time.DayOfWeek;
import java.util.Date;
import java.util.List;

/**
 * Created by Artur on 26.05.2017.
 */
@Data
public class ActivityDto implements Serializable {
    private static final long serialVersionUID = 6973331308091340540L;

    private Long id;
    private String name;

    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
    private Date startDate;

    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using =DateDeserializer.class)
    private Date endDate;
    private DayOfWeek dayOfWeek;

    @JsonDeserialize(using = TimeDeserializer.class)
    @JsonSerialize(using = TimeSerializer.class)
    private Date startTime;
    @JsonDeserialize(using = TimeDeserializer.class)
    @JsonSerialize(using = TimeSerializer.class)
    private Date endTime;

    private Long djId;
    private String djFullName;
    private Currency currency;
    private String photo;
    private int duration;
    private int cost;
    private Long eventId;
    private Long schoolId;
    private boolean regular;
    private Long grabbedGroupId;
    private AddressDto address;
    private ActivityType type;
    private List<Long> teachers;
    private Long roomId;
    private String description;
    private String vkUrl;
    private String fbUrl;
}
