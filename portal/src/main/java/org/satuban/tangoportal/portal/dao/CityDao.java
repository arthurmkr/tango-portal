package org.satuban.tangoportal.portal.dao;

import org.satuban.tangoportal.portal.dao.common.BaseDao;
import org.satuban.tangoportal.portal.model.dto.CityMarker;
import org.satuban.tangoportal.portal.model.dto.CitySimpleMarker;
import org.satuban.tangoportal.portal.model.entity.City;

import java.util.List;

/**
 * Created by Artur on 31.05.2017.
 */
public interface CityDao extends BaseDao<City> {
    City findByName(String cityName);

    List<CityMarker> getCityMarkers();

    List<CitySimpleMarker> getCityEventMarkers();

    List<CitySimpleMarker> getCityDjMarkers();

    List<CitySimpleMarker> getCitySchoolMarkers();
}
