package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.enums.PhotoSize;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Artur on 01.07.2017.
 */
public interface PhotoService {
    String savePhoto(PhotoSize size, MultipartFile file);

    String savePhoto(PhotoSize size, String urlPhoto);
}
