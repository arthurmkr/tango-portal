package org.satuban.tangoportal.portal.model.dto.external.facebook;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

/**
 * Created by Danilov on 25.09.2017.
 */
@Data
public class FacebookEventDto {
    private String id;
    private String description;
    private String name;
    @JsonProperty(value = "end_time")
    private Date endTime;
    @JsonProperty(value = "start_time")
    private Date startTime;
    private FacebookPlaceDto place;

}
