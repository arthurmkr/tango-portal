package org.satuban.tangoportal.portal.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.satuban.tangoportal.portal.model.json.DateSerializer;

import java.util.Date;

/**
 * Created by Artur on 11.08.2017.
 */
@Data
public class EventListItem {
    private long id;
    private String name;
    private int year;
    @JsonSerialize(using = DateSerializer.class)
    private Date startDate;
}
