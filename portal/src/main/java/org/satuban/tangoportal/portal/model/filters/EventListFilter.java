package org.satuban.tangoportal.portal.model.filters;

import lombok.Data;

/**
 * Created by Artur on 23.07.2017.
 */
@Data
public class EventListFilter {
    private String city;
}
