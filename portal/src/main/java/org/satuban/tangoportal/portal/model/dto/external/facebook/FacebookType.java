package org.satuban.tangoportal.portal.model.dto.external.facebook;

/**
 * Created by Danilov on 06.10.2017.
 */
public enum  FacebookType {
    EVENT, GROUP
}
