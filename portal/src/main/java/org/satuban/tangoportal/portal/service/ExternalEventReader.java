package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.dto.ActivityDto;
import org.satuban.tangoportal.portal.model.dto.external.ExternalEvent;
import org.satuban.tangoportal.portal.model.entity.GrabbedGroup;
import org.satuban.tangoportal.portal.model.enums.ActivityType;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by Danilov on 25.09.2017.
 */
public interface ExternalEventReader {
    default List<ActivityDto> updateEvents() {
        return convert(filter(process()));
    }

    default Map<GrabbedGroup, List<ExternalEvent>> filter(Map<GrabbedGroup, List<ExternalEvent>> events) {
        return events;
    }

    Map<GrabbedGroup, List<ExternalEvent>> process();

    List<ActivityDto> convert(Map<GrabbedGroup, List<ExternalEvent>> externalEvents);
}
