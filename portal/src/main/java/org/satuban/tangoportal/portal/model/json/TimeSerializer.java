package org.satuban.tangoportal.portal.model.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Artur on 11.07.2017.
 */
public class TimeSerializer extends StdSerializer<Date> {

    private static final long serialVersionUID = -4971278964083874706L;

    private SimpleDateFormat formatter  = new SimpleDateFormat("HH:mm");

    public TimeSerializer() {
        this(null);
    }

    public TimeSerializer(Class t) {
        super(t);
    }

    @Override
    public void serialize(Date value, JsonGenerator gen, SerializerProvider arg2)
            throws IOException {
        gen.writeString(formatter.format(value));
    }
}