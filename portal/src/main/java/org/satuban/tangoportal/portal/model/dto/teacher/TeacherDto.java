package org.satuban.tangoportal.portal.model.dto.teacher;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

/**
 * Created by Artur on 07.06.2017.
 */
@Data
public class TeacherDto implements Serializable {
    private static final long serialVersionUID = -813547171418539343L;

    private long id;
    private String firstName;
    private String secondName;
    private String resume;
    private String avatar;
}
