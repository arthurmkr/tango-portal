package org.satuban.tangoportal.portal.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.model.dto.*;
import org.satuban.tangoportal.portal.model.enums.MapType;
import org.satuban.tangoportal.portal.service.AddressService;
import org.satuban.tangoportal.portal.service.CabinetService;
import org.satuban.tangoportal.portal.service.UserService;
import org.satuban.tangoportal.portal.service.UtilService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Artur on 01.06.2017.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class DictionaryApiController {
    private final CabinetService facadeService;
    private final AddressService addressService;
    private final RestTemplate restTemplate;
    private final UtilService utilService;
    private final UserService userService;

    public DictionaryApiController(CabinetService facadeService, AddressService addressService, RestTemplate restTemplate, UtilService utilService, UserService userService) {
        this.facadeService = facadeService;
        this.addressService = addressService;
        this.restTemplate = restTemplate;
        this.utilService = utilService;
        this.userService = userService;
    }

    @GetMapping("/eventMarkers")
    public DailyEventMap getEventMarkers() {
        return facadeService.getDailyEventMap("Новосибирск");
    }

    @GetMapping("/tangoMap")
    public TangoMap getSchoolCityMarkers() {
        return addressService.getCityMarkers();
    }

    @GetMapping("/tangoMapByType")
    public TangoMap getTangMap(@RequestParam MapType type) {
        return addressService.getCityMarkersByType(type);
    }


    @GetMapping("/getLocation")
    public GeoFreeIpResponse getLocation(HttpServletRequest request) {
        log.info("Get location for IP [{}]", request.getRemoteAddr());
        GeoFreeIpResponse response = new GeoFreeIpResponse();
        try {
            response = restTemplate.getForObject("https://freegeoip.net/json/" + request.getRemoteAddr(),
                    GeoFreeIpResponse.class);

            log.info("response: {}", response);
        } catch (Exception e) {
            log.error("Error: ", e);
        }

        return response;
    }

//    @GetMapping("/getLocation")
//    public GeoFreeIpResponse getLocation(HttpServletRequest request) {
//        log.info("Get location for IP [{}]", request.getRemoteAddr());
//        GeoFreeIpResponse response = new GeoFreeIpResponse();
//        try {
//            response = restTemplate.getForObject("http://api.db-ip.com/v2/7ea1881bdb6a55a27ea1279719cb6f3a18823f38/" + request.getRemoteAddr(),
//                    GeoFreeIpResponse.class);
//
//            log.info("response: {}", response);
//        } catch (Exception e) {
//            log.error("Error: ", e);
//        }
//
//        return response;
//    }

    @GetMapping("/djs")
    public List<Dj> getAllDjs() {
        return userService.getAllDjs();
    }

    @PostMapping("/saveError")
    public HttpStatus saveError(@RequestBody JsErrorRequest error, HttpServletRequest request) {
        utilService.saveJsError(error, request);
        return HttpStatus.OK;
    }
}
