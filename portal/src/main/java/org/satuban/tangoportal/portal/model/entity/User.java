package org.satuban.tangoportal.portal.model.entity;

import lombok.Data;
import org.satuban.tangoportal.portal.dao.common.Column;
import org.satuban.tangoportal.portal.dao.common.Table;
import org.satuban.tangoportal.portal.model.dto.Roles;
import org.satuban.tangoportal.portal.model.enums.Role;
import org.satuban.tangoportal.portal.model.enums.UserStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Artur on 20.05.2017.
 */
@Table(name = "user_profile")
@Data
public class User implements UserDetails, BaseEntity {
    private static final long serialVersionUID = -1824698976916331722L;

    private Long id;
    @Column
    private String username;
    @Column
    private String password;
    @Column
    private String firstName;
    @Column
    private String secondName;
    @Column
    private String fbProfile;
    @Column
    private String vkProfile;
    @Column
    private Long cityId;
    @Column
    private String avatar;
    @Column
    private boolean dj;
    @Column
    private boolean teacher;
    @Column
    private boolean schoolLead;
    @Column
    private boolean organizer;
    @Column
    private boolean photographer;
    @Column
    private boolean schoolAdmin;
    @Column
    private boolean couturier;
    @Column
    private boolean admin;
    @Column
    private String inviteToken;
    @Column
    private UserStatus status = UserStatus.INACTIVE;
    @Column
    private long invitedBy;
    @Column
    private String djNick;
    @Column
    private String language = "ru";

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> list = new ArrayList<>();

        list.add(new SimpleGrantedAuthority(Role.USER.role()));

        return list;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return status != UserStatus.INACTIVE;
    }

    public void setRoles(Roles roles) {
        dj = roles.isDj();
        schoolLead = roles.isSchoolLead();
        photographer = roles.isPhotographer();
        organizer = roles.isOrganizer();
        teacher = roles.isTeacher();
        couturier = roles.isCouturier();
        schoolAdmin = roles.isSchoolAdmin();
        admin = roles.isAdmin();
    }

    public String getFullName() {
        return firstName + " " + secondName;
    }
}
