package org.satuban.tangoportal.portal.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.util.Files;
import org.satuban.tangoportal.portal.model.enums.PhotoSize;
import org.satuban.tangoportal.portal.service.PhotoService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

/**
 * Created by Artur on 01.07.2017.
 */
@Slf4j
@Service
public class PhotoServiceImpl implements PhotoService {
    public static final String EXTENSION = "jpg";
    private final String uploadFolder;
    private final String uploadImagesUrl;

    public PhotoServiceImpl(@Value("${uploadFolder}") String uploadFolder,
                            @Value("${uploadImagesUrl}") String uploadImagesUrl) {
        try {
            this.uploadFolder = Paths.get(new URL(uploadFolder).toURI()).toString() + "/";
        } catch (URISyntaxException | MalformedURLException e) {
            throw new RuntimeException("error"); // TODO error
        }
        this.uploadImagesUrl = uploadImagesUrl;
    }

    @Override
    public String savePhoto(PhotoSize size, MultipartFile file) {
        return file != null
                ? savePhoto(size, getOriginImage(file))
                : null;
    }

    @Override
    public String savePhoto(PhotoSize size, String urlPhoto) {
        return urlPhoto != null
                ? savePhoto(size, getOriginImage(urlPhoto))
                : null;
    }

    private String savePhoto(PhotoSize size, BufferedImage image) {
        if(image == null) {
            return null;
        }

        try{
            BufferedImage newResizeImage = resize(size, image);

            String fileName = RandomStringUtils.random(30, true, true) + "." + EXTENSION;
            String path = uploadFolder + fileName;

            log.info("uploadFolder: [{}], fileName: [{}]", uploadFolder, fileName);

            ImageIO.write(newResizeImage, EXTENSION, Files.newFile(path));

            return uploadImagesUrl + fileName;
        } catch (IOException ex) {
            log.error("Can't save file", ex);
            return null;
        }
    }

    private BufferedImage getOriginImage(MultipartFile file) {
        try {
            return ImageIO.read(file.getInputStream());
        } catch (IOException ex) {
            log.error("Can't read picture", ex);
            return null;
        }
    }

    private BufferedImage getOriginImage(String url) {
        try {
            return ImageIO.read(URI.create(url).toURL());
        } catch (IOException ex) {
            log.error("Can't read picture", ex);
            return null;
        }
    }

    private BufferedImage resize(PhotoSize size, BufferedImage origImage) throws IOException {
        float k = Math.min((float)size.getWidth() / (float)origImage.getWidth(), (float)size.getHeight() / (float)origImage.getHeight());
        int width = (int) ((float) origImage.getWidth() * k);
        int height = (int) ((float) origImage.getHeight() * k);

        BufferedImage im = new BufferedImage(size.getWidth(), size.getHeight(), BufferedImage.TYPE_INT_BGR);
        Graphics2D graphics = im.createGraphics();
        graphics.drawImage(origImage, (size.getWidth() - width) / 2, (size.getHeight() - height) / 2, width, height, null);

        graphics.dispose();

        return im;
    }
}
