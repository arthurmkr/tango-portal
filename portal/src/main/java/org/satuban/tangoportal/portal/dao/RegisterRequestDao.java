package org.satuban.tangoportal.portal.dao;

import org.satuban.tangoportal.portal.dao.common.BaseDao;
import org.satuban.tangoportal.portal.model.entity.RegisterRequest;

import java.util.List;

/**
 * Created by Artur on 19.07.2017.
 */
public interface RegisterRequestDao extends BaseDao<RegisterRequest> {
    List<RegisterRequest> getNewForSending();

    List<RegisterRequest> getConfirmedForSending();

    void markAsSent(List<RegisterRequest> requests);

    RegisterRequest getByToken(String token);

    void rejectRequest(long id);

    RegisterRequest getByUser(long userId);

    List<RegisterRequest> getApprovedForSending();
}
