package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.dto.teacher.TeacherDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * Created by Artur on 07.06.2017.
 */
public interface TeacherService {
    void remove(long id);

    List<TeacherDto> getAll();

    TeacherDto save(TeacherDto dto);

    List<TeacherDto> getAllBySchool(long schoolId);
}
