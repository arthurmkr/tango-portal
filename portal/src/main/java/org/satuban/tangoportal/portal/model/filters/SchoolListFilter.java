package org.satuban.tangoportal.portal.model.filters;

import lombok.Data;

/**
 * Created by Artur on 10.07.2017.
 */
@Data
public class SchoolListFilter {
    private String city;
}
