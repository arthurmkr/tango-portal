package org.satuban.tangoportal.portal.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.dao.SchoolDao;
import org.satuban.tangoportal.portal.dao.TeacherDao;
import org.satuban.tangoportal.portal.model.dto.teacher.TeacherDto;
import org.satuban.tangoportal.portal.model.entity.School;
import org.satuban.tangoportal.portal.model.entity.Teacher;
import org.satuban.tangoportal.portal.service.PhotoService;
import org.satuban.tangoportal.portal.service.TeacherService;
import org.satuban.tangoportal.portal.service.UserHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Artur on 07.06.2017.
 */
@Slf4j
@Service
@Transactional
public class TeacherServiceImpl implements TeacherService {
    private final TeacherDao teacherDao;
    private final SchoolDao schoolDao;
    private final PhotoService photoService;
    private final UserHolder userHolder;

    public TeacherServiceImpl(TeacherDao teacherDao, SchoolDao schoolDao, UserHolder userHolder, PhotoService photoService) {
        this.userHolder = userHolder;
        this.teacherDao = teacherDao;
        this.schoolDao = schoolDao;
        this.photoService = photoService;
    }

    private Teacher dtoToEntity(TeacherDto dto) {
        School school = schoolDao.findByUserId(userHolder.getUserId());

        if (school == null) {
            throw new RuntimeException("Error");
        }

        Teacher teacher = teacherDao.getOrCreate(dto.getId());

        if (teacher.getSchoolId() != 0 && teacher.getSchoolId() != school.getId()) {
            throw new RuntimeException("error");
        }

        teacher.setSchoolId(school.getId());
        teacher.setFirstName(dto.getFirstName());
        teacher.setSecondName(dto.getSecondName());
        teacher.setResume(dto.getResume());
        teacher.setUserId(userHolder.getUserId());
        teacher.setAvatar(dto.getAvatar());

        return teacher;
    }

    private TeacherDto entityToDto(Teacher entity) {
        TeacherDto dto = new TeacherDto();

        dto.setId(entity.getId());
        dto.setFirstName(entity.getFirstName());
        dto.setSecondName(entity.getSecondName());
        dto.setResume(entity.getResume());
        dto.setAvatar(entity.getAvatar());

        return dto;
    }

    @Override
    public void remove(long id) {
        teacherDao.removeById(id);
    }

    @Override
    public List<TeacherDto> getAll() {
        School school = schoolDao.findByUserId(userHolder.getUserId());

        if (school == null) {
            return Collections.emptyList();
        }

        return getAllBySchool(school.getId());
    }

    @Override
    public TeacherDto save(TeacherDto dto) {
        Teacher entity = dtoToEntity(dto);

        Teacher saved = teacherDao.save(entity);
        return entityToDto(saved);
    }

    @Override
    public List<TeacherDto> getAllBySchool(long schoolId) {
        return teacherDao.getBySchool(schoolId)
                .stream()
                .map(this::entityToDto)
                .collect(Collectors.toList());
    }
}
