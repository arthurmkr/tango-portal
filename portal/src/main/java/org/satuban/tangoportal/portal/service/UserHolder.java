package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.dto.Profile;
import org.satuban.tangoportal.portal.model.entity.School;
import org.satuban.tangoportal.portal.model.entity.User;
import org.springframework.web.context.annotation.SessionScope;

/**
 * Created by Artur on 08.06.2017.
 */
@SessionScope
public interface UserHolder {
    User get();

    long getUserId();
}
