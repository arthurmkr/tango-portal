package org.satuban.tangoportal.portal.web.controller.api.priv;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.model.dto.ActivityDto;
import org.satuban.tangoportal.portal.service.ActivityService;
import org.satuban.tangoportal.portal.web.WebConstants;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Artur on 06.08.2017.
 */
@Slf4j
@RestController
@RequestMapping(WebConstants.PRIVATE_API + "/milonga")
public class MilongaPrivateApiController {
    private final ActivityService activityService;

    public MilongaPrivateApiController(ActivityService activityService) {
        this.activityService = activityService;
    }

    @GetMapping
    public List<ActivityDto> getPlannedMilongas() {
        return activityService.getMilongasForUser();
    }

    @PostMapping
    public ActivityDto save(@RequestBody ActivityDto dto) {
        log.info("Milonga: {}", dto);
        return activityService.saveMilonga(dto);
    }
}
