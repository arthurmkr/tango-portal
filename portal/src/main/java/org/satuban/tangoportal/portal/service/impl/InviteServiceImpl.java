package org.satuban.tangoportal.portal.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.satuban.tangoportal.portal.dao.RegisterRequestDao;
import org.satuban.tangoportal.portal.dao.UserDao;
import org.satuban.tangoportal.portal.error.ErrorCode;
import org.satuban.tangoportal.portal.error.TPException;
import org.satuban.tangoportal.portal.error.TPObjectNotFoundException;
import org.satuban.tangoportal.portal.model.dto.Profile;
import org.satuban.tangoportal.portal.model.dto.RegistrationConfirmResponse;
import org.satuban.tangoportal.portal.model.entity.RegisterRequest;
import org.satuban.tangoportal.portal.model.entity.User;
import org.satuban.tangoportal.portal.model.enums.RegisterRequestStatus;
import org.satuban.tangoportal.portal.model.enums.UserStatus;
import org.satuban.tangoportal.portal.service.InviteService;
import org.satuban.tangoportal.portal.service.UserHolder;
import org.satuban.tangoportal.portal.util.Generator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Locale;

/**
 * Created by Artur on 18.07.2017.
 */
@Slf4j
@Service
@Transactional
public class InviteServiceImpl implements InviteService {
    public static final int REFERRAL_TOKEN_LENGTH = 50;
    private static final int CONFIRM_TOKEN_LENGTH = 50;
    private static final int USER_PASSWORD_LENGTH = 15;
    private final UserDao userDao;
    private final RegisterRequestDao registerRequestDao;
    private final UserHolder userHolder;
    private final Generator generator;

    public InviteServiceImpl(UserDao userDao,
                             RegisterRequestDao registerRequestDao,
                             UserHolder userHolder,
                             Generator generator) {
        this.userDao = userDao;
        this.registerRequestDao = registerRequestDao;
        this.userHolder = userHolder;
        this.generator = generator;
    }

    @Override
    public boolean isValidToken(String token) {
        User user = userDao.findByInviteToken(token);

        if (user == null) {
            throw new TPObjectNotFoundException();
        }
        return true;
    }

    @Override
    public void register(String token, Profile profile, Locale locale) {
        validate(profile);

        User referrer = userDao.findByInviteToken(token);

        if (referrer == null || referrer.getStatus() != UserStatus.ACTIVE) {
            throw new TPException(ErrorCode.INVALID_INVITE_TOKEN);
        }

        // создать пользователя
        User user = new User();
        user.setFirstName(profile.getFirstName());
        user.setSecondName(profile.getSecondName());
        user.setFbProfile(profile.getFbProfile());
        user.setVkProfile(profile.getVkProfile());
        user.setStatus(UserStatus.INVITED);
        user.setUsername(profile.getEmail());
        user.setInvitedBy(referrer.getId());
        user.setLanguage(locale.getLanguage());

        user.setRoles(profile.getRoles());

        User savedUser = userDao.save(user);

        // создать запрос регистрации
        RegisterRequest request = new RegisterRequest();
        request.setUserId(savedUser.getId());
        request.setCreated(new Date());
        // проверить уникальность токена
        request.setToken(RandomStringUtils.randomAlphanumeric(CONFIRM_TOKEN_LENGTH));
        request.setStatus(RegisterRequestStatus.NEW);

        registerRequestDao.save(request);
    }

    @Override
    public RegistrationConfirmResponse confirm(String token) {
        RegisterRequest request = registerRequestDao.getByToken(token);

        if (request == null) {
            throw new TPObjectNotFoundException();
        }

        RegistrationConfirmResponse response = new RegistrationConfirmResponse();
        if (request.getStatus() == RegisterRequestStatus.CONFIRMED ||
                request.getStatus() == RegisterRequestStatus.APPROVED) {
            response.setAlready(true);
            return response;
        }

        User referrer = userDao.getReferrerByUser(request.getUserId());

        if (referrer == null || referrer.getStatus() != UserStatus.ACTIVE) {
            log.info("Referrer for user[{}] is null or not active", request.getUserId());
            response.setUserInvalid(true);
            return response;
        }

        log.info("RegisterRequest[{}] is confirmed", request.getId());
        request.setStatus(RegisterRequestStatus.CONFIRMED);
        request.setSent(null);

        registerRequestDao.save(request);


        response.setUser(referrer);
        return response;
    }

    @Override
    public void rejectRequest(long id) {
        registerRequestDao.rejectRequest(id);
    }

    @Override
    public void approveRequest(long userId) {
        RegisterRequest request = registerRequestDao.getByUser(userId);
        request.setStatus(RegisterRequestStatus.APPROVED);
        request.setSent(null);
        registerRequestDao.save(request);

        User user = userDao.getById(request.getUserId());
        user.setPassword(RandomStringUtils.randomAlphanumeric(USER_PASSWORD_LENGTH));
        user.setStatus(UserStatus.ACTIVE);
        userDao.save(user);
    }

    @Override
    public String getReferralLink() {
        User user = userDao.getById(userHolder.getUserId());

        if (StringUtils.isBlank(user.getInviteToken())) {
            // TODO проверить уникальность токена
            user.setInviteToken(RandomStringUtils.randomAlphanumeric(REFERRAL_TOKEN_LENGTH));

            userDao.save(user);
        }

        return generator.generateReferralLink(user.getInviteToken());
    }

    private void validate(Profile profile) {
        if (StringUtils.isBlank(profile.getEmail()) ||
                StringUtils.isBlank(profile.getFirstName()) ||
                StringUtils.isBlank(profile.getSecondName())) {
            throw new TPException(ErrorCode.INVALID_FORM_PARAM);
        }

        User user = userDao.findByUsername(profile.getEmail());
        if(user != null) {
            throw new TPException(ErrorCode.EMAIL_IS_ALREADY_USED);
        }
    }
}
