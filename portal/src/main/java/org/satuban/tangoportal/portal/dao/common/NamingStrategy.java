package org.satuban.tangoportal.portal.dao.common;

import java.util.regex.Pattern;

/**
 * Created by Artur on 28.04.2017.
 */
public enum NamingStrategy {
    DEFAULT {
        private Pattern defaultSplitter = Pattern.compile("(?=\\p{Lu})");

        @Override
        public String convert(String value) {
            String[] parts = defaultSplitter.split(value);

            for (int i = 0; i < parts.length; i++) {
                parts[i] = parts[i].toLowerCase();
            }

            return String.join("_", parts);
        }
    };


    public abstract String convert(String value);
}
