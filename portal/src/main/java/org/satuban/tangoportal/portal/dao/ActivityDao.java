package org.satuban.tangoportal.portal.dao;

import org.satuban.tangoportal.portal.dao.common.BaseDao;
import org.satuban.tangoportal.portal.model.entity.Activity;

import java.util.Date;
import java.util.List;

/**
 * Created by Artur on 26.05.2017.
 */
public interface ActivityDao extends BaseDao<Activity> {
    List<Activity> findAllByRange(Date from, Date to, String city);
    List<Activity> findAllForSchool(long schoolId, Date from, Date to);

    List<Activity> findMilongasForUser(long userId);

    List<Activity> findMilongasByEvent(long eventId);
}
