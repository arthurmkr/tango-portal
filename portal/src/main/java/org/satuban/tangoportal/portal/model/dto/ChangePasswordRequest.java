package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;

/**
 * Created by Artur on 10.08.2017.
 */
@Data
public class ChangePasswordRequest {
    private String oldPassword;
    private String newPassword;
    private String confirmPassword;
}
