package org.satuban.tangoportal.portal.dao.impl;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.dao.common.BaseDao;
import org.satuban.tangoportal.portal.dao.common.Column;
import org.satuban.tangoportal.portal.dao.common.NamingStrategy;
import org.satuban.tangoportal.portal.dao.common.Table;
import org.satuban.tangoportal.portal.model.entity.BaseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.util.CollectionUtils;

import javax.validation.Valid;
import java.lang.reflect.Field;
import java.util.*;

import static java.util.stream.Collectors.toList;

/**
 * Created by Artur on 20.05.2017.
 */
@Slf4j
abstract class AbstractDao<T extends BaseEntity> implements BaseDao<T> {
    final RowMapper<T> rowMapper;
    final NamedParameterJdbcTemplate template;
    private final Class<T> clazz;
    private Set<Field> enumOrdered;
    private Map<String, Field> entityFields;
    private String findAllSql;
    private String findByIdSql;
    private String deleteByIdSql;
    private String persistSql;
    private String updateSql;
    private String findByIdsSql;
    private String tableName;


    AbstractDao(NamedParameterJdbcTemplate template, Class<T> clazz) {
        this.template = template;
        this.rowMapper = new BeanPropertyRowMapper<T>(clazz);
        this.clazz = clazz;

        createQueries(clazz);
    }

    private void createQueries(Class<T> clazz) {
        Table[] tableAnnotations = clazz.getAnnotationsByType(Table.class);
        if (tableAnnotations.length == 0) {
            throw new IllegalArgumentException("Not found annotation Table for class [" + clazz + "]");
        }

        Table table = tableAnnotations[0];
        tableName = table.name().equals("") ? createName(clazz.getSimpleName()) : table.name();

        createFieldsCollection(clazz);

        findAllSql = "SELECT * FROM " + tableName;
        findByIdSql = "SELECT * FROM " + tableName + " WHERE id = :id";
        deleteByIdSql = "DELETE FROM " + tableName + " WHERE id = :id";
        findByIdsSql = "SELECT * FROM " + tableName + " WHERE id in (:ids)";
        persistSql = createPersistSql(tableName);
        updateSql = createUpdateSql(tableName);
    }

    private void createFieldsCollection(Class<T> clazz) {
        List<Field> fields = getFields(clazz);

        entityFields = new HashMap<>(fields.size());
        enumOrdered = new HashSet<>(fields.size());

        for (Field field : fields) {
            Column[] columnsAnnotations = field.getAnnotationsByType(Column.class);
            if (columnsAnnotations.length > 0) {
                Column annotation = columnsAnnotations[0];
                String columnName = annotation.name();
                if (columnName.equals("")) {
                    columnName = createName(field.getName());
                }

                field.setAccessible(true);
                entityFields.put(columnName, field);

                if (annotation.enumOrder()) {
                    enumOrdered.add(field);
                }
            }
        }
    }

    private String createName(String name) {
        return NamingStrategy.DEFAULT.convert(name);
    }

    @SuppressWarnings("unchecked")
    private List<Field> getFields(Class clazz) {
        if (clazz.isAssignableFrom(Object.class)) {
            return Collections.EMPTY_LIST;
        }

        List<Field> res = new ArrayList<>(CollectionUtils.arrayToList(clazz.getDeclaredFields()));

        Class superclass = clazz.getSuperclass();
        res.addAll(getFields(superclass));

        return res;
    }

    protected T first(List<T> resultList) {
        return resultList.isEmpty() ? null : resultList.get(0);
    }

    private String createUpdateSql(String tableName) {
        Set<String> fieldNames = entityFields.keySet();

        List<String> sqlParamList = fieldNames.stream().map(v -> v + " = :" + v).collect(toList());
        String sqlParams = String.join(", \n", sqlParamList);

        return "UPDATE " + tableName + " SET " + sqlParams + " WHERE id = :id";
    }

    private String createPersistSql(String tableName) {
        Set<String> fieldNames = entityFields.keySet();

        String paramNames = String.join(", ", fieldNames);
        String valueNames = String.join(", ", fieldNames.stream().map(s -> ":" + s).collect(toList()));

        return "INSERT INTO " + tableName + "(" + paramNames + ") VALUES(" + valueNames + ")";
    }

    @Override
    public List<T> getAll() {
        return template.query(findAllSql, rowMapper);
    }

    @Override
    public T findById(Long id) {
        if (id == null) {
            return null;
        }

        List<T> list = template.query(findByIdSql, Collections.singletonMap("id", id), rowMapper);
        return list.isEmpty() ? null : list.get(0);
    }

    @Override
    public List<T> findByIds(Collection<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.EMPTY_LIST;
        }

        return template.query(findByIdsSql, Collections.singletonMap("ids", ids), rowMapper);
    }

    @Override
    public boolean removeById(Long id) {
        return template.update(deleteByIdSql, Collections.singletonMap("id", id)) > 0;
    }

    @Override
    public T save(@Valid T entity) {
        if (entity != null) {
            return entity.getId() != null ? update(entity) : persist(entity);
        }

        return null;
    }

    @Override
    public T getById(long id) {
        T res = findById(id);
        if (res == null) {
            throw new RuntimeException("Object not found ");
        }
        return res;
    }

    @Override
    public T getOrCreate(Long id) {
        T existed = findById(id);
        try {
            return existed == null ? clazz.newInstance() : existed;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private T persist(T entity) {
        GeneratedKeyHolder resultHolder = new GeneratedKeyHolder();
        Map<String, Object> params = createParamsMap(entity);
        template.update(persistSql, new MapSqlParameterSource(params), resultHolder);

        long id;
        if (resultHolder.getKeys().size() > 1) {
            id = (long) resultHolder.getKeys().get("id");
        } else {
            id = resultHolder.getKey().longValue();
        }
        entity.setId(id);
        return entity;
    }

    private Map<String, Object> createParamsMap(T entity) {
        Map<String, Object> values = new HashMap<>(entityFields.size());

        for (Map.Entry<String, Field> fieldEntry : entityFields.entrySet()) {
            try {
                values.put(fieldEntry.getKey(), convertValue(entity, fieldEntry.getValue()));
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }

        return values;
    }

    private T update(T entity) {
        Map<String, Object> params = createParamsMap(entity);
        params.put("id", entity.getId());
        template.update(updateSql, params);

        return entity;
    }


    private Object convertValue(T entity, Field field) throws IllegalAccessException {
        Object val = field.get(entity);
        if (val != null) {
            Class<?> type = val.getClass();

            if (type.isEnum()) {
                Enum enumVal = (Enum) val;
                return enumOrdered.contains(field) ? enumVal.ordinal() : enumVal.name();
            } else {
                return val;
            }
        }

        return null;
    }

    @Override
    public String getTableName() {
        return tableName;
    }
}
