package org.satuban.tangoportal.portal.util;

import org.apache.commons.lang3.time.DateUtils;

import java.time.DayOfWeek;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Artur on 01.08.2017.
 */
public class Utils {
    private static final long MILLISECONDS_PER_MINUTE = 60 * 1000;
    private static final long MILLISECONDS_PER_DAY = 24 * 60 * MILLISECONDS_PER_MINUTE;

    public static int convertToMondayWeek(int day) {
        return (day + 5) % 7;
    }

    public static int convertToMondayWeek(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return convertToMondayWeek(calendar.get(Calendar.DAY_OF_WEEK));
    }

    public static DayOfWeek convertToMondayWeekEnum(Date date) {
        return DayOfWeek.of(convertToMondayWeek(date) + 1);
    }

    public static int calcDurationByDateInterval(Date startTime, Date endTime) {
        if(startTime == null || endTime == null) {
            return 0;
        }

        long start = startTime.getTime();
        long end = endTime.getTime();

        if (start > end) {
            end += MILLISECONDS_PER_DAY;
        }

        return (int) ((end - start) / MILLISECONDS_PER_MINUTE);
    }

    public static Date createEndTime(Date startTime, int duration) {
        return duration == 0 ? null : DateUtils.addMinutes(startTime, duration);
    }
}
