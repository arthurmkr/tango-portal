package org.satuban.tangoportal.portal.model.enums;

/**
 * Created by Artur on 07.07.2017.
 */
public enum PhotoSize {
    AVATAR(700, PhotoRatio.SQUARE),
    SCHOOL_PHOTO(2000, PhotoRatio.RATIO_3_TO_1),
    EVENT_PHOTO(2000, PhotoRatio.RATIO_16_TO_9),
    MILONGA_PHOTO(1000, PhotoRatio.RATIO_16_TO_9);

    private final int width;
    private final int height;

    PhotoSize(int width, PhotoRatio ratio) {
        this.width = width;
        this.height = (int) (width / ratio.getRatio());
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
