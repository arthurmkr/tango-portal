package org.satuban.tangoportal.portal.dao;

import org.satuban.tangoportal.portal.dao.common.BaseDao;
import org.satuban.tangoportal.portal.model.entity.ExternalToken;
import org.satuban.tangoportal.portal.model.enums.TokenType;

/**
 * Created by Danilov on 28.09.2017.
 */
public interface ExternalTokenDao extends BaseDao<ExternalToken> {
    ExternalToken findByUserAndType(long userId, TokenType facebook);
    ExternalToken findAnyAdminTokenByType(TokenType tokenType);
}
