package org.satuban.tangoportal.portal.dao.common;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Artur on 26.04.2017.
 */
@Target({TYPE})
@Retention(RUNTIME)
public @interface Table {
    String name() default "";
}
