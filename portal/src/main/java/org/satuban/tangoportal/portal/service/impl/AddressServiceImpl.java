package org.satuban.tangoportal.portal.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.satuban.tangoportal.portal.dao.AddressDao;
import org.satuban.tangoportal.portal.dao.CityDao;
import org.satuban.tangoportal.portal.error.TPCityNotFoundException;
import org.satuban.tangoportal.portal.model.dto.*;
import org.satuban.tangoportal.portal.model.entity.Address;
import org.satuban.tangoportal.portal.model.entity.City;
import org.satuban.tangoportal.portal.model.enums.MapType;
import org.satuban.tangoportal.portal.service.AddressService;
import org.satuban.tangoportal.portal.service.GoogleApi;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Artur on 06.06.2017.
 */
@Slf4j
@Service
public class AddressServiceImpl implements AddressService {
    private static final String FAKE_PID = "0";
    private final AddressDao addressDao;
    private final CityDao cityDao;
    private final GoogleApi googleApi;
    private final Map<Long, AddressDto> addressCache = new ConcurrentHashMap<>();

    public AddressServiceImpl(AddressDao addressDao, CityDao cityDao, GoogleApi googleApi) {
        this.addressDao = addressDao;
        this.cityDao = cityDao;
        this.googleApi = googleApi;
    }

    @Override
    @Transactional
    public Address getOrCreateAddress(AddressDto param) {
        if (param == null || StringUtils.isEmpty(param.getValue())) {
            return null;
        }

        Address address = addressDao.findByValue(param.getValue());

        if (address == null) {
            address = new Address();

            if (param.getCity() == null) {
                throw new RuntimeException(); // TODO error
            }

            City city = getOrCreateCity(param.getCity().getName());

            if (city == null) {
                throw new RuntimeException(); //TODO error
            }
            address.setValue(param.getValue());
            address.setCityId(city.getId());
            address.setLat(param.getLat());
            address.setLng(param.getLng());
            address.setPid(param.getPid());

            address = addressDao.save(address);
        }
        return address;
    }

    @Override
    public Address obtainAddressFromGoogle(float lat, float lng) {
        return null;
    }

    @Override
    public CityDto getCityDto(Long id) {
        City city = cityDao.findById(id);

        CityDto dto = null;
        if (city != null) {
            dto = new CityDto();

            dto.setId(city.getId());
            dto.setName(city.getName());
        }

        return dto;
    }

    @Override
    public AddressDto getById(Long id) {
        return addressCache.computeIfAbsent(id, aLong -> toAddressDto(addressDao.findById(id)));
    }

    @Override
    public City getOrCreateCity(String cityName) {
        if (StringUtils.isBlank(cityName)) {
            return null;
        }
        City city = cityDao.findByName(cityName);

        if (city == null) {
            city = new City();

            GeoCodeResponse.Result result = googleApi.getPlaceForCity(cityName);
            log.info("City result: {}", result);

            if (result == null) {
                throw new TPCityNotFoundException();
            }

            city.setName(cityName);
            city.setPid(result.getPlaceId());
            city.setLat(result.getGeometry().getLocation().getLat());
            city.setLng(result.getGeometry().getLocation().getLng());

            city = cityDao.save(city);
        }

        return city;
    }

    @Override
    public TangoMap<CityMarker> getCityMarkers() {
        TangoMap<CityMarker> tangoMap = new TangoMap<>();

        tangoMap.setMarkers(cityDao.getCityMarkers());
        tangoMap.setCenter(getCenter(tangoMap.getMarkers()));

        return tangoMap;
    }

    @Override
    public TangoMap<CitySimpleMarker> getCityMarkersByType(MapType type) {
        TangoMap<CitySimpleMarker> tangoMap = new TangoMap<>();

        List<CitySimpleMarker> markers = new ArrayList<>();
        switch (type) {
            case EVENT:
                markers = cityDao.getCityEventMarkers();
                break;
            case DJ:
                markers = cityDao.getCityDjMarkers();
                break;
            case SCHOOL:
                markers = cityDao.getCitySchoolMarkers();
                break;
        }

        tangoMap.setMarkers(markers);
        tangoMap.setCenter(getCenter(tangoMap.getMarkers()));

        return tangoMap;
    }

    public AddressDto toAddressDto(Address address) {
        return toAddressDto(address, true);
    }

    public AddressDto toAddressDto(Address address, boolean includeCity) {
        if (address == null) {
            return null;
        }

        AddressDto dto = new AddressDto();

        dto.setId(address.getId());
        dto.setValue(address.getValue());
        dto.setPid(address.getPid());
        dto.setLat(address.getLat());
        dto.setLng(address.getLng());

        if (includeCity) {
            City city = cityDao.findById(address.getCityId());
            dto.setCity(toCityDto(city));
        }
        return dto;
    }

    private CityDto toCityDto(City city) {
        if (city == null) {
            return null;
        }

        CityDto dto = new CityDto();
        dto.setId(city.getId());
        dto.setName(city.getName());
        return dto;
    }

    private Coord getCenter(List<? extends CitySimpleMarker> markers) {
        if (CollectionUtils.isEmpty(markers)) {
            return new Coord();
        }

        double count = markers.size();

        double X = 0.0;
        double Y = 0.0;
        double Z = 0.0;

        for (CitySimpleMarker marker : markers) {
            double lat = marker.getLat() * Math.PI / 180;
            double lon = marker.getLng() * Math.PI / 180;

            double a = Math.cos(lat) * Math.cos(lon);
            double b = Math.cos(lat) * Math.sin(lon);
            double c = Math.sin(lat);

            X += a;
            Y += b;
            Z += c;
        }

        X /= count;
        Y /= count;
        Z /= count;

        double lon = Math.atan2(Y, X);
        double hyp = Math.sqrt(X * X + Y * Y);
        double lat = Math.atan2(Z, hyp);

        double newX = (lat * 180 / Math.PI);
        double newY = (lon * 180 / Math.PI);

        return new Coord(newX, newY);
    }
}
