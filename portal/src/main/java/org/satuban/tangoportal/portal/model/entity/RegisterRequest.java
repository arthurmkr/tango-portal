package org.satuban.tangoportal.portal.model.entity;

import lombok.Data;
import org.satuban.tangoportal.portal.dao.common.Column;
import org.satuban.tangoportal.portal.dao.common.Table;
import org.satuban.tangoportal.portal.model.enums.RegisterRequestStatus;

import java.util.Date;

/**
 * Created by Artur on 19.07.2017.
 */
@Data
@Table
public class RegisterRequest implements BaseEntity {
    private static final long serialVersionUID = -5926993838249467116L;

    private Long id;
    @Column
    private String token;
    @Column
    private long userId;
    @Column
    private RegisterRequestStatus status;
    @Column
    private Date created;
    @Column
    private Date sent;
}
