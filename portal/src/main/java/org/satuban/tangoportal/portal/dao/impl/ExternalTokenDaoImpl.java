package org.satuban.tangoportal.portal.dao.impl;

import org.satuban.tangoportal.portal.dao.ExternalTokenDao;
import org.satuban.tangoportal.portal.model.entity.ExternalToken;
import org.satuban.tangoportal.portal.model.enums.TokenType;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Danilov on 28.09.2017.
 */
@Repository
public class ExternalTokenDaoImpl extends AbstractDao<ExternalToken> implements ExternalTokenDao {
    ExternalTokenDaoImpl(NamedParameterJdbcTemplate template) {
        super(template, ExternalToken.class);
    }


    @Override
    public ExternalToken findByUserAndType(long userId, TokenType tokenType) {
        Map<String, Object> values = new HashMap<>();
        values.put("userId", userId);
        values.put("tokenType", tokenType.toString());

        return template.query("SELECT * FROM external_token WHERE user_id = :userId AND type = :tokenType", values, rowMapper)
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public ExternalToken findAnyAdminTokenByType(TokenType tokenType) {
        Map<String, Object> values = new HashMap<>();
        values.put("tokenType", tokenType.toString());

        return template.query("SELECT * FROM external_token " +
                "WHERE user_id IN(SELECT id FROM user_profile WHERE admin = TRUE) " +
                "AND type = :tokenType AND dead_time > now() LIMIT 1", values, rowMapper)
                .stream()
                .findFirst()
                .orElse(null);
    }
}
