package org.satuban.tangoportal.portal.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.dao.ExternalTokenDao;
import org.satuban.tangoportal.portal.dao.GrabbedActivityDao;
import org.satuban.tangoportal.portal.dao.GrabbedGroupDao;
import org.satuban.tangoportal.portal.model.dto.ActivityDto;
import org.satuban.tangoportal.portal.model.dto.AddressDto;
import org.satuban.tangoportal.portal.model.dto.CityDto;
import org.satuban.tangoportal.portal.model.dto.external.ExternalEvent;
import org.satuban.tangoportal.portal.model.dto.external.facebook.FacebookLocationDto;
import org.satuban.tangoportal.portal.model.dto.external.facebook.data.FacebookEventDataPage;
import org.satuban.tangoportal.portal.model.dto.external.facebook.FacebookEventDto;
import org.satuban.tangoportal.portal.model.dto.external.facebook.FacebookPlaceDto;
import org.satuban.tangoportal.portal.model.entity.GrabbedActivity;
import org.satuban.tangoportal.portal.model.entity.GrabbedGroup;
import org.satuban.tangoportal.portal.model.enums.ActivityType;
import org.satuban.tangoportal.portal.model.enums.TokenType;
import org.satuban.tangoportal.portal.service.ActivityService;
import org.satuban.tangoportal.portal.service.AddressService;
import org.satuban.tangoportal.portal.service.ExternalEventReader;
import org.satuban.tangoportal.portal.util.Utils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import static org.satuban.tangoportal.portal.service.FacebookService.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Danilov on 26.09.2017.
 */
@Service
@Slf4j
public class FacebookEventReader implements ExternalEventReader {
    private final ExternalTokenDao externalTokenDao;
    private final GrabbedGroupDao grabbedGroupDao;
    private final GrabbedActivityDao grabbedActivityDao;
    private final RestTemplate restTemplate;
    private final ActivityService activityService;
    private final AddressService addressService;

    public FacebookEventReader(ExternalTokenDao externalTokenDao,
                               GrabbedGroupDao grabbedGroupDao,
                               GrabbedActivityDao grabbedActivityDao,
                               ActivityService activityService,
                               AddressService addressService) {
        this.externalTokenDao = externalTokenDao;
        this.grabbedGroupDao = grabbedGroupDao;
        this.grabbedActivityDao = grabbedActivityDao;
        this.activityService = activityService;
        this.addressService = addressService;
        this.restTemplate = new RestTemplate();
    }

    @Override
    public Map<GrabbedGroup, List<ExternalEvent>> process() {
        if(externalTokenDao.findAnyAdminTokenByType(TokenType.FACEBOOK) == null) {
            throw new RuntimeException("Need token!");
        }

        Map<GrabbedGroup, List<ExternalEvent>> eventsMap = new HashMap<>();

        List<GrabbedGroup> allGroup = grabbedGroupDao.getAllEnabled();
        for (GrabbedGroup grabbedGroup : allGroup) {
            String groupUrl = groupIdToGraphUrl(grabbedGroup.getFbId());

            try {
                ResponseEntity<FacebookEventDataPage> dataResponse = restTemplate.getForEntity(groupUrl, FacebookEventDataPage.class);
                FacebookEventDataPage facebookEventData = dataResponse.getBody();

                log.info("count of events from group [{}] before filter: {}", grabbedGroup.getFbId(), facebookEventData.getData().size());
                List<ExternalEvent> externalEvents = facebookEventData.getData()
                        .stream()
                        .filter(e -> filterOne(grabbedGroup, e))
                        .map(this::convertFacebookToExternalEvent)
                        .peek(e -> updateActivityType(grabbedGroup, e))
                        .filter(e -> e.getActivityType() != null)
                        .collect(Collectors.toList());

                log.info("In group: {} events size: {}", grabbedGroup.getFbId(), externalEvents.size());

                eventsMap.put(grabbedGroup, externalEvents);
            } catch (RestClientException ex) {
                log.error("Can't pull events from group: {}", grabbedGroup.getFbId(), ex);
                throw new RuntimeException("Can't pull events from group: " + grabbedGroup.getFbId(), ex);
            }
        }

        return eventsMap;
    }


    private void updateActivityType(GrabbedGroup grabbedGroup, ExternalEvent event) {
        String eventName = Optional.ofNullable(event.getName())
                .orElse("").toLowerCase();

        if(eventName.matches(".*(\\s|^)(milonga|милонга)(\\s|$).*")
                || (grabbedGroup.getRegexpFilter() != null && eventName.matches(grabbedGroup.getRegexpFilter()))) {
            event.setActivityType(ActivityType.MILONGA);
        } else {
            log.info("Event[{}] name[{}] is not matched to activity type", event.getId());
        }
    }

    private boolean filterOne(GrabbedGroup group, FacebookEventDto event) {
        Date now = new Date();

        Date startTime = event.getStartTime();
        if(startTime == null) {
            log.info("Event[{}] without start date", event.getId());
            return false;
        }

        if (StringUtils.isEmpty(event.getName())) {
            log.info("Event[{}] without name", event.getId());
            return false;
        }

        if(startTime.before(now)) {
            log.info("Event[{}] started [{}] before now", event.getId(), startTime);
            return false;
        }

        if(group.getLastEvent() != null && !group.getLastEvent().before(startTime)) {
            log.info("Event[{}] started before last event date", event.getId());
            return false;
        }


        return true;
    }

    @Override
    public Map<GrabbedGroup, List<ExternalEvent>> filter(Map<GrabbedGroup, List<ExternalEvent>> events) {
        if(events == null || events.isEmpty()) {
            return Collections.emptyMap();
        }

        List<String> fbEventIds = events.values().stream()
                .flatMap(List::stream)
                .map(ExternalEvent::getId)
                .collect(Collectors.toList());

        log.debug("FB events before filtering by DB: [{}]", fbEventIds);
        if(fbEventIds.isEmpty()) {
            return Collections.emptyMap();
        }

        List<String> containsEvents = grabbedActivityDao.findAllFbIds(fbEventIds);

        events.values().forEach(externalEvents -> externalEvents.removeIf(
                facebookEventDto ->  containsEvents.contains(facebookEventDto.getId()))
        );

        log.debug("FB events after filtering by DB: [{}]", events);
        events.entrySet().forEach(grabbedGroupListEntry ->
            log.debug("Now after filter in group: {} events size: {}",
                    grabbedGroupListEntry.getKey().getFbId(),
                    grabbedGroupListEntry.getValue().size())
        );

        return events;
    }

    @Override
    @Transactional
    public List<ActivityDto> convert(Map<GrabbedGroup, List<ExternalEvent>> eventsMap) {
        List<ActivityDto> activityDtos = new ArrayList<>();

        for (Map.Entry<GrabbedGroup, List<ExternalEvent>> grabbedGroupListEntry : eventsMap.entrySet()) {
            GrabbedGroup grabbedGroup = grabbedGroupListEntry.getKey();
            List<ExternalEvent> externalEvents = grabbedGroupListEntry.getValue();

            Date lastEventDate = new Date(0);
            for (ExternalEvent externalEvent : externalEvents) {
                AddressDto addressDto = calcAddress(grabbedGroup, externalEvent);

                ActivityDto activityDto = convertToActivity(externalEvent);
                activityDto.setAddress(addressDto);
                activityDto.setGrabbedGroupId(grabbedGroup.getId());
                activityService.saveWithOutValidate(activityDto);


                GrabbedActivity grabbedActivity = new GrabbedActivity();

                grabbedActivity.setFbId(urlToEventId(externalEvent.getUrl()));
                grabbedActivity.setName(externalEvent.getName());
                grabbedActivity.setStartDate(externalEvent.getStartTime());
                grabbedActivity.setEndDate(externalEvent.getEndTime());
                grabbedActivity.setType(externalEvent.getActivityType());
                grabbedActivity.setGroupId(grabbedGroup.getId());

                grabbedActivityDao.save(grabbedActivity);

                activityDtos.add(activityDto);

                lastEventDate = lastEventDate.before(externalEvent.getStartTime()) ? externalEvent.getStartTime() : lastEventDate;
            }

            grabbedGroup.setLastGrabbing(new Date());
            grabbedGroup.setLastEvent(lastEventDate);

            grabbedGroupDao.save(grabbedGroup);

        }

        return activityDtos;
    }

    private AddressDto calcAddress(GrabbedGroup grabbedGroup, ExternalEvent externalEvent) {
        AddressDto addressDto = null;

        if(externalEvent.getCity() != null) {
            CityDto cityDto = createCityDto(externalEvent.getCity());

            if(cityDto != null) {
                addressDto = createAddressDto(cityDto, externalEvent);
            }
        }

        log.info("Address from event: {}", addressDto);
        if(addressDto == null) {
            addressDto = addressService.getById(grabbedGroup.getAddressId());
            log.info("Address from group: {}", addressDto);
        }
        return addressDto;
    }

    private ActivityDto convertToActivity(ExternalEvent externalEvent) {
        ActivityDto activityDto = new ActivityDto();
        activityDto.setType(externalEvent.getActivityType());
        activityDto.setName(externalEvent.getName());
        activityDto.setStartDate(externalEvent.getStartTime());
        activityDto.setEndDate(externalEvent.getEndTime());
        activityDto.setDayOfWeek(Utils.convertToMondayWeekEnum(activityDto.getStartDate()));
        activityDto.setStartTime(externalEvent.getStartTime());
        activityDto.setTeachers(Collections.emptyList());
        activityDto.setDescription(externalEvent.getDescription());
        activityDto.setFbUrl(externalEvent.getUrl());
        activityDto.setDuration(Utils.calcDurationByDateInterval(externalEvent.getStartTime(),
                externalEvent.getEndTime()));

        return activityDto;
    }

    private ExternalEvent convertFacebookToExternalEvent(FacebookEventDto facebookEvent) {
        if(facebookEvent == null) {
            return null;
        }

        ExternalEvent externalEvent = new ExternalEvent();
        externalEvent.setId(facebookEvent.getId());
        externalEvent.setName(facebookEvent.getName());
        externalEvent.setDescription(facebookEvent.getDescription());
        externalEvent.setEndTime(facebookEvent.getEndTime());
        externalEvent.setStartTime(facebookEvent.getStartTime());
        externalEvent.setUrl(eventIdToUrl(facebookEvent.getId()));

        if(facebookEvent.getPlace() != null) {
            FacebookLocationDto location = facebookEvent.getPlace().getLocation();

            if(location != null) {
                externalEvent.setCity(location.getCity());
                externalEvent.setCountry(location.getCountry());
                externalEvent.setStreet(location.getStreet());
                externalEvent.setLongitude(location.getLongitude());
                externalEvent.setLatitude(location.getLatitude());
            }
        }

        return externalEvent;
    }

    private CityDto createCityDto(String cityName) {
        CityDto cityDto = new CityDto();
        cityDto.setName(cityName);
        return cityDto;
    }

    private AddressDto createAddressDto(CityDto cityDto, ExternalEvent externalEvent) {
        AddressDto addressDto = new AddressDto();
        addressDto.setCity(cityDto);
        addressDto.setLat(externalEvent.getLatitude());
        addressDto.setLng(externalEvent.getLongitude());
        addressDto.setValue(externalEvent.getStreet());
        addressDto.setPid(UUID.randomUUID().toString());

        return addressDto;
    }

    private String groupIdToGraphUrl(String groupId) {
        return "https://graph.facebook.com/" + groupId + "/events?access_token=" + externalTokenDao
                .findAnyAdminTokenByType(TokenType.FACEBOOK)
                .getToken();
    }

    private String eventIdToUrl(String eventId) {
        return FACEBOOK_EVENTS_URL + eventId;
    }

    private String urlToEventId(String urlEvent) {
        if(urlEvent == null || urlEvent.isEmpty() || urlEvent.length() <= FACEBOOK_EVENTS_URL.length()) {
            return null;
        }

        return urlEvent.substring(FACEBOOK_EVENTS_URL.length());
    }
}
