package org.satuban.tangoportal.portal.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.satuban.tangoportal.portal.model.enums.ActivityType;
import org.satuban.tangoportal.portal.model.enums.LessonLevel;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Artur on 13.06.2017.
 */
@Data
public class TimetableItem implements Serializable {
    private static final long serialVersionUID = -2235619117812070146L;

    private long id;
    private ActivityType type;
    private String name;
    private List<String> teachers;
    private Set<LessonLevel> level;
    @JsonIgnore
    private Date startTime;
    @JsonIgnore
    private Date endTime;
    private Long firstTeacherId;
    private Long secondTeacherId;
}
