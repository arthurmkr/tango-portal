package org.satuban.tangoportal.portal.util;

import org.satuban.tangoportal.portal.model.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * Служебный класс DTO профиля пользователя. Для инжекта в контроллеры
 * <p>
 * Created by Arthur on 19.01.2016.
 */
public class UserWebArgumentResolver {
//        implements HandlerMethodArgumentResolver {
//    private static final Logger log = LoggerFactory.getLogger(UserWebArgumentResolver.class);
//
//    @Override
//    public boolean supportsParameter(MethodParameter parameter) {
//        return parameter.getParameterType().isAssignableFrom(User.class);
//    }
//
//    @Override
//    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
//        return SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//    }
}
