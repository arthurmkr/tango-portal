package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.dto.RoomDto;

import java.util.List;

/**
 * Created by Artur on 11.06.2017.
 */
public interface SchoolRoomService extends CrudService<RoomDto> {
}
