package org.satuban.tangoportal.portal.model.dto.teacher;

import lombok.Data;

/**
 * Created by Artur on 23.06.2017.
 */
@Data
public class TeacherLink {
    private long id;
    private String firstName;
    private String secondName;
    private String avatar;
}
