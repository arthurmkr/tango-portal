package org.satuban.tangoportal.portal.dao;

import org.satuban.tangoportal.portal.dao.common.BaseDao;
import org.satuban.tangoportal.portal.model.dto.Dj;
import org.satuban.tangoportal.portal.model.dto.PublicProfile;
import org.satuban.tangoportal.portal.model.entity.User;

import java.util.List;

/**
 * Created by Artur on 20.05.2017.
 */
public interface UserDao extends BaseDao<User> {
    User findByUsername(String username);

    User findByInviteToken(String token);

    User getReferrerByUser(Long userId);

    List<User> findReferralsByUser(long userId);

    List<PublicProfile> findDjsByCity(String pid);

    List<PublicProfile> findPhotographers();

    List<Dj> getAllDjs();

    Dj getDjById(Long id);
}
