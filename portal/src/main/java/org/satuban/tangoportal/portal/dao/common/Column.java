package org.satuban.tangoportal.portal.dao.common;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Artur on 26.04.2017.
 */
@Target({FIELD})
@Retention(RUNTIME)
public @interface Column {
    String name() default "";

    boolean enumOrder() default false;
}
