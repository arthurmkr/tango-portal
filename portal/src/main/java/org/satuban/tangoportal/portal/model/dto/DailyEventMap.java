package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Artur on 01.06.2017.
 */
@Data
public class DailyEventMap implements Serializable {
    private static final long serialVersionUID = -8519761728411686743L;

    private double centerLat;
    private double centerLng;

    private List<EventMarker> events;
}
