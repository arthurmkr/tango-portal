package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Artur on 04.06.2017.
 */
@Data
public class CityDto implements Serializable {
    private static final long serialVersionUID = -1547772196969211389L;

    private Long id;
    private String name;
}
