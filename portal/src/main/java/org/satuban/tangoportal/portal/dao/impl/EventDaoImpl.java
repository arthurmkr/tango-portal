package org.satuban.tangoportal.portal.dao.impl;

import org.satuban.tangoportal.portal.dao.EventDao;
import org.satuban.tangoportal.portal.model.dto.EventListItem;
import org.satuban.tangoportal.portal.model.entity.Event;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Artur on 11.08.2017.
 */
@Repository
public class EventDaoImpl extends AbstractDao<Event> implements EventDao {
    private final RowMapper<EventListItem> listItemMapper = new BeanPropertyRowMapper<>(EventListItem.class);

    EventDaoImpl(NamedParameterJdbcTemplate template) {
        super(template, Event.class);
    }

    @Override
    public List<EventListItem> getAllListItemsForUser(long userId) {
        return template.query("SELECT f.id, f.name, extract(YEAR FROM f.start_date) \"year\", f.start_date " +
                        "FROM event f " +
                        " WHERE f.user_id = :userId " +
                        " ORDER BY f.start_date DESC ",
                Collections.singletonMap("userId", userId),
                listItemMapper);
    }

    @Override
    public Event getById(long id, long userId) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        params.put("userId", userId);

        List<Event> list = template.query("SELECT * FROM event WHERE id = :id AND user_id = :userId",
                params, rowMapper);

        return first(list);
    }

    @Override
    public List<Event> findAllActual(String pid) {
        return template.query("SELECT e.* " +
                        " FROM event e " +
                        "   JOIN city c ON c.id = e.city_id " +
                        "   WHERE (:pid = '' OR c.pid = :pid) " +
                        " AND e.end_date > now() " +
                        " ORDER BY start_date",
                Collections.singletonMap("pid", pid), rowMapper);
    }

    @Override
    public List<Event> findAllWithOutPictureAndFbLink() {
        return template.query("SELECT * FROM event e WHERE e.photo IS NULL AND e.fb_url IS NOT NULL AND e.end_date > now()", rowMapper);
    }
}
