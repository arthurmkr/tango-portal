package org.satuban.tangoportal.portal.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.client.utils.URIBuilder;
import org.satuban.tangoportal.portal.dao.ExternalTokenDao;
import org.satuban.tangoportal.portal.model.dto.external.facebook.FacebookAccessToken;
import org.satuban.tangoportal.portal.model.dto.external.facebook.FacebookError;
import org.satuban.tangoportal.portal.model.entity.ExternalToken;
import org.satuban.tangoportal.portal.model.enums.TokenType;
import org.satuban.tangoportal.portal.service.FacebookOauthService;
import org.satuban.tangoportal.portal.service.UserHolder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Optional;

import static org.satuban.tangoportal.portal.service.FacebookService.*;

/**
 * Created by Danilov on 26.09.2017.
 */
@Slf4j
@Service
public class FacebookOauthServiceImpl implements FacebookOauthService {
    private final String FACEBOOK_OAUTH_ACCESS_TOKEN = FACEBOOK_GRAPH_URL + "/" + FACEBOOK_API_VERSION + "/oauth/access_token"+
            "?client_id={app-id}"+
            "&client_secret={app-secret}";
    private final String FACEBOOK_GENERATE_LONG_TOKEN_URL = FACEBOOK_OAUTH_ACCESS_TOKEN +
            "&grant_type=fb_exchange_token" +
            "&fb_exchange_token={short-lived-token}";
    private final String FACEBOOK_GENERATE_ACCESS_TOKEN_BY_CODE_URL = FACEBOOK_OAUTH_ACCESS_TOKEN +
            "&redirect_uri={redirect-uri}" +
            "&code={code-parameter}";
    private final String FACEBOOK_GENERATE_APP_TOKEN_URL = FACEBOOK_OAUTH_ACCESS_TOKEN +
            "&grant_type=client_credentials";

    private final UserHolder userHolder;
    private final ExternalTokenDao externalTokenDao;

    private final String clientId;
    private final String clientSecret;
    private final String redirectUrl;
    private final String scope;

    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;

    public FacebookOauthServiceImpl(UserHolder userHolder,
                                    ExternalTokenDao externalTokenDao,
                                    @Value("${facebook.client.id}") String clientId,
                                    @Value("${facebook.client.secret}") String clientSecret,
                                    @Value("${facebook.redirect-url}") String redirectUrl,
                                    @Value("${facebook.scope}") String scope) {
        this.userHolder = userHolder;
        this.externalTokenDao = externalTokenDao;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.redirectUrl = redirectUrl;
        this.scope = scope;

        this.objectMapper = new ObjectMapper();
        this.restTemplate = new RestTemplate();
    }

    @Override
    public FacebookAccessToken generateApplicationToken() {
        return getAccessToken(FACEBOOK_GENERATE_APP_TOKEN_URL,
                clientId,
                clientSecret);
    }

    @Override
    public FacebookAccessToken generateAccessTokenByCode(String code) {
        FacebookAccessToken accessToken = getAccessToken(FACEBOOK_GENERATE_ACCESS_TOKEN_BY_CODE_URL,
                clientId,
                clientSecret,
                redirectUrl,
                code);

        long userId = this.userHolder.getUserId();
        if(userId > 0 && accessToken != null) {
            ExternalToken externalToken = Optional
                    .ofNullable(externalTokenDao.findByUserAndType(userId, TokenType.FACEBOOK))
                    .orElse(new ExternalToken());
            externalToken.setToken(accessToken.getAccessToken());
            externalToken.setDeadTime(accessToken.getDateToDeadToken());
            externalToken.setType(TokenType.FACEBOOK);
            externalToken.setUserId(userId);

            externalTokenDao.save(externalToken);
        }

        return accessToken;
    }

    @Override
    public String generateOauthUrl() {
        try {
            return new URIBuilder(FACEBOOK_URL + "/" + FACEBOOK_API_VERSION + "/dialog/oauth")
            .addParameter("client_id", clientId)
            .addParameter("redirect_uri", redirectUrl)
            .addParameter("scope", scope)
            .toString();
        } catch (URISyntaxException e) {
            log.error("Can't build url", e);
        }

        return null;
    }

    private FacebookAccessToken getAccessToken(String url, Object... values) {
        try {
            ResponseEntity<String> accessTokenResponseEntity = restTemplate
                    .exchange(url,
                            HttpMethod.GET,
                            null,
                            String.class,
                            values);

            log.debug("Body: {}", accessTokenResponseEntity.getBody());

            if(accessTokenResponseEntity.getStatusCode() == HttpStatus.OK) {
                FacebookAccessToken accessToken = readValue(accessTokenResponseEntity.getBody(), FacebookAccessToken.class);
                if(accessToken == null) {
                    return null;
                }

                Long expiresIn = accessToken.getExpiresIn();
                if(expiresIn != null) {
                    accessToken.setDateGenerateToken(new Date());
                    accessToken.setDateToDeadToken(DateUtils.addSeconds(accessToken.getDateGenerateToken(),
                                    expiresIn.intValue()));
                }

                return accessToken;
            } else {
                FacebookError facebookError = readValue(accessTokenResponseEntity.getBody(), FacebookError.class);
                log.error("Facebook return error: {}", facebookError);
            }
        } catch (RestClientException ex) {
            log.error("Can't read!", ex);
        }

        return null;
    }

    private <T> T readValue(String value, Class<T> type) {
        try {
            return objectMapper.readValue(value, type);
        } catch (IOException ex) {
            log.error("Can't parse!", ex);
        }

        return null;
    }
}
