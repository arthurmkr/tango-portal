package org.satuban.tangoportal.portal.model.entity;

import lombok.Data;
import org.satuban.tangoportal.portal.dao.common.Column;
import org.satuban.tangoportal.portal.dao.common.Table;

/**
 * Created by Artur on 31.05.2017.
 */
@Data
@Table
public class City implements BaseEntity {
    private static final long serialVersionUID = -2252931541161673345L;
    public Long id;
    @Column
    private String name;
    @Column
    private String pid;
    @Column
    private double lat;
    @Column
    private double lng;
}
