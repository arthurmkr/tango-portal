package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.entity.User;
import org.satuban.tangoportal.portal.model.enums.MailTemplates;

import java.util.List;

/**
 * Created by Artur on 16.07.2017.
 */
public interface MailService {
    void send(String to, String subject, String body);

    void sendRegisterConfirmationToUser(String userEmail, String registerToken, String language);

    void sendRegisterConfirmationToReferrers(String referrerEmail, List<User> users, String language);

    void sendApproveRegistrationMails(String username, String password, String language);
}
