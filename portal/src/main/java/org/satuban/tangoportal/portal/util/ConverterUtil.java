package org.satuban.tangoportal.portal.util;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.satuban.tangoportal.portal.dao.UserDao;
import org.satuban.tangoportal.portal.model.dto.ActivityDto;
import org.satuban.tangoportal.portal.model.entity.Activity;
import org.satuban.tangoportal.portal.model.entity.User;
import org.satuban.tangoportal.portal.service.AddressService;
import org.springframework.stereotype.Component;

/**
 * Created by Artur on 23.05.2017.
 */
@Component
public class ConverterUtil {
    private final AddressService addressService;
    private final UserDao userDao;
    private FastDateFormat dateFormat = FastDateFormat.getInstance("dd/MM/yyyy");
    private FastDateFormat timeFormat = FastDateFormat.getInstance("HH:mm");

    public ConverterUtil(AddressService addressService, UserDao userDao) {
        this.addressService = addressService;
        this.userDao = userDao;
    }

    public ActivityDto convertActivityToMilongaDto(Activity a) {
        ActivityDto dto = new ActivityDto();
        dto.setId(a.getId());
        dto.setName(a.getName());
        dto.setCost(a.getCost());
        dto.setCurrency(a.getCurrency());
        dto.setDjId(a.getDjId());

        if (a.getDjId() != null) {
            User dj = userDao.getById(a.getDjId());

            // TODO надо подумать о том, чтобы не делать конкатенацию строк и насколько целесообразно здесь оптимизировать
            dto.setDjFullName(dj.getFirstName() + " " + dj.getSecondName());
        }
        dto.setAddress(addressService.getById(a.getAddressId()));
        dto.setDuration(a.getDuration());
        dto.setStartDate(a.getStartDate());
        dto.setStartTime(a.getStartTime());
        dto.setEndTime(a.getDuration() == 0 ? null : DateUtils.addMinutes(a.getStartTime(), a.getDuration()));
        dto.setVkUrl(a.getVkUrl());
        dto.setFbUrl(a.getFbUrl());
        dto.setPhoto(a.getPhoto());
        dto.setEventId(a.getEventId());

        return dto;
    }
}
