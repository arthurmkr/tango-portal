package org.satuban.tangoportal.portal.model.enums;

/**
 * Created by Artur on 19.07.2017.
 */
public enum MailTemplates {
    CONFIRM_REGISTER_REQUEST("confirmRegisterRequest"),
    USERS_WAIT_REGISTRATION_CONFIRM("usersWaitRegistrationConfirm"),
    REGISTRATION_APPROVED("registrationApproved");

    private final String templateName;

    MailTemplates(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateName() {
        return templateName;
    }
}
