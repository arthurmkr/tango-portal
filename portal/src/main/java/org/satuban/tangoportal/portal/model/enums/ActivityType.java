package org.satuban.tangoportal.portal.model.enums;

/**
 * Created by Artur on 16.05.2017.
 */
public enum ActivityType {
    LESSON,
    PRACTICE,
    MILONGA,
    ACTION
}
