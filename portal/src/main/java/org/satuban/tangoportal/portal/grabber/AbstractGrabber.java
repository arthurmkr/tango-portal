package org.satuban.tangoportal.portal.grabber;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.satuban.tangoportal.portal.model.enums.ActivityType;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Artur on 25.09.2017.
 */
@Slf4j
@Service
public class AbstractGrabber implements Grabber {
    private final CloseableHttpClient client;
    private String url;
    private String rootBlock;
    private String itemBlock;
    private String itemBlockEnd;
    private int year;

    public AbstractGrabber() {
        this.client  = HttpClients.createDefault();
        url = "/groups/637938676239622/events/";
        rootBlock = "fbGroupCalendar";
        itemBlock = "uiProfileBlockContent";
        itemBlockEnd = "</div></div></div></div>";
    }

    public void grab() throws IOException {
        initYear();
        String string = loadHtml(url);

        int fbGroupCalendar = string.indexOf(rootBlock);
        int openBrace = string.lastIndexOf("<!--", fbGroupCalendar);
        int closeBrace = string.indexOf("-->", fbGroupCalendar);

        String content = string.substring(openBrace, closeBrace);

        int startBlock;
        int endBlock = 0;

        while (true) {
            startBlock = content.indexOf(itemBlock, endBlock);

            if (startBlock < 0) {
                break;
            }

            endBlock = content.indexOf(itemBlockEnd, startBlock);

            try {
                parseItem(content.substring(startBlock, endBlock));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private void initYear() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
    }

    private void parseItem(String itemContent) throws IOException, ParseException {
        String eventPrefix = "/events/";
        int eventUrlStart = itemContent.indexOf(eventPrefix);
        int eventUrlEnd = itemContent.indexOf("/", eventUrlStart + eventPrefix.length());

        String eventId = itemContent.substring(eventUrlStart + eventPrefix.length(), eventUrlEnd);
        String eventUrl = itemContent.substring(eventUrlStart, eventUrlEnd);

        String itemFullContent = loadHtml(eventUrl);
        String eventName = extractEventName(itemFullContent);

        DateRange dateRange = extractEventDate(itemFullContent);

        if(dateRange == null) {
            return;
        }

        ActivityType type = matchType(eventName);

        if (type == null) {
            return;
        }

        GrabberItem res = GrabberItem.builder()
                .id(eventId)
                .from(dateRange.getFrom())
                .to(dateRange.getTo())
                .name(eventName)
                .url(eventUrl)
                .type(type)
                .build();

        processGrabberItem(res);
    }

    private void processGrabberItem(GrabberItem res) {
        log.info("Res: {}", res);
    }

    private ActivityType matchType(String eventName) {
        return eventName.startsWith("Рандеву") ? ActivityType.MILONGA : null;
    }

    private DateRange extractEventDate(String itemFullContent) throws ParseException {
        String datePrefix = "startDate";
        int indexOfNameClass = itemFullContent.indexOf(datePrefix);

        String startDate = "<span>С";
        int startOfStartTag = itemFullContent.indexOf(startDate, indexOfNameClass);
        String dateSeparator = " до ";
        int startOfEndTag = itemFullContent.indexOf(dateSeparator, startOfStartTag);

        if (startOfStartTag < 0) {
            return null;
        }

        if (startOfEndTag < 0) {
            return null;
        }

        FastDateFormat dateFormat = FastDateFormat.getInstance("HH:mm dd MMMM yyyy");
        String dateFromStr = itemFullContent.substring(startOfStartTag + startDate.length() + 1, startOfEndTag) + " " + year;

        int startOfEndDate = startOfEndTag + dateSeparator.length();
        int endOfEndDate = itemFullContent.indexOf("</span>", startOfEndDate);
        String dateToStr = itemFullContent.substring(startOfEndDate, endOfEndDate).replaceFirst("UTC[\\-, \\+]\\d\\d\\s", "") + " " + year;

        Date from = dateFormat.parse(dateFromStr);
        Date to = dateFormat.parse(dateToStr);

        if (from.after(to)) {
            to = DateUtils.addYears(to, 1);
        }

        return new DateRange(from, to);
    }

    private String extractEventName(String itemFullContent) {
        String nameClass = "event-permalink-event-name";
        int indexOfNameClass = itemFullContent.indexOf(nameClass);

        int endOfStartTag = itemFullContent.indexOf(">", indexOfNameClass);
        int startOfEndTag = itemFullContent.indexOf("<", endOfStartTag);

        if (endOfStartTag < 0) {
            return null;
        }

        return itemFullContent.substring(endOfStartTag + 1, startOfEndTag);
    }

    private String loadHtml(String url) throws IOException {
        log.info("Load page: {}", url);

        HttpGet get = new HttpGet("https://www.facebook.com" + url);

        CloseableHttpResponse response = client.execute(get);

        HttpEntity entity = response.getEntity();

        String res = IOUtils.toString(entity.getContent(), "UTF-8");
        get.releaseConnection();

        return res;
    }

    @Data
    @AllArgsConstructor
    private class DateRange {
        private Date from;
        private Date to;
    }
}
