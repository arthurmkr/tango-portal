package org.satuban.tangoportal.portal.model.dto.external.facebook;

import lombok.Data;

/**
 * Created by Danilov on 26.09.2017.
 */
@Data
public class FacebookErrorContent {
    private String message;
    private String type;
    private String fbtrace_id;
    private Integer code;
}
