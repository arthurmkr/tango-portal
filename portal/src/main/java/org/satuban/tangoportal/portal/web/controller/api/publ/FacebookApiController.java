package org.satuban.tangoportal.portal.web.controller.api.publ;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.model.dto.ActivityDto;
import org.satuban.tangoportal.portal.model.dto.EventDto;
import org.satuban.tangoportal.portal.model.dto.external.facebook.FacebookAccessToken;
import org.satuban.tangoportal.portal.service.FacebookOauthService;
import org.satuban.tangoportal.portal.service.impl.FacebookEventReader;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Danilov on 26.09.2017.
 */
@RestController
@RequestMapping("external/facebook")
@Slf4j
public class FacebookApiController {

    private final FacebookOauthService facebookOauthService;
    private final FacebookEventReader facebookEventReader;

    public FacebookApiController(FacebookOauthService facebookOauthService,
                                 FacebookEventReader facebookEventReader) {
        this.facebookOauthService = facebookOauthService;
        this.facebookEventReader = facebookEventReader;
    }

    @GetMapping("token")
    public FacebookAccessToken getAccessToken(@RequestParam("code") String code) {
        return facebookOauthService.generateAccessTokenByCode(code);
    }

    @GetMapping("activities")
    public List<ActivityDto> getActivities() {
        return facebookEventReader.updateEvents();
    }

    @GetMapping("oauth/url")
    public String facebookOauthUrl() {
        return facebookOauthService.generateOauthUrl();
    }

}
