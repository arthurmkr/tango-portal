package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Artur on 13.06.2017.
 */
@Data
public class DayTimetable implements Serializable {
    private static final long serialVersionUID = 4325841672397218688L;

    private DayOfWeek dayOfWeek;
    private List<TimetableRow> rows = new ArrayList<>();
}
