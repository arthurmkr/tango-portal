package org.satuban.tangoportal.portal.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.dao.AddressDao;
import org.satuban.tangoportal.portal.dao.RoomDao;
import org.satuban.tangoportal.portal.dao.SchoolDao;
import org.satuban.tangoportal.portal.model.dto.RoomDto;
import org.satuban.tangoportal.portal.model.entity.Address;
import org.satuban.tangoportal.portal.model.entity.Room;
import org.satuban.tangoportal.portal.model.entity.School;
import org.satuban.tangoportal.portal.service.AddressService;
import org.satuban.tangoportal.portal.service.SchoolRoomService;
import org.satuban.tangoportal.portal.service.UserHolder;
import org.satuban.tangoportal.portal.util.ConverterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Artur on 11.06.2017.
 */
@Slf4j
@Service
public class SchoolRoomServiceImpl implements SchoolRoomService {
    private final RoomDao roomDao;
    private final UserHolder userHolder;
    private final AddressDao addressDao;
    private final AddressService addressService;
    private final SchoolDao schoolDao;
    private final ConverterUtil converterUtil;

    @Autowired
    public SchoolRoomServiceImpl(RoomDao roomDao, UserHolder userHolder, AddressDao addressDao, AddressService addressService, SchoolDao schoolDao, ConverterUtil converterUtil) {
        this.roomDao = roomDao;
        this.userHolder = userHolder;
        this.addressDao = addressDao;
        this.addressService = addressService;
        this.schoolDao = schoolDao;
        this.converterUtil = converterUtil;
    }


    @Override
    @Transactional
    public RoomDto save(RoomDto dto) {
        School school = schoolDao.findByUserId(userHolder.getUserId());

        if (school == null) {
            throw new RuntimeException("User doesn't have school");
        }

        Room room = new Room();
        room.setId(dto.getId());
        room.setName(dto.getName());
        room.setSchoolId(school.getId());

        Address address = addressService.getOrCreateAddress(dto.getAddress());

        room.setAddressId(address.getId());
        room.setUserId(userHolder.getUserId());

        return entityToDto(roomDao.save(room));
    }

    @Override
    @Transactional
    public void remove(long id) {
        roomDao.removeById(id);
    }

    @Override
    @Transactional
    public List<RoomDto> getAll() {
        return roomDao.findAllSchoolForUser(userHolder.getUserId())
                .stream()
                .map(this::entityToDto)
                .collect(Collectors.toList());
    }

    protected RoomDto entityToDto(Room entity) {
        RoomDto dto = new RoomDto();

        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setAddress(addressService.getById(entity.getAddressId()));

        return dto;
    }
}
