package org.satuban.tangoportal.portal.model.enums;

import java.util.*;

/**
 * Created by Artur on 27.05.2017.
 */
public enum LessonLevel {
    A(0x0001), B(0x0002), C(0x0004), D(0x0008);

    private final int mask;
    private static Map<Integer, Set<LessonLevel>> cache = Collections.synchronizedMap(new HashMap<>());

    LessonLevel(int mask) {
        this.mask = mask;
    }

    public static int toCode(Set<LessonLevel> set) {
        int code = 0;
        for(LessonLevel level : set) {
            code |= level.mask;
        }

        return code;
    }

    public static Set<LessonLevel> toSet(int code) {
        Set<LessonLevel> res = EnumSet.noneOf(LessonLevel.class);

        for(LessonLevel level : values()) {
            if((level.mask & code) > 0) {
                res.add(level);
            }
        }

        return res;
    }
}
