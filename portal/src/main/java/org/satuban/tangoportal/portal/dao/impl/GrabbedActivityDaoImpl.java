package org.satuban.tangoportal.portal.dao.impl;

import org.satuban.tangoportal.portal.dao.GrabbedActivityDao;
import org.satuban.tangoportal.portal.model.entity.GrabbedActivity;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Artur on 25.09.2017.
 */
@Repository
public class GrabbedActivityDaoImpl extends AbstractDao<GrabbedActivity> implements GrabbedActivityDao {
    GrabbedActivityDaoImpl(NamedParameterJdbcTemplate template) {
        super(template, GrabbedActivity.class);
    }

    @Override
    public List<String> findAllFbIds(List<String> fbIds) {
        if(fbIds == null || fbIds.isEmpty()) {
            return Collections.emptyList();
        }

        return template.query("SELECT fb_id FROM grabbed_activity WHERE fb_id in (:ids)",
                Collections.singletonMap("ids", fbIds), rowMapper)
                .stream()
                .map(GrabbedActivity::getFbId)
                .collect(Collectors.toList());
    }
}
