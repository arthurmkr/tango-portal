package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;
import org.satuban.tangoportal.portal.dao.common.Column;

/**
 * Created by Artur on 21.07.2017.
 */
@Data
public class JsErrorRequest {
    @Column
    private String message;
    @Column
    private String url;
    @Column
    private int lineNumber;
    @Column
    private int colNumber;
}
