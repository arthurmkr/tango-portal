package org.satuban.tangoportal.portal.web.controller.api.priv;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.model.dto.Profile;
import org.satuban.tangoportal.portal.model.dto.SchoolDto;
import org.satuban.tangoportal.portal.model.dto.WeekTimetable;
import org.satuban.tangoportal.portal.service.SchoolService;
import org.satuban.tangoportal.portal.service.TimetableService;
import org.satuban.tangoportal.portal.web.WebConstants;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;

/**
 * Created by Artur on 04.06.2017.
 */
@Slf4j
@RestController
@RequestMapping(WebConstants.PRIVATE_API + "/school")
public class SchoolApiController {
    private final SchoolService schoolService;
    private final TimetableService timetableService;

    public SchoolApiController(SchoolService schoolService, TimetableService timetableService) {
        this.schoolService = schoolService;
        this.timetableService = timetableService;
    }

    @PostMapping
    public SchoolDto save(@RequestBody  SchoolDto dto) {
       return schoolService.save(dto);
    }

    @GetMapping
    public SchoolDto getSchool() {
        return schoolService.get();
    }

    @GetMapping("/timetable")
    public WeekTimetable getTimetable(@RequestParam Date startFrom) {
        return timetableService.getForSchool(startFrom);
    }
}
