package org.satuban.tangoportal.portal.web.controller.api.publ;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.model.dto.EventDto;
import org.satuban.tangoportal.portal.model.filters.DjListFilter;
import org.satuban.tangoportal.portal.model.filters.EventListFilter;
import org.satuban.tangoportal.portal.service.EventService;
import org.satuban.tangoportal.portal.web.WebConstants;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Artur on 24.06.2017.
 */
@Slf4j
@RestController
@RequestMapping(WebConstants.PUBLIC_API + "/event")
public class EventPublicApiController {
    private final EventService eventService;

    public EventPublicApiController(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping
    public List<EventDto> getEvents(EventListFilter filter) {
        return eventService.getEvents(filter);
    }
}
