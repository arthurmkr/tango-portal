package org.satuban.tangoportal.portal.model.entity;

import lombok.Data;
import org.satuban.tangoportal.portal.dao.common.Column;
import org.satuban.tangoportal.portal.dao.common.Table;

import java.util.Date;

/**
 * Created by Artur on 25.09.2017.
 */
@Data
@Table
public class GrabbedGroup implements BaseEntity{
    private static final long serialVersionUID = -7059217389154206325L;

    private Long id;
    @Column
    private String fbId;
    @Column
    private Date lastEvent;
    @Column
    private Date lastGrabbing;
    @Column
    private String regexpFilter;
    @Column
    private Long addressId;
    @Column
    private boolean disabled;
}
