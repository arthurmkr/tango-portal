package org.satuban.tangoportal.portal.web.controller.api.publ;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.model.dto.EventDto;
import org.satuban.tangoportal.portal.model.dto.PublicProfile;
import org.satuban.tangoportal.portal.model.filters.DjListFilter;
import org.satuban.tangoportal.portal.service.EventService;
import org.satuban.tangoportal.portal.service.UserService;
import org.satuban.tangoportal.portal.web.WebConstants;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Artur on 24.06.2017.
 */
@Slf4j
@RestController
@RequestMapping(WebConstants.PUBLIC_API + "/user")
public class UserPublicApiController {
    private final UserService userService;

    public UserPublicApiController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/dj")
    public List<PublicProfile> getDjs(DjListFilter filter) {
        return userService.getDjsByFilter(filter);
    }

    @GetMapping("/photographer")
    public List<PublicProfile> get() {
        return userService.getPhotographers();
    }
}
