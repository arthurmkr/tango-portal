package org.satuban.tangoportal.portal.model.entity;

import lombok.Data;
import org.satuban.tangoportal.portal.dao.common.Column;
import org.satuban.tangoportal.portal.dao.common.Table;

/**
 * Created by Artur on 07.06.2017.
 */
@Data
@Table
public class Teacher implements BaseEntity {
    private static final long serialVersionUID = -6265188571539686803L;

    private Long id;
    @Column
    private String firstName;
    @Column
    private String secondName;
    @Column
    private String resume;
    @Column
    private String avatar;
    @Column
    private long schoolId;
    @Column
    private long userId;

    public String getFullName() {
        return firstName + " " + secondName;
    }
}
