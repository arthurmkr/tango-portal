package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.dto.Profile;
import org.satuban.tangoportal.portal.model.dto.RegistrationConfirmResponse;

import java.util.Locale;

/**
 * Created by Artur on 18.07.2017.
 */
public interface InviteService {
    boolean isValidToken(String token);

    void register(String token, Profile profile, Locale locale);

    RegistrationConfirmResponse confirm(String token);

    void rejectRequest(long id);

    void approveRequest(long userId);

    String getReferralLink();
}
