package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.dto.EventDto;
import org.satuban.tangoportal.portal.model.dto.EventListItem;
import org.satuban.tangoportal.portal.model.filters.EventListFilter;

import java.util.List;

/**
 * Created by Artur on 11.08.2017.
 */
public interface EventService {
    List<EventListItem> getListData();

    EventDto getById(long id);

    EventDto save(EventDto dto);

    void remove(long id);

    List<EventDto> getEvents(EventListFilter filter);
}
