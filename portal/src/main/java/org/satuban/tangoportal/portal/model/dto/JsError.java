package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;
import org.satuban.tangoportal.portal.dao.common.Column;
import org.satuban.tangoportal.portal.dao.common.Table;
import org.satuban.tangoportal.portal.model.entity.BaseEntity;

import java.util.Date;

/**
 * Created by Artur on 21.07.2017.
 */
@Data
@Table
public class JsError implements BaseEntity {
    private static final long serialVersionUID = 3451172426380393851L;

    private Long id;
    @Column
    private String message;
    @Column
    private String url;
    @Column
    private int lineNumber;
    @Column
    private int colNumber;
    @Column
    private long userId;
    @Column
    private Date created;
    @Column
    private String userAgent;
    @Column
    private String ip;
    @Column
    private String referrer;

}
