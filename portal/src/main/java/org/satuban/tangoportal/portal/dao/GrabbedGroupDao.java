package org.satuban.tangoportal.portal.dao;

import org.satuban.tangoportal.portal.dao.common.BaseDao;
import org.satuban.tangoportal.portal.model.entity.GrabbedGroup;

import java.util.List;

/**
 * Created by Artur on 25.09.2017.
 */
public interface GrabbedGroupDao extends BaseDao<GrabbedGroup> {
    List<GrabbedGroup> getAllEnabled();
}
