package org.satuban.tangoportal.portal.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Artur on 23.05.2017.
 */
@Data
public class SchoolDto implements Serializable {
    private static final long serialVersionUID = 3518841365258668494L;

    private Long id;
    private String name;
    private String mobilePhone;
    private String workPhone;
    private String siteUrl;
    private String fbUrl;
    private String vkUrl;
    private AddressDto address;
    private Integer defaultDuration;
    private String photo;
}
