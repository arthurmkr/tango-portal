package org.satuban.tangoportal.portal.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by Artur on 20.07.2017.
 */
@Service
public class Generator {
    private final String referralUrlPrefix;

    public Generator(@Value("${app.server.url}") String serverUrl,
                     @Value("${referral.urlPart}") String referralUrlPart) {
        this.referralUrlPrefix = serverUrl + referralUrlPart;
    }

    public String generateReferralLink(String token) {
        return StringUtils.isBlank(token) ? null : referralUrlPrefix + token;
    }
}
