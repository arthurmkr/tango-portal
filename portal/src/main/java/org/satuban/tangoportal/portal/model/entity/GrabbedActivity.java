package org.satuban.tangoportal.portal.model.entity;

import lombok.Data;
import org.satuban.tangoportal.portal.dao.common.Column;
import org.satuban.tangoportal.portal.dao.common.Table;
import org.satuban.tangoportal.portal.model.enums.ActivityType;

import java.util.Date;

/**
 * Created by Artur on 25.09.2017.
 */
@Data
@Table
public class GrabbedActivity implements BaseEntity{
    private static final long serialVersionUID = -4475277582530256528L;

    private Long id;
    @Column
    private long groupId;
    @Column
    private String name;
    @Column
    private String fbId;
    @Column
    private Date startDate;
    @Column
    private Date endDate;
    @Column
    private ActivityType type;
}
