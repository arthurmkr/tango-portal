package org.satuban.tangoportal.portal.dao.impl;

import org.satuban.tangoportal.portal.dao.GrabbedGroupDao;
import org.satuban.tangoportal.portal.model.entity.GrabbedGroup;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

/**
 * Created by Artur on 25.09.2017.
 */
@Repository
public class GrabbedGroupDaoImpl extends AbstractDao<GrabbedGroup> implements GrabbedGroupDao {
    GrabbedGroupDaoImpl(NamedParameterJdbcTemplate template) {
        super(template, GrabbedGroup.class);
    }

    @Override
    public List<GrabbedGroup> getAllEnabled() {
        return template.query("SELECT * FROM grabbed_group WHERE disabled = FALSE", Collections.emptyMap(), rowMapper);
    }
}
