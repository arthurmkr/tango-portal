package org.satuban.tangoportal.portal.dao.impl;

import org.satuban.tangoportal.portal.dao.SchoolDao;
import org.satuban.tangoportal.portal.model.entity.School;
import org.satuban.tangoportal.portal.model.filters.SchoolListFilter;
import org.satuban.tangoportal.portal.model.dto.SchoolRequest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

/**
 * Created by Artur on 17.05.2017.
 */
@Repository
public class SchoolDaoImpl extends AbstractDao<School> implements SchoolDao {
    SchoolDaoImpl(NamedParameterJdbcTemplate template) {
        super(template, School.class);
    }

    @Override
    public School findByUserId(long userId) {
        List<School> query = template.query("SELECT s.* FROM school s " +
                        " WHERE s.user_id = :userId",
                Collections.singletonMap("userId", userId),
                rowMapper);

        return query.isEmpty() ? null : query.get(0);
    }

    @Override
    public SchoolRequest getFormByUserId(long userId) {
        List<SchoolRequest> list = template.query("SELECT " +
                        " s.*, " +
                        " addr.value address, " +
                        " addr.pid addressPid, " +
                        " addr.lat lat, " +
                        " addr.lng lng, " +
                        " city.name cityName " +
                        " FROM school s " +
                        " LEFT JOIN address addr ON addr.id = s.address_id " +
                        " LEFT JOIN city ON city.id = addr.city_id " +
                        " WHERE s.user_id = :userId",
                Collections.singletonMap("userId", userId),
                new BeanPropertyRowMapper<>(SchoolRequest.class));
        return list.isEmpty() ? null : list.get(0);
    }

    @Override
    public List<School> findByFilter(String pid) {
        return template.query("SELECT s.* FROM school s " +
                        " JOIN address addr ON addr.id = s.address_id " +
                        " JOIN city c ON c.id = addr.city_id " +
                        " WHERE c.pid = :pid",
                Collections.singletonMap("pid", pid), rowMapper);
    }

    @Override
    public List<School> findAll() {
        return template.query("SELECT * FROM school", rowMapper);
    }
}
