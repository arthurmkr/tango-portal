package org.satuban.tangoportal.portal.web.controller.api.priv;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.model.dto.Profile;
import org.satuban.tangoportal.portal.service.InviteService;
import org.satuban.tangoportal.portal.service.UserService;
import org.satuban.tangoportal.portal.web.WebConstants;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Artur on 20.07.2017.
 */
@Slf4j
@RestController
@RequestMapping(WebConstants.PRIVATE_API + "/referral")
public class ReferralApiController {
    private final UserService userService;
    private final InviteService inviteService;

    public ReferralApiController(UserService userService, InviteService inviteService) {
        this.userService = userService;
        this.inviteService = inviteService;
    }

    @GetMapping
    public List<Profile> get() {
        return userService.getReferrals();
    }

    @DeleteMapping("/{id}")
    public HttpStatus remove(@PathVariable long id) {
        inviteService.rejectRequest(id);
        return HttpStatus.OK;
    }

    @PutMapping("/{userId}/approve")
    public HttpStatus approve(@PathVariable long userId) {
        inviteService.approveRequest(userId);
        return HttpStatus.OK;
    }

    @GetMapping("/getLink")
    public String getLink() {
        return inviteService.getReferralLink();
    }

}
