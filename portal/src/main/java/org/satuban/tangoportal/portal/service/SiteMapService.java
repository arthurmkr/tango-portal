package org.satuban.tangoportal.portal.service;

import org.satuban.tangoportal.portal.model.dto.SiteMapDto;

import java.io.File;
import java.util.List;

/**
 * Created by Danilov on 17.08.2017.
 */
public interface SiteMapService {
    List<SiteMapDto> getCities(String currentUrl);
}
