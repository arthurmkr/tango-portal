package org.satuban.tangoportal.portal.web.controller.api.publ;

import lombok.extern.slf4j.Slf4j;
import org.satuban.tangoportal.portal.model.dto.Profile;
import org.satuban.tangoportal.portal.service.InviteService;
import org.satuban.tangoportal.portal.web.WebConstants;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Artur on 18.07.2017.
 */
@Slf4j
@RestController
@RequestMapping(WebConstants.PUBLIC_API + "/register")
public class RegisterApiController {
    private final InviteService inviteService;

    public RegisterApiController(InviteService inviteService) {
        this.inviteService = inviteService;
    }

    @PostMapping("/{token}")
    public HttpStatus register(@PathVariable String token, @RequestBody Profile profile) {
        log.info("token: {}, request: {}", token, profile);
        inviteService.register(token, profile, LocaleContextHolder.getLocale());
        return HttpStatus.OK;
    }
}
