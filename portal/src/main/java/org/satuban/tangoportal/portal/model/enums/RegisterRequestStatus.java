package org.satuban.tangoportal.portal.model.enums;

/**
 * Created by Artur on 20.07.2017.
 */

public enum RegisterRequestStatus {
    NEW,
    CONFIRMED,
    APPROVED,
    REJECTED
}
