package org.satuban.tangoportal.portal.service.impl;

import org.satuban.tangoportal.portal.model.entity.User;
import org.satuban.tangoportal.portal.service.UserHolder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * Created by Artur on 08.06.2017.
 */
@Service
public class UserHolderImpl implements UserHolder {
    @Override
    public User get() {
        SecurityContext context = SecurityContextHolder.getContext();
        if(context == null) {
            return null;
        }

        Authentication authentication = context.getAuthentication();
        if(authentication == null) {
            return null;
        }

        Object principal = authentication.getPrincipal();
        return principal instanceof User ? (User) principal : null;
    }

    @Override
    public long getUserId() {
        User user = get();

        if (user == null) {
            return 0;
        }

        return user.getId();
    }

}
