package org.satuban.tangoportal.portal;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Artur on 23.05.2017.
 */
@TestConfiguration
@ComponentScan(basePackages = {
        "org.satuban.tangoportal.portal.dao.impl",
        "org.satuban.tangoportal.portal.service.impl",
        "org.satuban.tangoportal.portal.util"})
public class TPTestConfig {
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
