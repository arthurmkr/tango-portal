package org.satuban.tangoportal.portal.service;

import org.junit.Test;
import org.satuban.tangoportal.portal.BaseTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;
/**
 * Created by Artur on 31.05.2017.
 */
public class GoogleApiTest extends BaseTest {
    @Autowired
    private GoogleApi googleApi;

    @Test
    public void getPlaceIdForCity() throws Exception {
        String expected = "ChIJybDUc_xKtUYRTM9XV8zWRD0";
        assertEquals(expected, googleApi.getPlaceForCity("Москва").getPlaceId());

        assertEquals(expected, googleApi.getPlaceForCity("Moscow").getPlaceId());
    }

}