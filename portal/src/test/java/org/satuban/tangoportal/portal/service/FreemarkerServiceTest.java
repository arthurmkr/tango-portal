package org.satuban.tangoportal.portal.service;

import org.junit.Ignore;
import org.junit.Test;
import org.satuban.tangoportal.portal.BaseTest;
import org.satuban.tangoportal.portal.model.enums.MailTemplates;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;

/**
 * Created by Artur on 19.07.2017.
 */
public class FreemarkerServiceTest extends BaseTest {
    @Autowired
    private FreemarkerService freemarkerService;

    @Ignore
    @Test
    public void generate() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("inviteToken", "234");
        params.put("registerToken", "xfgsdf");
        System.out.println(freemarkerService.generate(MailTemplates.CONFIRM_REGISTER_REQUEST, "ru", params));
    }
}
