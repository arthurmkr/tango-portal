package org.satuban.tangoportal.portal.service;

import lombok.extern.log4j.Log4j;
import org.junit.Ignore;
import org.junit.Test;
import org.satuban.tangoportal.portal.BaseTest;
import org.satuban.tangoportal.portal.dao.EventDao;
import org.satuban.tangoportal.portal.model.dto.external.facebook.FacebookAccessToken;
import org.satuban.tangoportal.portal.model.entity.Event;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Danilov on 26.09.2017.
 */
@Log4j
public class FacebookServiceTest extends BaseTest {
    @Autowired
    FacebookService facebookService;
    @Autowired
    EventDao eventDao;

    @Test
    public void updateEvents() {
        List<Event> allWithOutPictureAndFbLink = eventDao.findAllWithOutPictureAndFbLink();
        log.info(allWithOutPictureAndFbLink.size());
        facebookService.updateEventsPhoto(allWithOutPictureAndFbLink);
    }

}
