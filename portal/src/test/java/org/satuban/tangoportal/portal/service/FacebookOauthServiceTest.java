package org.satuban.tangoportal.portal.service;

import lombok.extern.log4j.Log4j;
import org.junit.Ignore;
import org.junit.Test;
import org.satuban.tangoportal.portal.BaseTest;
import org.satuban.tangoportal.portal.model.dto.external.facebook.FacebookAccessToken;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

/**
 * Created by Danilov on 26.09.2017.
 */
@Log4j
@Ignore
public class FacebookOauthServiceTest extends BaseTest {
    @Autowired
    FacebookOauthService facebookOauthService;

    @Test
    public void generateAppAccessToken() {
        FacebookAccessToken applicationToken = facebookOauthService.generateApplicationToken();
        assertNotNull(applicationToken);
        assertNotNull(applicationToken.getAccessToken());

        log.debug(applicationToken.getAccessToken());
    }

    @Test
    public void generatePopupUrl() {
        String url = facebookOauthService.generateOauthUrl();
        assertNotNull(url);

        log.debug(url);
    }

}
