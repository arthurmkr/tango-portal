package org.satuban.tangoportal.portal.dao;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.satuban.tangoportal.portal.BaseTest;
import org.satuban.tangoportal.portal.model.entity.*;
import org.satuban.tangoportal.portal.model.enums.ActivityType;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.DayOfWeek;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Artur on 28.05.2017.
 */
public class ActivityDaoTest extends BaseTest {

    @Autowired
    private ActivityDao activityDao;

    @Autowired
    private CityDao cityDao;

    @Autowired
    private AddressDao addressDao;

    @Autowired
    private SchoolDao schoolDao;

    @Autowired
    private RoomDao roomDao;

    Room room;
    School school;
    private City city;
    private Address address;

    @Before
    public void setUp() throws Exception {
        activityDao.getAll().forEach(e ->
                activityDao.removeById(e.getId())
        );

        city = createCity("city");
        address = createAddress(city);
        school = createSchool(address.getId());
        room = createRoom(school, address.getId());
    }

//    @Test
//    public void testAddLesson() {
//        Activity expected = createRegularActivity(DayOfWeek.WEDNESDAY, createDate(1, 4, 2017), createDate(7, 4, 2017));
//        Activity saved = activityDao.save(expected);
//
//        assertNotNull(saved);
//        assertNotNull(saved.getId());
//
//        Activity act = activityDao.findById(saved.getId());
//        assertNotNull(act);
//        assertEquals(saved.getId(), act.getId());
//
//        assertEquals(expected.getStartDate(), act.getStartDate());
//        assertEquals(expected.getEndDate(), act.getEndDate());
//    }
//

    /**
     * Начало действия после завтра, повтор по завтрашнему дню, период с сегодняшнего дня
     */
    @Test
    public void testFindAllByRange1() {
        Date now = createDate(13, 6, 2017);



        Activity saved = activityDao.save(createRegularActivity(DayOfWeek.WEDNESDAY, now, DateUtils.addDays(now, 20), address.getId(), school, room));

        List<Activity> list = activityDao.findAllByRange(createDate(11, 6, 2017), createDate(18, 6, 2017), "city");
        assertTrue(list.isEmpty());
    }


    /**
     * Начало действия после завтра, повтор по завтрашнему дню, период с завтрашнего дня
     */
    @Test
    public void testFindAllByRange2() {
        Date now = createDate(13, 6, 2017);

        City city = createCity("city");
        Address address = createAddress(city);

        Activity saved = activityDao.save(createRegularActivity(DayOfWeek.WEDNESDAY, now, DateUtils.addDays(now, 20), address.getId(), school, room));

        List<Activity> list = activityDao.findAllByRange(createDate(12, 6, 2017), createDate(19, 6, 2017), city.getPid());
        assertEquals(1, list.size());

        assertEquals(saved.getId(), list.get(0).getId());
    }

    /**
     * Начало действия после завтра, повтор по завтрашнему дню, период с завтрашнего дня
     */
    @Test
    public void testFindAllForSchool() {
        Date now = createDate(13, 6, 2017);

        City city = createCity("city");
        Address address = createAddress(city);

        Activity saved = activityDao.save(createRegularActivity(DayOfWeek.THURSDAY, now, DateUtils.addDays(now, 20), address.getId(), school, room));

        List<Activity> list = activityDao.findAllForSchool(school.getId(), createDate(10, 6, 2017), createDate(16, 6, 2017));
        assertEquals(1, list.size());

        assertEquals(saved.getId(), list.get(0).getId());
    }

    private Address createAddress(City city) {
        Address address = new Address();
        address.setValue("test address");
        address.setCityId(city.getId());
        address.setPid("sdfsdf");

        return addressDao.save(address);
    }

    private City createCity(String cityName) {
        City city = new City();
        city.setName(cityName);
        city.setPid("sdfs");
        return cityDao.save(city);
    }

    //
//    /**
//     * Дата конца выборки больше, чем дата окончания действия урока. Но сам урок попадает в указанные даты
//     */
//    @Test
//    public void testFindAllByRange2() {
//        Activity saved = activityDao.save(createRegularActivity(DayOfWeek.WEDNESDAY, createDate(1, 4, 2017), createDate(7, 4, 2017)));
//        Activity saved2 = activityDao.save(createRegularActivity(DayOfWeek.WEDNESDAY, createDate(8, 4, 2017), createDate(14, 4, 2017)));
//
//        List<Activity> list = activityDao.findAllByRange(createDate(2, 4, 2017), createDate(8, 4, 2017), "");
//        assertEquals(1, list.size());
//
//        assertEquals(saved.getId(), list.getForSchool(0).getId());
//
//    }
//
//    /**
//     * Урок повторяется дважды за период
//     */
//    @Test
//    public void testFindAllByRange3() {
//        Activity saved = activityDao.save(createRegularActivity(DayOfWeek.WEDNESDAY, createDate(1, 4, 2017), createDate(14, 4, 2017)));
//        Activity saved2 = activityDao.save(createRegularActivity(DayOfWeek.WEDNESDAY, createDate(15, 4, 2017), createDate(21, 4, 2017)));
//
//        List<Activity> list = activityDao.findAllByRange(createDate(2, 4, 2017), createDate(14, 4, 2017), "");
//        assertEquals(2, list.size());
//
//        Activity act1 = list.getForSchool(0);
//        assertEquals(saved.getId(), act1.getId());
//        assertEquals(createDate(3, 4, 2017), act1.getActionDate());
//
//
//        Activity act2 = list.getForSchool(1);
//        assertEquals(saved.getId(), act2.getId());
//        assertEquals(createDate(10, 4, 2017), act2.getActionDate());
//    }
//
//    /**
//     * Разовые уроки
//     */
//    @Test
//    public void testFindAllByRange4() {
//        Activity saved = activityDao.save(createSingleActivity(createDate(4, 4, 2017)));
//        Activity saved2 = activityDao.save(createRegularActivity(DayOfWeek.WEDNESDAY, createDate(15, 4, 2017), createDate(21, 4, 2017)));
//
//        List<Activity> list = activityDao.findAllByRange(createDate(2, 4, 2017), createDate(14, 4, 2017), "");
//        assertEquals(1, list.size());
//
//        Activity act1 = list.getForSchool(0);
//        assertEquals(saved.getId(), act1.getId());
//        assertEquals(createDate(4, 4, 2017), act1.getActionDate());
//    }
//
    private Room createRoom(School school, long addressId) {
        Room room = new Room();
        room.setAddressId(addressId);
        room.setName("345");
        room.setSchoolId(school.getId());
        room.setUserId(1);
        return roomDao.save(room);
    }

    ;

    private School createSchool(long addressId) {
        School school = new School();
        school.setAddressId(addressId);
        school.setUserId(1);
        school.setMobilePhone("sdfs");
        school.setName("sdfsd");
        return schoolDao.save(school);
    }

    private Activity createRegularActivity(DayOfWeek dayOfWeek, Date startDate, Date endDate, long addressId, School school, Room room) {
        Activity entity = new Activity();
        entity.setStartDate(startDate);
        entity.setEndDate(endDate);
        entity.setStartTime(new Date());
        entity.setDayOfWeek(dayOfWeek);
        entity.setRegular(true);
        entity.setType(ActivityType.LESSON);
        entity.setUserId(1L);
        entity.setAddressId(addressId);

        entity.setRoomId(room.getId());
        entity.setSchoolId(school.getId());
        return entity;
    }

    //
//    private Activity createSingleActivity(Date startDate) {
//        Activity entity = new Activity();
//        entity.setStartDate(startDate);
//        entity.setEndDate(startDate);
//        entity.setStartTime(new Date());
//        entity.setRegular(false);
//        entity.setType(ActivityType.LESSON);
//        entity.setUserId(1);
//        return entity;
//    }
//
//
    private Date createDate(int day, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(year, month, day);
        return calendar.getTime();
    }
}
